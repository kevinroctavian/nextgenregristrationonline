//
//  NGREGEngine.h
//  NextGenRegistrationOnline
//
//  Created by Kevin Octavian on 1/16/14.
//  Copyright (c) 2014 Kevin R. Octavian. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <RestKit/CoreData.h>
#import <RestKit/RestKit.h>
#import "NGREGCheckRegistrantResponse.h"
#import "NGREGSync+Additions.h"

@interface NGREGEngine : NSObject

typedef void(^NGREGRequestCompletionHandler)(id result, NSError *error);
typedef void (^entourageBlock)(NGREGCheckRegistrantResponse* entourage);
typedef void (^successBlock)(bool success);

+ (id)sharedEngine;
+ (void)initializeEngine;

- (NSString *)imageAtURLString:(NSString *)urlString withCompletionHandler:(NGREGRequestCompletionHandler)block;


#pragma mark - API Methods

- (NSString *)requestLoginWithUsername:(NSString*)username andPassword:(NSString*)pwd andBrandId:(NSString*)brandId andWithCompletionHandler:(NGREGRequestCompletionHandler)block;
- (NSString *)requestBrandListWithCompletionHandler:(NGREGRequestCompletionHandler)block;
- (NSString *)requestBrandThemeWithBrandId:(NSString*)brandId andCompletionHandler:(NGREGRequestCompletionHandler)block;
- (NSString *)requestMyProfileWithCompletionHandler:(NGREGRequestCompletionHandler)block;

- (NSString *)requestDataRegistrantWithGIIDNumber:(NSString*)giidnumber andCompletionHandler:(NGREGRequestCompletionHandler)block;
- (NSString *)requestDataRegistrantWithEMail:(NSString*)email andCompletionHandler:(NGREGRequestCompletionHandler)block;

// tracking
- (NSString *)requestTrackActivityWithMenu:(NSString*)menu andPage:(NSString*)page andCompletionHandler:(NGREGRequestCompletionHandler)block;
- (NSString *)requestTrackGameWithData:(NSDictionary*)data andCompletionHandler:(NGREGRequestCompletionHandler)block;

- (NSString *)requestListSubBrandsWithCompletionHandler:(NGREGRequestCompletionHandler)block;
- (NSString *)requestListCityWithCompletionHandler:(NGREGRequestCompletionHandler)block;

- (NSString*)requestRegisterRegistrant:(NGREGCheckRegistrantResponse *)entourage atEvent:(NSString*)event withCompletionHandler:(NGREGRequestCompletionHandler)block;

- (NSString *)requestSync:(NGREGSync *)sync withCompletionHandler:(NGREGRequestCompletionHandler)block;
- (NSString *)requestLogoutWithCompletionHandler:(NGREGRequestCompletionHandler)block;

- (NSString *)requestReportWithMenuId:(NSString*)menuId andCompletionHandler:(NGREGRequestCompletionHandler)block;

- (NSString *)requestCrashReportWithBrand:(NSString*)brand andUserId:(NSString*)userid andTime:(NSDate*)time andReason:(NSString*)reason andDetail:(NSString*)detail andCompletionHandler:(NGREGRequestCompletionHandler)block;

- (void)checkEntourageByGIIDNumber:(NSString*)number andCompletionHandler:(entourageBlock)block;
- (void)checkEntourageByEmail:(NSString*)email andCompletionHandler:(entourageBlock)block;

@end
