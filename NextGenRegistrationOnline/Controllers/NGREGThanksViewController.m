//
//  NGREGThanksViewController.m
//  NextGenRegistrationOnline
//
//  Created by Kevin Octavian on 2/19/14.
//  Copyright (c) 2014 Kevin R. Octavian. All rights reserved.
//

#import "NGREGThanksViewController.h"
#import "Reachability.h"

@interface NGREGThanksViewController ()
@property (weak, nonatomic) IBOutlet UIButton *homeButton;
@property (weak, nonatomic) IBOutlet UILabel *thankyouLabel;
@property (weak, nonatomic) IBOutlet UILabel *text1;
@property (weak, nonatomic) IBOutlet UILabel *text2;
@property (weak, nonatomic) IBOutlet UIImageView *bgImage;

@end

@implementation NGREGThanksViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    NGREGAppDelegate* appDelegate = (NGREGAppDelegate *)[[UIApplication sharedApplication] delegate];
    
    NSArray* ar = [NGREGProfile profileWithEmail:[[NSUserDefaults standardUserDefaults] valueForKey:kNGREGEmailNextGen] InManagedObjectContext:[appDelegate managedObjectContext]];
    NGREGProfile *profile = [ar lastObject];
    
//    [self.bgImage setImageWithURL:[NSURL URLWithString:[profile.customizeTemplates valueForKey:@"global"]] placeholderImage:nil options:SDWebImageRetryFailed];
    [self loadImage:self.bgImage withUrl:[profile.customizeTemplates valueForKey:@"global"] andMD5:[profile.customizeTemplates valueForKey:@"global_md5"]];
    
    // set text size
    [self.thankyouLabel setFont:[UIFont systemFontOfSize:[[profile.customizeTemplates objectForKey:@"title-text-thanks-size"] intValue] ]];
    [self.text1 setFont:[UIFont systemFontOfSize:[[profile.customizeTemplates objectForKey:@"subtitle-text-thanks-size"] intValue] ]];
    [self.text2 setFont:[UIFont systemFontOfSize:[[profile.customizeTemplates objectForKey:@"subtitle-text-thanks-size"] intValue] ]];
    
    
    [NGREGUtil setFontBoldFromThisView:self.homeButton andIdFont:[[profile.customizeTemplates valueForKey:@"text-style"] integerValue]];
    [NGREGUtil setFontBoldFromThisView:self.thankyouLabel andIdFont:[[profile.customizeTemplates valueForKey:@"text-style"] integerValue]];

    [NGREGUtil setFontFromThisView:self.text1 andIdFont:[[profile.customizeTemplates valueForKey:@"text-style"] integerValue]];
    [NGREGUtil setFontFromThisView:self.text2 andIdFont:[[profile.customizeTemplates valueForKey:@"text-style"] integerValue]];
    
    if([self.registrant.registrationID length] >0){
        // existing
        NSString* copyText = [profile.customizeTemplates valueForKey:@"thankyou-existing-online"];
        
        copyText = [copyText stringByReplacingOccurrencesOfString:@"[%@name%@]" withString:self.registrant.email];
        
        [self.text1 setText:copyText];
    }else{
        Reachability *reach = [Reachability reachabilityForInternetConnection];
        NetworkStatus status = [reach currentReachabilityStatus];
        
        switch(status) {
            case NotReachable:
                [self.text1 setText:[profile.customizeTemplates valueForKey:@"thankyou-new-registrant-offline"]];
                break;
            case ReachableViaWiFi:
                [self.text1 setText:[profile.customizeTemplates valueForKey:@"thankyou-new-registrant-online"]];
                break;
            case ReachableViaWWAN:
                [self.text1 setText:[profile.customizeTemplates valueForKey:@"thankyou-new-registrant-online"]];
                break;
            default:
                [self.text1 setText:[profile.customizeTemplates valueForKey:@"thankyou-new-registrant-offline"]];
                break;
        }
    }
    
    
    [self loadImageButton:self.homeButton forState:UIControlStateNormal withUrl:[profile.customizeTemplates objectForKey:@"button-submit"] andMD5:[profile.customizeTemplates objectForKey:@"button-submit_md5"]];
    [self.homeButton.titleLabel setTextColor:[NGREGUtil colorFromHexString:[profile.customizeTemplates valueForKey:@"text-label-menu-color"] ]];
    
    [self.thankyouLabel setTextColor:[NGREGUtil colorFromHexString:[profile.customizeTemplates valueForKey:@"text-label-color"] ]];
    [self.text1 setTextColor:[NGREGUtil colorFromHexString:[profile.customizeTemplates valueForKey:@"text-label-color"] ]];
    [self.text2 setTextColor:[NGREGUtil colorFromHexString:[profile.customizeTemplates valueForKey:@"text-label-color"] ]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Action methods
- (IBAction)homeButtonClicked:(id)sender {
//    [self cekLogging];
    [self.navigationController popViewControllerAnimated:NO];
}


@end
