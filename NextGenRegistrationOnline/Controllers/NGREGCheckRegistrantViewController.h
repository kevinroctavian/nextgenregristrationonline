//
//  NGREGCheckRegistrantViewController.h
//  NextGenRegistrationOnline
//
//  Created by Kevin Octavian on 1/21/14.
//  Copyright (c) 2014 Kevin R. Octavian. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NGREGGenericViewController.h"

@protocol  NGREGCheckRegistrantViewControllerDelegate;

@interface NGREGCheckRegistrantViewController : NGREGGenericViewController

@property (strong, nonatomic) NSDictionary* menu;
@property (assign, nonatomic) id <NGREGCheckRegistrantViewControllerDelegate>delegate;

@end

@protocol NGREGCheckRegistrantViewControllerDelegate <NSObject>
- (void)checkRegistrantVideoViewController:(UIViewController *)viewController didFinish:(NSDictionary*) menu withRegistrant:(NGREGCheckRegistrantResponse*) registrant;
@end
