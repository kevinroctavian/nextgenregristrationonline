//
//  BEATCityPickerViewController.m
//  BEAT-Universal
//
//  Created by Bayu Wy on 11/11/13.
//  Copyright (c) 2013 bayuwy. All rights reserved.
//

#import "NGREGCityPickerViewController.h"

@interface NGREGCityPickerViewController () <UITableViewDataSource, UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSArray *filteredCityArray;
@end

@implementation NGREGCityPickerViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    self.filteredCityArray = [self.delegate cityPickerViewControllerCityArray:self];
    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Public

- (void)showSuggestionsFor:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    NSMutableString *rightText;
    
    if (textField.text) {
        rightText = [NSMutableString stringWithString:textField.text];
        [rightText replaceCharactersInRange:range withString:string];
    }
    else {
        rightText = [NSMutableString stringWithString:string];
    }
    
    NSArray *cities = [self.delegate cityPickerViewControllerCityArray:self];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"cityName CONTAINS[cd] %@", rightText];
    self.filteredCityArray = [cities filteredArrayUsingPredicate:predicate];
    
    if([self.filteredCityArray count] > 4){
        [self.tableView flashScrollIndicators];
    }
    
    [self.tableView reloadData];
}

#pragma mark - UITableViewDataSource, UITableViewDelegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [self.filteredCityArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CityCellId" forIndexPath:indexPath];
    
    NSDictionary *data = self.filteredCityArray[indexPath.row];
    cell.textLabel.text = [data[@"cityName"]capitalizedString];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [self.delegate cityPickerViewController:self didSelectCity:self.filteredCityArray[indexPath.row]];
}

@end
