//
//  NGREGBrandPopoverViewController.h
//  NextGenRegistrationOnline
//
//  Created by Kevin Octavian on 1/21/14.
//  Copyright (c) 2014 Kevin R. Octavian. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NGREGBrand.h"

#define kNGREGBrandPopoverWidth       200
#define kNGREGBrandPopoverHeight      100
#define kNGREGBrandPopoverHeightItemCell      50
#define kNGREGBrandPopoverHeightMax      500

@interface NGREGBrandPopoverViewController : UITableViewController

@property(strong) NSArray* brands;
@property (strong) void (^selectedBrand)(NGREGBrand* brand);

@end
