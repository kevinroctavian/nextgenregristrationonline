//
//  NGREGEventPopoverViewController.h
//  NextGenRegistrationOnline
//
//  Created by Kevin Octavian on 2/10/14.
//  Copyright (c) 2014 Kevin R. Octavian. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NGREGEventPopoverViewController : UITableViewController

@property(strong) NSArray* events;
@property (strong) void (^selectedEvent)(NSDictionary* event);

@end
