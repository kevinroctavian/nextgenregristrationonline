//
//  BEATDatePickerPopupViewController.m
//  BEAT-Universal
//
//  Created by Kevin R. Octavian on 10/30/13.
//  Copyright (c) 2013 bayuwy. All rights reserved.
//

#import "NGREGDatePickerPopupViewController.h"

@interface NGREGDatePickerPopupViewController ()

@property (weak, nonatomic) IBOutlet UIButton *buttonDone;
@property (weak, nonatomic) IBOutlet UIDatePicker *datePicker;

@end
//33067892

@implementation NGREGDatePickerPopupViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"dd MMM yyyy"];
    
    if(_isBirthday){
        NSDateComponents* dateComponents = [[NSDateComponents alloc]init];
        [dateComponents setYear:-18];
        NSCalendar* calendar = [NSCalendar currentCalendar];
        NSDate* year18ago = [calendar dateByAddingComponents:dateComponents toDate:[NSDate date] options:0];
        
//        NSDate *year18ago =  [[NSDate date] dateByAddingTimeInterval:-60*60*24*30*12*18];
        
//        self.datePicker.maximumDate = [dateFormat dateFromString:@"12/31/1995"];
        
        self.datePicker.maximumDate = year18ago;
        self.datePicker.minimumDate = [dateFormat dateFromString:@"1/1/1973"];
    }else{
        NSDate *now = [NSDate date];
        self.datePicker.minimumDate = now;
    }
    
    if(self.date.length > 0){
        NSDate *anyDate = [dateFormat dateFromString:self.date];
        [self.datePicker setDate:anyDate];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark Action

- (IBAction)done:(id)sender {
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd"];
    
    self.done([dateFormat stringFromDate:self.datePicker.date], self.datePicker.date);
}


@end
