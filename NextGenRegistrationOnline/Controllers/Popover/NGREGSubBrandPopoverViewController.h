//
//  NGREGSubBrandPopoverViewController.h
//  NextGenRegistrationOnline
//
//  Created by Kevin Octavian on 2/6/14.
//  Copyright (c) 2014 Kevin R. Octavian. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NGREGSubBrand.h"

@interface NGREGSubBrandPopoverViewController : UITableViewController

@property(strong) NSArray* brands;
@property (strong) void (^selectedBrand)(NSDictionary* brand);

@end
