//
//  BEATDatePickerPopupViewController.h
//  BEAT-Universal
//
//  Created by Kevin R. Octavian on 10/30/13.
//  Copyright (c) 2013 bayuwy. All rights reserved.
//

#import <UIKit/UIKit.h>

#define kBEATDatePickerContentWidth       262
#define kBEATDatePickerContentHeight       230

@interface NGREGDatePickerPopupViewController : UIViewController{
    
}

@property(nonatomic, strong) NSString* date;
@property(nonatomic) bool isBirthday;

@property (strong) void (^done)(NSString* dateString, NSDate* date);

@end
