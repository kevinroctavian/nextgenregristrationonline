//
//  BEATCityPickerViewController.h
//  BEAT-Universal
//
//  Created by Bayu Wy on 11/11/13.
//  Copyright (c) 2013 bayuwy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NGREGCity.h"

@protocol NGREGCityPickerViewControllerDelegate;

@interface NGREGCityPickerViewController : UIViewController
@property (nonatomic, assign) id<NGREGCityPickerViewControllerDelegate> delegate;

- (void)showSuggestionsFor:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string;

@end

@protocol NGREGCityPickerViewControllerDelegate <NSObject>
- (NSArray *)cityPickerViewControllerCityArray:(NGREGCityPickerViewController *)controller;
- (void)cityPickerViewController:(NGREGCityPickerViewController *)controller didSelectCity:(NSDictionary *)city;

@end
