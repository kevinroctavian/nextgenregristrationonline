//
//  NGREGReportViewController.h
//  NextGenRegistrationOnline
//
//  Created by Kevin Octavian on 1/24/14.
//  Copyright (c) 2014 Kevin R. Octavian. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NGREGGenericViewController.h"

@interface NGREGReportViewController : NGREGGenericViewController

@property (strong, nonatomic) NSDictionary* menu;

@end
