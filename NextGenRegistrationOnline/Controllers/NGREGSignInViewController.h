//
//  NGREGSignInViewController.h
//  NextGenRegistrationOnline
//
//  Created by Kevin Octavian on 2/5/14.
//  Copyright (c) 2014 Kevin R. Octavian. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NGREGGenericViewController.h"
#import "NGREGCheckRegistrantViewController.h"

@interface NGREGSignInViewController : NGREGGenericViewController

@property (assign, nonatomic) id <NGREGCheckRegistrantViewControllerDelegate>delegate;
@property (strong, nonatomic) NSDictionary* menu;
@property (strong, nonatomic) NGREGCheckRegistrantResponse* registrant;
@property (strong) void (^didFinishRegisterRegistrant)(NGREGCheckRegistrantResponse* registrant);

@end
