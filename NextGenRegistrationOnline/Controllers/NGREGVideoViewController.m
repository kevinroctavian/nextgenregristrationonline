//
//  NGREGVideoViewController.m
//  NextGenRegistrationOnline
//
//  Created by Kevin Octavian on 1/24/14.
//  Copyright (c) 2014 Kevin R. Octavian. All rights reserved.
//

#import "NGREGVideoViewController.h"
#import "NGREGAppDelegate.h"
#import "NGREGProfile+Additions.h"
#import <MediaPlayer/MediaPlayer.h>
#import "NGREGEngine.h"

@interface NGREGVideoViewController ()

@property (weak, nonatomic) IBOutlet UIView *videoView;
@property (weak, nonatomic) IBOutlet UIProgressView *progressView;
@property (nonatomic, strong) MPMoviePlayerController *moviePlayer;
@property (weak, nonatomic) IBOutlet UIButton *homeButton;
@property (weak, nonatomic) IBOutlet UIButton *nextButton;
@property (weak, nonatomic) IBOutlet UIView *endOfVideoView;


@end

@implementation NGREGVideoViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setUI];
    [self.progressView setHidden:YES];
    
    if(![[[self.menu valueForKey:@"video"] valueForKey:@"status"]boolValue]){
        [self.delegate videoViewController:self didFinish:self.menu];
        return;
    }
    
    NSArray *parts = [[[self.menu valueForKey:@"video"] valueForKey:@"url"] componentsSeparatedByString:@"/"];
    NSString *filename = [parts objectAtIndex:[parts count]-1];
    
    if([self playVideo:filename]){
        return;
    }else{
        
        [self downloadFile];
    }
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Helper methods

- (void)setUI{
    
    NGREGAppDelegate* appDelegate = (NGREGAppDelegate *)[[UIApplication sharedApplication] delegate];
    
    
    NSArray* ar = [NGREGProfile profileWithEmail:[[NSUserDefaults standardUserDefaults] valueForKey:kNGREGEmailNextGen] InManagedObjectContext:[appDelegate managedObjectContext]];
    NGREGProfile* profile = [ar lastObject];
    
    [NGREGUtil setFontBoldFromThisView:self.nextButton andIdFont:[[profile.customizeTemplates valueForKey:@"text-style"] integerValue]];
    [NGREGUtil setFontBoldFromThisView:self.homeButton andIdFont:[[profile.customizeTemplates valueForKey:@"text-style"] integerValue]];
    
    [self.nextButton setTintColor:[NGREGUtil colorFromHexString:[profile.customizeTemplates objectForKey:@"text-label-menu-color"] ]];
//    [self.nextButton setBackgroundImageWithURL:[NSURL URLWithString:[profile.customizeTemplates valueForKey:@"button-submit"]] forState:UIControlStateNormal placeholderImage:nil options:SDWebImageRetryFailed];
    [self.homeButton setTintColor:[NGREGUtil colorFromHexString:[profile.customizeTemplates objectForKey:@"text-label-menu-color"] ]];
//    [self.homeButton setBackgroundImageWithURL:[NSURL URLWithString:[profile.customizeTemplates valueForKey:@"button-submit"]] forState:UIControlStateNormal placeholderImage:nil options:SDWebImageRetryFailed];
    
    [self loadImageButton:self.nextButton forState:UIControlStateNormal withUrl:[profile.customizeTemplates objectForKey:@"button-submit"] andMD5:[profile.customizeTemplates objectForKey:@"button-submit_md5"]];
    [self loadImageButton:self.homeButton forState:UIControlStateNormal withUrl:[profile.customizeTemplates objectForKey:@"button-submit"] andMD5:[profile.customizeTemplates objectForKey:@"button-submit_md5"]];
    
}

- (BOOL)playVideo:(NSString*)fileName{
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *path = [[paths objectAtIndex:0] stringByAppendingPathComponent:fileName];
    
    NSFileManager *fileManager = [[NSFileManager alloc] init];
    BOOL isDir;
    BOOL fileExists = [fileManager fileExistsAtPath:path isDirectory:&isDir];
    
    if (!fileExists)
        return false;
    
    NSURL *url = [NSURL fileURLWithPath:path];
    
    self.moviePlayer =
    [[MPMoviePlayerController alloc]
     initWithContentURL:url];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(moviePlayBackDidFinish:)
                                                 name:MPMoviePlayerPlaybackDidFinishNotification
                                               object:self.moviePlayer];
    [[NSNotificationCenter defaultCenter] addObserver: self selector: @selector(moviePlayResignActive:) name: UIApplicationWillResignActiveNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver: self selector: @selector(moviePlayDidBecomeActive:) name: UIApplicationDidBecomeActiveNotification object:nil];

    self.moviePlayer.shouldAutoplay = YES;
    [self.moviePlayer setFullscreen:YES animated:YES];
    [self.moviePlayer prepareToPlay];
    self.moviePlayer.controlStyle = MPMovieControlStyleNone;
    self.moviePlayer.shouldAutoplay = YES;
    
    self.moviePlayer.movieSourceType = MPMovieSourceTypeFile;
    self.moviePlayer.view.frame = self.videoView.bounds;
    
    [self.moviePlayer setMovieSourceType:MPMovieSourceTypeFile];
    
    [self.videoView addSubview:self.moviePlayer.view];
    [self.moviePlayer setFullscreen:YES animated:YES];
    [self.moviePlayer play];
    
    [[NGREGEngine sharedEngine] requestTrackActivityWithMenu:[self.menu valueForKey:@"name"] andPage:@"video" andCompletionHandler:nil];
    
    return true;
}

- (void) moviePlayBackDidFinish:(NSNotification*)notification {
    [self.moviePlayer setFullscreen:NO animated:YES];
    self.endOfVideoView.hidden = NO;
    self.homeButton.hidden = NO;
    self.nextButton.hidden = NO;
}

- (void) moviePlayResignActive:(NSNotification*)notification {
    [self.moviePlayer pause];
}

- (void) moviePlayDidBecomeActive:(NSNotification*)notification {
    if(self.endOfVideoView.hidden)
        [self.moviePlayer play];
}


- (void)downloadFile{
    
    [self.progressView setHidden:NO];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:[[self.menu valueForKey:@"video"] valueForKey:@"url"]]];
    NSString *pdfName = [[self.menu valueForKey:@"video"] valueForKey:@"filename"];
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *path = [[paths objectAtIndex:0] stringByAppendingPathComponent:pdfName];
    operation.outputStream = [NSOutputStream outputStreamToFileAtPath:path append:NO];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         NSLog(@"Successfully downloaded file to %@", path);
         [self.progressView setHidden:YES];
         [self playVideo:[[self.menu valueForKey:@"video"] valueForKey:@"filename"]];
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         NSLog(@"Error: %@", error);
     }];
    [operation setDownloadProgressBlock:^(NSUInteger bytesRead, long long totalBytesRead, long long totalBytesExpectedToRead)
     {
         //do something in this line with the calculation to cell
         float progress = (float)totalBytesRead / totalBytesExpectedToRead;

         [self.progressView setProgress:progress animated:YES];
         
         NSLog(@"Download = %f", progress);
     }];
    [operation start];
}

#pragma mark - Action Methods

- (IBAction)homeButtonClicked:(id)sender {
    [self.navigationController popViewControllerAnimated:NO];
}

- (IBAction)nextButtonClicked:(id)sender {
    [self.delegate videoViewController:self didFinish:self.menu];
}

- (IBAction)skipButtonClicked:(id)sender {
    [self.moviePlayer stop];
    [self.moviePlayer setFullscreen:NO animated:YES];
    self.endOfVideoView.hidden = NO;
    self.homeButton.hidden = NO;
    self.nextButton.hidden = NO;
}


@end
