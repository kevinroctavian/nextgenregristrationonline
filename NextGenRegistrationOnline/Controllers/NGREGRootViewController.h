//
//  NGREGRootViewController.h
//  NextGenRegistrationOnline
//
//  Created by Kevin Octavian on 1/17/14.
//  Copyright (c) 2014 Kevin R. Octavian. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NGREGAppDelegate.h"
#import "NGREGProfile+Additions.h"
#import "NGREGBottomPanelViewController.h"
#import "NGREGEngine.h"
#import "DejalActivityView.h"
#import "NGREGLoginResponse.h"
#import "NGREGBrand+Additions.h"
#import "NGREGAppDelegate.h"
#import "UIView+Additions.h"
#import "NGREGBrandPopoverViewController.h"
#import "WYPopoverController.h"
#import "NGREGUtil.h"
#import "Constant.h"

//#define kNGREGSubmissionVersionNextGen @"submissionVersionNextGen"
//#define kNGREGSubmissionTotalNextGen @"submissionTotalNextGen"

@interface NGREGRootViewController : UIViewController

@end
