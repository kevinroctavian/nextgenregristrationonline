//
//  NGREGSignInViewController.m
//  NextGenRegistrationOnline
//
//  Created by Kevin Octavian on 2/5/14.
//  Copyright (c) 2014 Kevin R. Octavian. All rights reserved.
//

#import "NGREGSignInViewController.h"
#import "BezierInterpView.h"
#import "NGREGUtil.h"
#import "NGREGBottomPanelViewController.h"
#import "NGREGReport+Additions.h"
#import "AMPopTip.h"

@interface NGREGSignInViewController () <NGREGBottomPanelViewControllerDelegate>{
    bool isTncRegistrant, isTncDST, isSignRegistrant, isSignDST;
    NGREGAppDelegate *appDelegate;
}

@property (strong, nonatomic) NGREGBottomPanelViewController *panelViewController;
@property (weak, nonatomic) IBOutlet UIView *noViewRegistrant;
@property (weak, nonatomic) IBOutlet UIView *noViewDst;

@property (weak, nonatomic) IBOutlet UIImageView *bgImageView;

@property (weak, nonatomic) IBOutlet UILabel *signHereLabel;
@property (weak, nonatomic) IBOutlet UILabel *registrantLabel;
@property (weak, nonatomic) IBOutlet UILabel *dstLabel;
@property (weak, nonatomic) IBOutlet UILabel *tncRegistrantLabel;
@property (weak, nonatomic) IBOutlet UILabel *tncDSTLabel;
@property (weak, nonatomic) IBOutlet UITextView *tncRegistrantTextView;

@property (weak, nonatomic) IBOutlet UIButton *checkBoxRegistrantButton;
@property (weak, nonatomic) IBOutlet UIButton *checkBoxDSTButton;
@property (weak, nonatomic) IBOutlet UIButton *submitButton;

@property (weak, nonatomic) IBOutlet UIButton *clearSignRegistrantLabel;
@property (weak, nonatomic) IBOutlet UIButton *clearSignDSTButton;

@property (weak, nonatomic) IBOutlet BezierInterpView *signInRegistrantView;
@property (weak, nonatomic) IBOutlet BezierInterpView *signInDSTView;

@property (nonatomic, strong) AMPopTip *popTip;

@end

@implementation NGREGSignInViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    isTncRegistrant = false;
    isTncDST = false;
    isSignDST = false;
    isSignRegistrant = false;
    
    [self changeButtonSubmit];
    
    [self.signInDSTView setTouchesEnded:^{
        isSignDST = true;
        [self changeButtonSubmit];
    }];
    
    [self.signInRegistrantView setTouchesEnded:^{
        isSignRegistrant = true;
        [self changeButtonSubmit];
    }];
    
    [self setUI];
    
    
    self.popTip = [AMPopTip popTip];
    self.popTip.shouldDismissOnTap = YES;
    
    self.popTip.popoverColor = [NGREGUtil colorFromHexString:@"#333333"];
    self.popTip.textColor = [NGREGUtil colorFromHexString:@"#ffffff"];
}

- (void)viewWillAppear:(BOOL)animated{
    [self setUI];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Helpher methods

- (void)setUI{
    
    [self.tncRegistrantTextView flashScrollIndicators];
    
    appDelegate = (NGREGAppDelegate *)[[UIApplication sharedApplication] delegate];
    
    NSArray* ar = [NGREGProfile profileWithEmail:[[NSUserDefaults standardUserDefaults] valueForKey:kNGREGEmailNextGen] InManagedObjectContext:[appDelegate managedObjectContext]];
    NGREGProfile *profile = [ar lastObject];
    
//    [self.bgImageView setImageWithURL:[NSURL URLWithString:[profile.customizeTemplates valueForKey:@"global"]]];
//    [self.bgImageView setImageWithURL:[NSURL URLWithString:[profile.customizeTemplates valueForKey:@"global"]] placeholderImage:nil options:SDWebImageRetryFailed];
    
    [self loadImage:self.bgImageView withUrl:[profile.customizeTemplates valueForKey:@"global"] andMD5:[profile.customizeTemplates valueForKey:@"global_md5"]];
    
    self.panelViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"BottomPanel"];
    [self.panelViewController setDelegate:self];
    [self addChildViewController:self.panelViewController];
    
    CGRect panelViewFrame = CGRectMake(0.0f, self.view.bounds.size.height - kPanelViewVisibleHeight, 1024.0f, 500);
    self.panelViewController.view.frame = panelViewFrame;
    [self.view addSubview:self.panelViewController.view];
    
    [self.panelViewController setDataArray:@[@{@"name":@"< HOME"}]];
    
    [self.panelViewController setTextSize:[[profile.customizeTemplates valueForKey:@"text-size"] integerValue] andTextColor:[profile.customizeTemplates valueForKey:@"text-label-menu-color"] andBackgroundColor:[profile.customizeTemplates valueForKey:@"text-label-menu-background-color"]andFontId:[[profile.customizeTemplates valueForKey:@"text-style"] integerValue] andBgUrl:[profile.customizeTemplates valueForKey:@"button-menu"] andMD5:[profile.customizeTemplates valueForKey:@"button-menu_md5"]];
    
    [self.signHereLabel setTextColor:[NGREGUtil colorFromHexString:[profile.customizeTemplates valueForKey:@"text-label-menu-background-color"] ]];
    
    [self.registrantLabel setTextColor:[NGREGUtil colorFromHexString:[profile.customizeTemplates valueForKey:@"text-label-color"] ]];
    [self.dstLabel setTextColor:[NGREGUtil colorFromHexString:[profile.customizeTemplates valueForKey:@"text-label-color"] ]];
    [self.tncDSTLabel setTextColor:[NGREGUtil colorFromHexString:[profile.customizeTemplates valueForKey:@"text-label-color"] ]];
    [self.tncRegistrantLabel setTextColor:[NGREGUtil colorFromHexString:[profile.customizeTemplates valueForKey:@"text-label-color"] ]];
    [self.tncRegistrantTextView setTextColor:[NGREGUtil colorFromHexString:[profile.customizeTemplates valueForKey:@"text-label-color"] ]];
    
//    [self.submitButton setBackgroundImageWithURL:[NSURL URLWithString:[profile.customizeTemplates valueForKey:@"button-submit"]] forState:UIControlStateNormal placeholderImage:nil options:SDWebImageRetryFailed];
//    [self.submitButton setBackgroundImageWithURL:[NSURL URLWithString:[profile.customizeTemplates valueForKey:@"button-submit-disable"]] forState:UIControlStateSelected placeholderImage:nil options:SDWebImageRetryFailed];
    
    
    [self loadImageButton:self.submitButton forState:UIControlStateNormal withUrl:[profile.customizeTemplates objectForKey:@"button-submit"] andMD5:[profile.customizeTemplates objectForKey:@"button-submit_md5"]];
    [self loadImageButton:self.submitButton forState:UIControlStateDisabled withUrl:[profile.customizeTemplates objectForKey:@"button-submit-disable"] andMD5:[profile.customizeTemplates objectForKey:@"button-submit-disable_md5"]];
    
    [NGREGUtil setFontBoldFromThisView:self.submitButton andIdFont:[[profile.customizeTemplates valueForKey:@"text-style"] integerValue]];
    [NGREGUtil setFontBoldFromThisView:self.signHereLabel andIdFont:[[profile.customizeTemplates valueForKey:@"text-style"] integerValue]];
    [NGREGUtil setFontBoldFromThisView:self.registrantLabel andIdFont:[[profile.customizeTemplates valueForKey:@"text-style"] integerValue]];
    [NGREGUtil setFontBoldFromThisView:self.dstLabel andIdFont:[[profile.customizeTemplates valueForKey:@"text-style"] integerValue]];
    [NGREGUtil setFontBoldFromThisView:self.tncDSTLabel andIdFont:[[profile.customizeTemplates valueForKey:@"text-style"] integerValue]];
    [NGREGUtil setFontBoldFromThisView:self.tncRegistrantLabel andIdFont:[[profile.customizeTemplates valueForKey:@"text-style"] integerValue]];
    
    [NGREGUtil setFontFromThisView:self.tncRegistrantTextView andIdFont:[[profile.customizeTemplates valueForKey:@"text-style"] integerValue]];
    [NGREGUtil setFontFromThisView:self.clearSignDSTButton andIdFont:[[profile.customizeTemplates valueForKey:@"text-style"] integerValue]];
    [NGREGUtil setFontFromThisView:self.clearSignRegistrantLabel andIdFont:[[profile.customizeTemplates valueForKey:@"text-style"] integerValue]];
    
    [self.tncDSTLabel setNumberOfLines:0];
    [self.tncDSTLabel sizeToFit];
}

- (void)changeButtonSubmit{
    
    [self.popTip hide];
    if(isSignRegistrant && isSignDST && isTncDST && isTncRegistrant)
        [self.submitButton setSelected:NO];
    else
        [self.submitButton setSelected:YES];
}

#pragma mark - Action methods

- (IBAction)clearSignInRegistrant:(id)sender {
    if(self.signInRegistrantView.incrementalImage){
        self.signInRegistrantView.incrementalImage = FALSE;
        [self.signInRegistrantView setNeedsDisplay];
    }
    isSignRegistrant = false;
    [self changeButtonSubmit];
}

- (IBAction)clearSignInDST:(id)sender {
    if(self.signInDSTView.incrementalImage){
        self.signInDSTView.incrementalImage = FALSE;
        [self.signInDSTView setNeedsDisplay];
    }
    isSignDST = false;
    [self changeButtonSubmit];
}

- (IBAction)submitButtonClicked:(id)sender {
    
    [self.popTip hide];

    NSString* message = @"";
    UIView* view;
    
    if(!isSignRegistrant ){
        message = @"Please input your digital signature";
        view = self.noViewDst;
        
        [self.popTip showText:message direction:AMPopTipDirectionNone maxWidth:300 inView:self.view fromFrame:view.frame];
        return;
    }
    
    if(!isSignDST){
        message = @"Please input your digital signature";
        view = self.noViewRegistrant;
        
        [self.popTip showText:message direction:AMPopTipDirectionNone maxWidth:300 inView:self.view fromFrame:view.frame];
        return;
    }
    
    if( !isTncRegistrant){
        message = @"Please agree to the disclaimer notices by ticking \nthe checkboxes";
        view = self.checkBoxRegistrantButton;
        
        [self.popTip showText:message direction:AMPopTipDirectionUp maxWidth:350 inView:self.view fromFrame:view.frame];
        return;
    }
    
    if(!isTncDST ){
        message = @"Please agree to the disclaimer notices by ticking \nthe checkboxes";
        view = self.checkBoxDSTButton;
        
        [self.popTip showText:message direction:AMPopTipDirectionUp maxWidth:350 inView:self.view fromFrame:view.frame];
        return;
    }
    
    
//    if(message.length > 0){
////        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
////        [alertView show];
//        
//        return;
//    }
    
    [self.registrant setSignDSTImage:[NGREGUtil getUIImageFromThisUIView:self.signInDSTView]];
    [self.registrant setSignImage:[NGREGUtil getUIImageFromThisUIView:self.signInRegistrantView]];
    
    NSString* menu = nil;
    
    if([self.menu objectForKey:@"name"] ){
        menu = [self.menu valueForKey:@"name"];
    }else{
        menu = [self.menu valueForKey:@"menuName"];
    }
    
//    [NGREGReport reportNewRegistrantWithEmail:[[NSUserDefaults standardUserDefaults] valueForKey:kNGREGEmailNextGen] andMenu:menu inManagedObjectContext:[appDelegate managedObjectContext]];
    
    [[NGREGEngine sharedEngine] requestRegisterRegistrant:self.registrant atEvent:menu withCompletionHandler:^(id result, NSError *error) {
        
    }];
    self.didFinishRegisterRegistrant(self.registrant);
}

- (IBAction)checkBoxTncRegistrant:(id)sender {
    isTncRegistrant = !isTncRegistrant;
    if(isTncRegistrant){
        [self.checkBoxRegistrantButton setSelected:YES];
    }else{
        [self.checkBoxRegistrantButton setSelected:NO];
    }
    [self changeButtonSubmit];
}

- (IBAction)checkBoxTncDST:(id)sender {
    isTncDST = !isTncDST;
    if(isTncDST){
        [self.checkBoxDSTButton setSelected:YES];
    }else{
        [self.checkBoxDSTButton setSelected:NO];
    }
    [self changeButtonSubmit];
}


#pragma mark - NGREGBottomPanelViewControllerDelegate methods

- (void)bottomPanelViewController:(NGREGBottomPanelViewController *)viewController didSelectMenuAtIndex:(NSInteger)index WithData:(id)menu{
    [self.navigationController popToRootViewControllerAnimated:NO];
}

@end
