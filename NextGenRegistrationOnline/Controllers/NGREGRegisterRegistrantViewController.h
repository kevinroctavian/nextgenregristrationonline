//
//  NGREGRegisterRegistrantViewController.h
//  NextGenRegistrationOnline
//
//  Created by Kevin Octavian on 1/29/14.
//  Copyright (c) 2014 Kevin R. Octavian. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NGREGCheckRegistrantResponse.h"
#import "NGREGGenericViewController.h"
#import "NGREGCheckRegistrantViewController.h"

#define MAXLENGTH_GIID_NUMBER 20
#define MAXLENGTH_FULL_NAME 60
#define MAXLENGTH_PHONE_NUMBER 15

@interface NGREGRegisterRegistrantViewController : NGREGGenericViewController

@property (strong, nonatomic) NSDictionary* menu;
@property (strong, nonatomic) NGREGCheckRegistrantResponse* registrant;

@property (strong) void (^didFinishRegisterRegistrant)(NGREGCheckRegistrantResponse* registrant);

@end
