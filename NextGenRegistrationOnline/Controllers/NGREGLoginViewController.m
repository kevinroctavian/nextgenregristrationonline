//
//  NGREGLoginViewController.m
//  NextGenRegistrationOnline
//
//  Created by Kevin Octavian on 1/16/14.
//  Copyright (c) 2014 Kevin R. Octavian. All rights reserved.
//

#import "NGREGLoginViewController.h"
#import "NGREGTheme+Additions.h"

@interface NGREGLoginViewController ()<UITextFieldDelegate>{

    NSArray *brands;
    CGRect _originalViewFrame;
 
    NGREGBrand* brand;
    
    WYPopoverController* _popoverBrand;
}

@property (weak, nonatomic) IBOutlet UITextField *emailText;
@property (weak, nonatomic) IBOutlet UITextField *passwordText;
@property (weak, nonatomic) IBOutlet UITextField *brandText;
@property (weak, nonatomic) IBOutlet UIView *loginView;
@property (weak, nonatomic) IBOutlet UILabel *dstMobileRegistrationLabel;
@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UIView *frontView;
@property (weak, nonatomic) IBOutlet UILabel *versionLabel;


@end

@implementation NGREGLoginViewController

- (float)avgWithData:(NSArray*) data andIndex:(int) index andSum:(float) sum{
    if(index == data.count){
        return (sum/index);
    }else{
        sum += [data[index] integerValue];
        return [self avgWithData:data andIndex:++index andSum:sum];
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSArray* data = @[@4, @4, @8, @8];
    
    NSLog(@"average : %f", [self avgWithData:data andIndex:0 andSum:0]);
 
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    
    self.emailText.delegate = self;
    self.passwordText.delegate = self;
    
//    [self.storyboard instantiateViewControllerWithIdentifier:@"apa coba"];
    
    NSString* email = [[NSUserDefaults standardUserDefaults] valueForKey:kNGREGEmailNextGen];
    if(email != nil){
        if([email length] > 0){
            [self navigateToHome];
            return;
        }
    }
    
    NSString *version = [NSString stringWithFormat:@"Version %@", [NSBundle mainBundle].infoDictionary[@"CFBundleVersion"]];
    self.versionLabel.text = version;

    
    [[NGREGEngine sharedEngine]requestBrandListWithCompletionHandler:^(id result, NSError *error) {
        if(error == nil)
            [self reloadBrands];
    }];
    
    [self reloadBrands];

    [NGREGUtil setFontBoldFromThisView:self.dstMobileRegistrationLabel andIdFont:99];
    [NGREGUtil setFontFromThisView:self.emailText andIdFont:99];
    [NGREGUtil setFontFromThisView:self.passwordText andIdFont:99];
    [NGREGUtil setFontFromThisView:self.brandText andIdFont:99];
    [NGREGUtil setFontFromThisView:self.versionLabel andIdFont:99];
    
    UIColor *color = [NGREGUtil colorFromHexString:@"#CCCCCC"];
    self.emailText.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Username" attributes:@{NSForegroundColorAttributeName: color}];
    self.passwordText.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Password" attributes:@{NSForegroundColorAttributeName: color}];
    [[UITextField appearance] setTintColor:[NGREGUtil colorFromHexString:@"#DDDDDD"]];
    
    self.contentView.layer.borderColor = [NGREGUtil colorFromHexString:@"#BBBBBB"].CGColor;
    self.contentView.layer.borderWidth = 0.6f;
    
//    // array
//    NSMutableArray *fontNames = [[NSMutableArray alloc] init];
//    
//    // get font family
//    NSArray *fontFamilyNames = [UIFont familyNames];
//    
//    // loop
//    for (NSString *familyName in fontFamilyNames)
//    {
//        NSLog(@"Font Family Name = %@", familyName);
//        
//        // font names under family
//        NSArray *names = [UIFont fontNamesForFamilyName:familyName];
//        
//        NSLog(@"Font Names = %@", fontNames);
//        
//        // add to array
//        [fontNames addObjectsFromArray:names];
//    }
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Helpher methods

- (void)reloadBrands{
    
    NGREGAppDelegate *appDelegate = (NGREGAppDelegate *)[[UIApplication sharedApplication] delegate];
    
    brands = [NGREGBrand brandInManagedObjectContext:[appDelegate managedObjectContext]];
    
    if([brands count] > 0){
        brand = brands[0];
        [self.brandText setText:[brand.brandName capitalizedString]];
        [self reloadThemeWithBrandId:brand.brandId];
    }
}

- (void)reloadThemeWithBrandId:(NSString*) brandId{
    [[NGREGEngine sharedEngine]requestBrandThemeWithBrandId:brandId andCompletionHandler:^(id result, NSError *error) {
        
        [self loadLocalThemeWithBrand:brandId];
        
    }];
    [self loadLocalThemeWithBrand:brandId];
}

- (void)loadLocalThemeWithBrand:(NSString*) brandName{
    NGREGAppDelegate *appDelegate = (NGREGAppDelegate *)[[UIApplication sharedApplication] delegate];
    NSArray* array = [NGREGTheme themeWith:brandName InManagedObjectContext:[appDelegate managedObjectContext]];
    if(array.count > 0){
        NGREGTheme* theme = array[array.count - 1];
        if([theme.brandId isEqualToString:brand.brandId])
            [self.dstMobileRegistrationLabel setText:theme.loginBrandName];
    }
        
}

- (void) navigateToHome{
    NGREGAppDelegate *appDelegate = (NGREGAppDelegate *)[[UIApplication sharedApplication] delegate];
    [appDelegate setRootViewControllerToHomeViewController];
}

#pragma mark - Action methods

- (IBAction)nextButtonClicked:(id)sender {
    
    NSString* message = @"";
    
    if(self.emailText.text.length == 0 && self.passwordText.text.length == 0 ){
//        message = @"Data must not be empty";
        // QC
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"Please input your username and password " delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertView show];
        return;
    }else if(brand == nil){
        message = @"Please select your brand preference";
    }else if ([self.emailText.text length] == 0) {
        message = @"Please input your username and try again";
        [self.emailText becomeFirstResponder];
    }else if ([self.passwordText.text length] == 0) {
        message = @"Please input your password and try again";
        [self.passwordText becomeFirstResponder];
    }
//    else if(self.emailText.text.length > 0)
//        if(![NGREGUtil validateEmailWithString:self.emailText.text]){
//            message = @"Invalid Email Input";
//            [self.emailText becomeFirstResponder];
//        }

    
    if (message.length > 0){
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertView show];
        return;
    }
    
    [self.view endEditing:YES];
    
    [DejalBezelActivityView activityViewForView:self.frontView.superview];
    
    
    [[NGREGEngine sharedEngine]requestLoginWithUsername:self.emailText.text andPassword:self.passwordText.text andBrandId:brand.brandId andWithCompletionHandler:^(id result, NSError *error) {
    
        [DejalBezelActivityView removeViewAnimated:YES];

        NSString *version = [NSBundle mainBundle].infoDictionary[@"CFBundleVersion"];
        
        NSString *message = @"";
        if(error == nil){
            NGREGLoginResponse *response = result;
            if([response.status intValue] == 1){
                
                if(![response.version isEqualToString:version]){
                    message = [NSString stringWithFormat:@"%@ %@", response.messageVersion, response.version];
                }else{
                    
                    [[NSUserDefaults standardUserDefaults] setValue:self.emailText.text forKey:kNGREGEmailNextGen];
                    [[NSUserDefaults standardUserDefaults] setValue:self.passwordText.text forKey:kNGREGPasswordNextGen];
                    [[NSUserDefaults standardUserDefaults] setValue:brand.brandId forKey:kNGREGBrandIDNextGen];
                    
                    //                [self navigateToHome];
                    
                    UIViewController *homeView = [self.storyboard instantiateViewControllerWithIdentifier:@"DownloadViewController"];
                    [self presentViewController:homeView animated:YES completion:nil];
                }
                
            }else{
                message = response.msg;
            }
        }else{
            message = @"No Internet Connection Detected";
        }
        
        if([message length]>0){
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alertView show];
        }
        
    }];
}

- (IBAction)hideKeyboardClicked:(id)sender {
    [self.view endEditing:YES];
}


- (IBAction)brandClicked:(id)sender {
    
    [self.view endEditing:YES];
    
    if(_popoverBrand != nil){
        [_popoverBrand dismissPopoverAnimated:YES];
    }
    
    NGREGBrandPopoverViewController *datePickerViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"BrandPopover"];
    
    _popoverBrand = [[WYPopoverController alloc] initWithContentViewController:datePickerViewController];
    
    int height = (brands == nil) ? kNGREGBrandPopoverHeight : (brands.count * kNGREGBrandPopoverHeightItemCell);
    height = height > kNGREGBrandPopoverHeightMax ? kNGREGBrandPopoverHeightMax : height;
    
    datePickerViewController.preferredContentSize = CGSizeMake(kNGREGBrandPopoverWidth, height);
    
    [datePickerViewController setBrands:brands];
    
    [datePickerViewController setSelectedBrand:^(NGREGBrand * brandSelected) {
        
        brand = brandSelected;
        [self.brandText setText:[brandSelected.brandName capitalizedString]];
        [_popoverBrand dismissPopoverAnimated:YES];
        [self reloadThemeWithBrandId:brand.brandId];
        
    }];
    
    
    [[WYPopoverBackgroundView appearance] setFillTopColor:[UIColor whiteColor]];
    
    _popoverBrand.passthroughViews = @[sender];
    _popoverBrand.popoverLayoutMargins = UIEdgeInsetsMake(0.0f, 0.0f, 0.0f, 0.0f);
    _popoverBrand.wantsDefaultContentAppearance = NO;
    [_popoverBrand presentPopoverFromRect:self.brandText.bounds inView:self.brandText permittedArrowDirections:WYPopoverArrowDirectionAny animated:YES];
}

#pragma mark - Keyboard Management

- (void)keyboardWillShow:(NSNotification *)notification {
    
    NSDictionary* userInfo = [notification userInfo];
    
    // we don't use SDK constants here to be universally compatible with all SDKs ≥ 3.0
    NSValue* keyboardFrameValue = [userInfo objectForKey:@"UIKeyboardBoundsUserInfoKey"];
    if (!keyboardFrameValue) {
        keyboardFrameValue = [userInfo objectForKey:@"UIKeyboardFrameEndUserInfoKey"];
    }
    
    _originalViewFrame = self.loginView.frame;
    
    CGRect frame = CGRectMake(_originalViewFrame.origin.x, -170.0f, _originalViewFrame.size.width, self.view.superview.height -[keyboardFrameValue CGRectValue].size.height -16.0f);
    
    CGFloat animationDuration = [[userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] floatValue];
    [UIView animateWithDuration:animationDuration animations:^{
        
        self.loginView.frame = frame;
    }];
}

- (void)keyboardWillHide:(NSNotification *)notification {
    
	NSDictionary* userInfo = [notification userInfo];
    CGFloat animationDuration = [[userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] floatValue];
    
    [UIView animateWithDuration:animationDuration animations:^{
        
		self.loginView.frame = _originalViewFrame;
        
    } completion:^(BOOL finished) {
        
    }];
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if(textField == self.emailText){
        [self.passwordText becomeFirstResponder];
    }
    if(textField == self.passwordText){
        [self.passwordText resignFirstResponder];
        [self nextButtonClicked:self.passwordText];
    }
    return YES;
}


@end
