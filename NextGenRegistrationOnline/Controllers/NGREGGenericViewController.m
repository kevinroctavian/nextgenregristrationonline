//
//  NGREGGenericViewController.m
//  NextGenRegistrationOnline
//
//  Created by Kevin Octavian on 10/15/14.
//  Copyright (c) 2014 Kevin R. Octavian. All rights reserved.
//

#import "NGREGGenericViewController.h"
#import "NSData+MD5.h"
#import "FileDownloadInfo.h"
#import "NGREGAppDelegate.h"
#import "NSString+AES.h"
#import <CommonCrypto/CommonCryptor.h>

#import "NGREGRegistrant+Additions.h"

@interface NGREGGenericViewController ()<NSURLSessionDelegate>

@property (nonatomic, strong) NSURLSession *session;
@property (nonatomic, strong) NSMutableArray *arrFileDownloadData;
@property (nonatomic, strong) NSMutableArray *views;
@property (nonatomic, strong) NSMutableArray *states;

@end

@implementation NGREGGenericViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    NSArray *URLs = [[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask];
    self.docDirectoryURL = [URLs objectAtIndex:0];
    
    self.arrFileDownloadData = [[NSMutableArray alloc] init];
    self.views = [[NSMutableArray alloc] init];
    self.states = [[NSMutableArray alloc] init];
    
    
    NSURLSessionConfiguration *sessionConfiguration = [NSURLSessionConfiguration backgroundSessionConfiguration:@"com.kana.ios.Touchbase.generic"];
    sessionConfiguration.HTTPMaximumConnectionsPerHost = 5;
    
    
    self.session = [NSURLSession sessionWithConfiguration:sessionConfiguration
                                                 delegate:self
                                            delegateQueue:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void) loadImageButton:(UIButton*)button forState:(UIControlState)state withUrl:(NSString*)url andMD5:(NSString*) md5{
    
    
    
    [self.views addObject:button];
    [self.states addObject:[NSNumber numberWithInteger:state]];
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    NSArray *parts = [url componentsSeparatedByString:@"/"];
    NSString *filename = [parts objectAtIndex:[parts count]-1];
    NSURL *destinationURL = [self.docDirectoryURL URLByAppendingPathComponent:filename];
    
    if ([fileManager fileExistsAtPath:[destinationURL path]]) {
        // load image dari file
        UIImage* image = [[UIImage alloc] initWithContentsOfFile:[destinationURL path]];
        
        [button setBackgroundImage:image forState:state];
        
        NSData *nsData = [NSData dataWithContentsOfFile:[destinationURL path]];
        if (nsData){
            NSString* md5File = [nsData MD5];
            NSLog(@"MD5-nya adalah %@", md5File);
            if(![md5File isEqualToString:md5]){
                // download yang terbaru
                [self download:url];
            }
        }
    }else{
        // download file
        [self download:url];
    }
}

- (void) loadImage:(UIImageView*)imageView withUrl:(NSString*)url andMD5:(NSString*) md5{
    
    
    [self.views addObject:imageView];
    
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    NSArray *parts = [url componentsSeparatedByString:@"/"];
    NSString *filename = [parts objectAtIndex:[parts count]-1];
    NSURL *destinationURL = [self.docDirectoryURL URLByAppendingPathComponent:filename];
    
    if ([fileManager fileExistsAtPath:[destinationURL path]]) {
        // load image dari file
        imageView.image = [[UIImage alloc] initWithContentsOfFile:[destinationURL path]];
        
        NSData *nsData = [NSData dataWithContentsOfFile:[destinationURL path]];
        if (nsData){
            NSString* md5File = [nsData MD5];
            NSLog(@"MD5-nya adalah %@", md5File);
            if(![md5File isEqualToString:md5]){
                // download yang terbaru
                [self download:url];
            }
        }
    }else{
        // download file
        [self download:url];
    }
    
}

- (void)download:(NSString*)url{
    FileDownloadInfo* fdi = [[FileDownloadInfo alloc] initWithFileTitle:@"image" andDownloadSource:url];
    if (fdi.taskIdentifier == -1) {
        // If the taskIdentifier property of the fdi object has value -1, then create a new task
        // providing the appropriate URL as the download source.
        fdi.downloadTask = [self.session downloadTaskWithURL:[NSURL URLWithString:fdi.downloadSource]];
        
        // Keep the new task identifier.
        fdi.taskIdentifier = fdi.downloadTask.taskIdentifier;
        
        // Start the task.
        [fdi.downloadTask resume];
    }
    else{
        // Create a new download task, which will use the stored resume data.
        fdi.downloadTask = [self.session downloadTaskWithResumeData:fdi.taskResumeData];
        [fdi.downloadTask resume];
        
        // Keep the new download task identifier.
        fdi.taskIdentifier = fdi.downloadTask.taskIdentifier;
    }
    fdi.isDownloading = !fdi.isDownloading;
    
    [self.arrFileDownloadData addObject:fdi];
}

- (int)getFileDownloadInfoIndexWithTaskIdentifier:(unsigned long)taskIdentifier{
    int index = 0;
    for (int i=0; i<[self.arrFileDownloadData count]; i++) {
        FileDownloadInfo *fdi = [self.arrFileDownloadData objectAtIndex:i];
        if (fdi.taskIdentifier == taskIdentifier) {
            index = i;
            break;
        }
    }
    
    return index;
}

#pragma mark - NSURLSession Delegate method implementation

-(void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask didFinishDownloadingToURL:(NSURL *)location{
    
    NSError *error;
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    NSString *destinationFilename = downloadTask.originalRequest.URL.lastPathComponent;
    NSURL *destinationURL = [self.docDirectoryURL URLByAppendingPathComponent:destinationFilename];
    
    if ([fileManager fileExistsAtPath:[destinationURL path]]) {
        [fileManager removeItemAtURL:destinationURL error:nil];
    }
    
    BOOL success = [fileManager copyItemAtURL:location
                                        toURL:destinationURL
                                        error:&error];
    
    if (success) {
        // Change the flag values of the respective FileDownloadInfo object.
        int index = [self getFileDownloadInfoIndexWithTaskIdentifier:downloadTask.taskIdentifier];
        
        if(self.arrFileDownloadData.count ==0)
            return;
        
        FileDownloadInfo *fdi = [self.arrFileDownloadData objectAtIndex:index];
        
        fdi.isDownloading = NO;
        fdi.downloadComplete = YES;
        
        // Set the initial value to the taskIdentifier property of the fdi object,
        // so when the start button gets tapped again to start over the file download.
        fdi.taskIdentifier = -1;
        
        // In case there is any resume data stored in the fdi object, just make it nil.
        fdi.taskResumeData = nil;
        
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            NSObject* view = self.views[index];
            if([view isKindOfClass:[UIImageView class]])
                ((UIImageView*)view).image = [[UIImage alloc] initWithContentsOfFile:[destinationURL path]];
            else if([view isKindOfClass:[UIButton class]]){
                UIImage* image = [[UIImage alloc] initWithContentsOfFile:[destinationURL path]];
                
                [((UIButton*)view) setBackgroundImage:image forState:(UIControlState)self.states[index]];
            }
        }];
        
    }
    else{
        NSLog(@"Unable to copy temp file. Error: %@", [error localizedDescription]);
    }
}


-(void)URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task didCompleteWithError:(NSError *)error{
    if (error != nil) {
        NSLog(@"Download completed with error: %@", [error localizedDescription]);
    }
    else{
        NSLog(@"Download finished successfully.");
    }
}


-(void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask didWriteData:(int64_t)bytesWritten totalBytesWritten:(int64_t)totalBytesWritten totalBytesExpectedToWrite:(int64_t)totalBytesExpectedToWrite{
    
    if (totalBytesExpectedToWrite == NSURLSessionTransferSizeUnknown) {
        NSLog(@"Unknown transfer size");
    }
    else{
        // Locate the FileDownloadInfo object among all based on the taskIdentifier property of the task.
        int index = [self getFileDownloadInfoIndexWithTaskIdentifier:downloadTask.taskIdentifier];
        
        if(self.arrFileDownloadData.count ==0)
            return;
        FileDownloadInfo *fdi = [self.arrFileDownloadData objectAtIndex:index];
        
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            // Calculate the progress.
            fdi.downloadProgress = (double)totalBytesWritten / (double)totalBytesExpectedToWrite;
            
            // Get the progress view of the appropriate cell and update its progress.
            //            UITableViewCell *cell = [self.tblFiles cellForRowAtIndexPath:[NSIndexPath indexPathForRow:index inSection:0]];
            //            UIProgressView *progressView = (UIProgressView *)[cell viewWithTag:CellProgressBarTagValue];
            //            progressView.progress = fdi.downloadProgress;
            //            [progressView setProgress:(currentProgressDownloadPerFile + (fdi.downloadProgress/(float)self.arrFileDownloadData.count))];
            
        }];
    }
}


-(void)URLSessionDidFinishEventsForBackgroundURLSession:(NSURLSession *)session{
    NGREGAppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
    
    // Check if all download tasks have been finished.
    [self.session getTasksWithCompletionHandler:^(NSArray *dataTasks, NSArray *uploadTasks, NSArray *downloadTasks) {
        
        if ([downloadTasks count] == 0) {
            if (appDelegate.backgroundTransferCompletionHandler != nil) {
                // Copy locally the completion handler.
                void(^completionHandler)() = appDelegate.backgroundTransferCompletionHandler;
                
                // Make nil the backgroundTransferCompletionHandler.
                appDelegate.backgroundTransferCompletionHandler = nil;
                
                [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                    // Call the completion handler to tell the system that there are no other background transfers.
                    completionHandler();
                    
                    // Show a local notification when all downloads are over.
                    
                }];
            }
        }
    }];
}

- (void)cekLogging{
    
    NGREGAppDelegate* appDelegate = (NGREGAppDelegate *)[[UIApplication sharedApplication] delegate];
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    NSString *filename = @"registrant.data";
    NSURL *destinationURL = [self.docDirectoryURL URLByAppendingPathComponent:filename];
    
    NSLog(@"Lokasi file : %@", [destinationURL path]);
    
    //kalo ada file di clear terlebih dahulu
    if ([fileManager fileExistsAtPath:[destinationURL path]]) {
        [fileManager removeItemAtURL:destinationURL error:nil];
    }
    
    if (![fileManager fileExistsAtPath:[destinationURL path]]) {
        [fileManager createFileAtPath:[destinationURL path] contents:nil attributes:nil];
        NSLog(@"Route creato");
    }
    
    NSMutableString *writeString = [NSMutableString stringWithCapacity:0]; //don't worry about the capacity, it will expand as necessary
    
    NSArray* array = [NGREGRegistrant registrantBefore2WeeksinManagedObjectContext:[appDelegate managedObjectContext]];
    
    //title
    [writeString appendString:[NSString stringWithFormat:@"%@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@ \n",@"eventId", @"userId", @"registerDate", @"registrantID", @"fullname", @"dateOfBirth", @"email", @"gIIDType", @"gIIDNumber", @"mobile", @"socialAccount", @"sex", @"city", @"brand1Id" ]];
    
    for(NGREGRegistrant* registrant in array){
        [writeString appendString:[NSString stringWithFormat:@"%@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@, %@ \n",registrant.eventId, registrant.userId, registrant.registerDate, registrant.registrationID,registrant.fullName, registrant.dateOfBirth, registrant.email, registrant.gIIDType, registrant.gIIDNumber, registrant.mobile, registrant.socialAccount, registrant.sex, registrant.city, registrant.brand1Id]]; //the \n will put a newline in
    }
    
    
    //Moved this stuff out of the loop so that you write the complete string once and only once.
    NSLog(@"writeString :%@",writeString);
    
    NSString* encryptString = [writeString AES128EncryptWithKey:@"0jw82bsk2md"];
    NSLog(@"encyptString :%@",encryptString);
    
    NSFileHandle *handle;
    handle = [NSFileHandle fileHandleForWritingAtPath: [destinationURL path] ];
    //say to handle where's the file fo write
    [handle truncateFileAtOffset:[handle seekToEndOfFile]];
    //position handle cursor to the end of file
    [handle writeData:[encryptString dataUsingEncoding:NSUTF8StringEncoding]];
    
}

@end
