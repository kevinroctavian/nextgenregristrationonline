//
//  NGREGDownloadViewController.m
//  NextGenRegistrationOnline
//
//  Created by Kevin Octavian on 2/10/14.
//  Copyright (c) 2014 Kevin R. Octavian. All rights reserved.
//

#import "NGREGDownloadViewController.h"
#import "LDProgressView.h"
#import "NGREGEngine.h"
#import "NGREGAppDelegate.h"
#import "NGREGProfile+Additions.h"
#import "NGREGUtil.h"
#import "UIAlertView+BlockExtensions.h"
#import "FileDownloadInfo.h"
#import "NSData+MD5.h"
#import "Reachability.h"

@interface NGREGDownloadViewController ()<NSURLSessionDelegate>{
    NGREGProfile *profile;
    LDProgressView *progressView ;
    int fileDownloadCount;
    int indexDownload;
    float currentProgressDownloadPerFile;
    NSMutableArray * fileVideoToDownload;
    NSMutableArray * fileImageToDownload;
    NSMutableSet * processed;
}
@property (weak, nonatomic) IBOutlet UILabel *loadingLabel;
@property (weak, nonatomic) IBOutlet UILabel *downloadLabel;

@property (nonatomic, strong) NSURLSession *session;
@property (nonatomic, strong) NSMutableArray *arrFileDownloadData;
@property (nonatomic, strong) NSURL *docDirectoryURL;

@end

@implementation NGREGDownloadViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.loadingLabel setText:@""];
    
    
    NSArray *URLs = [[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask];
    self.docDirectoryURL = [URLs objectAtIndex:0];
    
    // flat, green, no text, progress inset, outer stroke, solid
    progressView = [[LDProgressView alloc] initWithFrame:CGRectMake(100, 385, 824, 10)];
    progressView.progress = 0.00;
    progressView.animate = @YES;
    progressView.type = LDProgressGradient;
    [self.view addSubview:progressView];
    
    [self initFirst];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Helpher methods

- (void)initFirst{
    
    NSURLSessionConfiguration *sessionConfiguration = [NSURLSessionConfiguration backgroundSessionConfiguration:@"com.kana.ios.Touchbase.download"];
    sessionConfiguration.HTTPMaximumConnectionsPerHost = 10;
    
    self.session = [NSURLSession sessionWithConfiguration:sessionConfiguration
                                                 delegate:self
                                            delegateQueue:nil];
    
    self.arrFileDownloadData = [[NSMutableArray alloc] init];
    
    [NGREGUtil setFontFromThisView:self.loadingLabel andIdFont:99];
    
    [[NGREGEngine sharedEngine]requestMyProfileWithCompletionHandler:^(id result, NSError *error) {
        
        NSArray* ar = [NGREGProfile profileWithEmail:[[NSUserDefaults standardUserDefaults] valueForKey:kNGREGEmailNextGen] InManagedObjectContext:[(NGREGAppDelegate *)[[UIApplication sharedApplication] delegate] managedObjectContext]];
        profile = [ar lastObject];
        
        
        NSArray* fileVideo = profile.menu;
        
        fileDownloadCount = 6;
        
        currentProgressDownloadPerFile = 0;
        
        
        fileDownloadCount += fileVideo.count;
        
        [self addFileSessionToDownload:@"image" andURL:[profile.customizeTemplates valueForKey:@"global"] andMD5:[profile.customizeTemplates valueForKey:@"global_md5"]];
        [self addFileSessionToDownload:@"image" andURL:[profile.customizeTemplates valueForKey:@"home"] andMD5:[profile.customizeTemplates valueForKey:@"home_md5"]];
        [self addFileSessionToDownload:@"image" andURL:[profile.customizeTemplates valueForKey:@"button-submit-disable"] andMD5:[profile.customizeTemplates valueForKey:@"button-submit-disable_md5"]];
        [self addFileSessionToDownload:@"image" andURL:[profile.customizeTemplates valueForKey:@"search"] andMD5:[profile.customizeTemplates valueForKey:@"search_md5"]];
        [self addFileSessionToDownload:@"image" andURL:[profile.customizeTemplates valueForKey:@"button-menu"] andMD5:[profile.customizeTemplates valueForKey:@"button-menu_md5"]];
        [self addFileSessionToDownload:@"image" andURL:[profile.customizeTemplates valueForKey:@"button-submit"] andMD5:[profile.customizeTemplates valueForKey:@"button-submit_md5"]];
    
        for(int i=0;i<[fileVideo count];i++){
            NSDictionary* menu = profile.menu[i];
            
            NSString* url = [[menu valueForKey:@"video"] valueForKey:@"url"];
            NSString* md5 = [[menu valueForKey:@"video"] valueForKey:@"filename_md5"];
            
            [self addFileSessionToDownload:@"video" andURL:url andMD5:md5];
            
        }
        
        progressView.color = [NGREGUtil colorFromHexString:[profile.customizeTemplates valueForKey:@"text-label-menu-color"]];
        progressView.background = [[NGREGUtil colorFromHexString:[profile.customizeTemplates valueForKey:@"text-label-menu-background-color"]] colorWithAlphaComponent:0.8];
        
        [progressView setProgress:currentProgressDownloadPerFile];
        
        [self.loadingLabel setText:[NSString stringWithFormat:@"Downloading visual assets from server. Please wait a moment."]];
        
        if(self.arrFileDownloadData.count > 0){
            [self downloadFileSessionAt:0];
        }else{
            [self.session finishTasksAndInvalidate];
            NGREGAppDelegate *appDelegate = (NGREGAppDelegate *)[[UIApplication sharedApplication] delegate];
            [appDelegate setRootViewControllerToHomeViewController];
        }
        //        [self downloadImageAtIndex:0];
        
    }];
}

- (void)addFileSessionToDownload:(NSString*)title andURL:(NSString*)url andMD5:(NSString*)md5{
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    NSArray *parts = [url componentsSeparatedByString:@"/"];
    NSString *filename = [parts objectAtIndex:[parts count]-1];
    NSURL *destinationURL = [self.docDirectoryURL URLByAppendingPathComponent:filename];
    
    if ([fileManager fileExistsAtPath:[destinationURL path]]) {
        NSData *nsData = [NSData dataWithContentsOfFile:[destinationURL path]];
        if (nsData){
            NSString* md5File = [nsData MD5];
            NSLog(@"MD5-nya adalah %@", md5File);
            if([md5File isEqualToString:md5]){
                currentProgressDownloadPerFile += (1.0/(float)fileDownloadCount);
                return;
            }
        }
    }
    
    [self.arrFileDownloadData addObject:[[FileDownloadInfo alloc] initWithFileTitle:title andDownloadSource:url]];

}



- (void)addFileImageToDownload:(NSString*)url{
    if ([processed containsObject:url] == NO) {
        [fileImageToDownload addObject:url];
        [processed addObject:url];
    }
}

//- (void)downloadImageAtIndex:(NSInteger) index{
//    
//    if(index >= [fileImageToDownload count]){
//        [self downloadVideoAtIndex:0];
//        return;
//    }
//    
//    NSString* imageURL = fileImageToDownload[index];
//    
//    
//    __block NSInteger _index = index;
//    
//    SDWebImageManager *manager = [SDWebImageManager sharedManager];
//    [manager downloadWithURL:[NSURL URLWithString:imageURL] options:SDWebImageRetryFailed progress:^(NSInteger receivedSize, NSInteger expectedSize) {
//        float progress = (float)receivedSize / expectedSize;
//        
//        NSLog(@"Download = %f and Progress View = %f", progress, (progress/(float)fileDownloadCount));
//        
//        [progressView setProgress:(currentProgressDownloadPerFile + (progress/(float)fileDownloadCount))];
//    } completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished) {
//        
//        
//        if(error == nil){
//            
//            currentProgressDownloadPerFile += (1.0/(float)fileDownloadCount);
//            
//            [self downloadImageAtIndex:++_index];
//        }else{
//            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"Please check your connection and try again" completionBlock:^(NSUInteger buttonIndex, UIAlertView *alertView) {
//                
//                if ([[alertView buttonTitleAtIndex:buttonIndex] isEqualToString:@"Retry"]) {
//                    [self downloadImageAtIndex:++_index];
//                }else{
//                    [self dismissViewControllerAnimated:YES completion:NO];
//                    return;
//                }
//                
//            } cancelButtonTitle:@"Cancel" otherButtonTitles:@"Retry", nil];
//            [alertView show];
//        }
//    }];
//}

- (void)downloadVideoAtIndex:(NSInteger)index{
    
    if(index >=  [fileVideoToDownload count]){
        NGREGAppDelegate *appDelegate = (NGREGAppDelegate *)[[UIApplication sharedApplication] delegate];
        [appDelegate setRootViewControllerToHomeViewController];
        return;
    }
    
    NSString* url = fileVideoToDownload[index];
    
    NSArray *parts = [url componentsSeparatedByString:@"/"];
    NSString *pdfName = [parts objectAtIndex:[parts count]-1];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:url] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:30.0f];
    //    NSString *pdfName = [[menu valueForKey:@"video"] valueForKey:@"filename"];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *path = [[paths objectAtIndex:0] stringByAppendingPathComponent:pdfName];
    
    NSFileManager *fileManager = [[NSFileManager alloc] init];
    BOOL isDir;
    BOOL fileExists = [fileManager fileExistsAtPath:path isDirectory:&isDir];
    
    if (fileExists){
        [self downloadVideoAtIndex:++index];
        return;
    }
    
    
    //    [self.loadingLabel setText:[NSString stringWithFormat:@"Downloading visual assets (%@) from server. Please wait a moment.", pdfName]];
    
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    path = [[paths objectAtIndex:0] stringByAppendingPathComponent:pdfName];
    operation.outputStream = [NSOutputStream outputStreamToFileAtPath:path append:YES];
    
    
    __block NSInteger _index = index;
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         NSLog(@"Successfully downloaded file to %@", path);
         
         [self downloadVideoAtIndex:++_index];
         
         currentProgressDownloadPerFile += (1.0/(float)fileDownloadCount);
     } failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         NSLog(@"Error: %@", error);
         if ([[NSFileManager defaultManager] isDeletableFileAtPath:path]) {
             BOOL success = [[NSFileManager defaultManager] removeItemAtPath:path error:&error];
             if (!success) {
                 NSLog(@"Error removing file at path: %@", error.localizedDescription);
             }else{
                 NSLog(@"Success removing file at path: %@", error.localizedDescription);
             }
         }
         //Oops, We can’t Seem to Connect to the Internet
         UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"Please check your connection and try again" completionBlock:^(NSUInteger buttonIndex, UIAlertView *alertView) {
             
             if ([[alertView buttonTitleAtIndex:buttonIndex] isEqualToString:@"Retry"]) {
                 [self downloadVideoAtIndex:_index];
             }else{
                 [self dismissViewControllerAnimated:YES completion:NO];
                 return;
             }
             
         } cancelButtonTitle:@"Cancel" otherButtonTitles:@"Retry", nil];
         [alertView show];
         
     }];
    [operation setDownloadProgressBlock:^(NSUInteger bytesRead, long long totalBytesRead, long long totalBytesExpectedToRead)
     {
         
         float progress = (float)totalBytesRead / totalBytesExpectedToRead;
         
         [progressView setProgress:currentProgressDownloadPerFile + (progress/(float)fileDownloadCount)];
         
         
         NSLog(@"Download = %f", progress);
     }];
    [operation start];
}

- (int)getFileDownloadInfoIndexWithTaskIdentifier:(unsigned long)taskIdentifier{
    int index = 0;
    for (int i=0; i<[self.arrFileDownloadData count]; i++) {
        FileDownloadInfo *fdi = [self.arrFileDownloadData objectAtIndex:i];
        if (fdi.taskIdentifier == taskIdentifier) {
            index = i;
            break;
        }
    }
    
    return index;
}
- (void)downloadFileSessionAt:(NSInteger)i{
    
    indexDownload = i;
    
    FileDownloadInfo *fdi = [self.arrFileDownloadData objectAtIndex:i];
    if (fdi.taskIdentifier == -1) {
        // If the taskIdentifier property of the fdi object has value -1, then create a new task
        // providing the appropriate URL as the download source.
        fdi.downloadTask = [self.session downloadTaskWithURL:[NSURL URLWithString:fdi.downloadSource]];
        
        // Keep the new task identifier.
        fdi.taskIdentifier = fdi.downloadTask.taskIdentifier;
        
        // Start the task.
        [fdi.downloadTask resume];
    }
    else{
        // Create a new download task, which will use the stored resume data.
        fdi.downloadTask = [self.session downloadTaskWithResumeData:fdi.taskResumeData];
        [fdi.downloadTask resume];
        
        // Keep the new download task identifier.
        fdi.taskIdentifier = fdi.downloadTask.taskIdentifier;
    }
    fdi.isDownloading = !fdi.isDownloading;
}




#pragma mark - NSURLSession Delegate method implementation

-(void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask didFinishDownloadingToURL:(NSURL *)location{
    
    NSError *error;
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    NSString *destinationFilename = downloadTask.originalRequest.URL.lastPathComponent;
    NSURL *destinationURL = [self.docDirectoryURL URLByAppendingPathComponent:destinationFilename];
    
    if ([fileManager fileExistsAtPath:[destinationURL path]]) {
        [fileManager removeItemAtURL:destinationURL error:nil];
    }
    
    BOOL success = [fileManager copyItemAtURL:location
                                        toURL:destinationURL
                                        error:&error];
    
    if (success) {
        // Change the flag values of the respective FileDownloadInfo object.
        int index = [self getFileDownloadInfoIndexWithTaskIdentifier:downloadTask.taskIdentifier];
        
        
        if(self.arrFileDownloadData.count ==0)
            return;
        
        FileDownloadInfo *fdi = [self.arrFileDownloadData objectAtIndex:index];
        
        fdi.isDownloading = NO;
        fdi.downloadComplete = YES;
        
        // Set the initial value to the taskIdentifier property of the fdi object,
        // so when the start button gets tapped again to start over the file download.
        fdi.taskIdentifier = -1;
        
        // In case there is any resume data stored in the fdi object, just make it nil.
        fdi.taskResumeData = nil;
        
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            // Reload the respective table view row using the main thread.
//            [self.tblFiles reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:index inSection:0]]
//                                 withRowAnimation:UITableViewRowAnimationNone];
            if((index + 1) < [self.arrFileDownloadData count]){
                currentProgressDownloadPerFile += (1.0/(float)fileDownloadCount);
                [self downloadFileSessionAt:(index+1)];
            }else{
                currentProgressDownloadPerFile = 0;
                [self.session finishTasksAndInvalidate];
                NGREGAppDelegate *appDelegate = (NGREGAppDelegate *)[[UIApplication sharedApplication] delegate];
                [appDelegate setRootViewControllerToHomeViewController];
            }
        }];
        
    }
    else{
        NSLog(@"Unable to copy temp file. Error: %@", [error localizedDescription]);
    }
}


-(void)URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task didCompleteWithError:(NSError *)error{
    if (error != nil) {
        
        Reachability *reach = [Reachability reachabilityForInternetConnection];
        NetworkStatus status = [reach currentReachabilityStatus];
        
        NSString* message = @"";
        
        switch(status) {
            case NotReachable:
                message = @"Please check your connection and try again";
                break;
            case ReachableViaWiFi:
//                self.offlineImage.image = [UIImage imageNamed:@"iPad_OnlineIcon.png"];
                break;
            case ReachableViaWWAN:
//                self.offlineImage.image = [UIImage imageNamed:@"iPad_OnlineIcon.png"];
                break;
            default:
                message = @"Please check your connection and try again";
                break;
        }
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            if(message.length > 0){
                
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"Please check your connection and try again" completionBlock:^(NSUInteger buttonIndex, UIAlertView *alertView) {
                    
                    if ([[alertView buttonTitleAtIndex:buttonIndex] isEqualToString:@"Retry"]) {
                        [self downloadFileSessionAt:indexDownload];
                    }else{
                        [self dismissViewControllerAnimated:YES completion:NO];
                        return;
                    }
                    
                } cancelButtonTitle:@"Cancel" otherButtonTitles:@"Retry", nil];
                [alertView show];
            }else{
//                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:[NSString stringWithFormat:@"Download completed with error: %@", [error localizedDescription]] completionBlock:^(NSUInteger buttonIndex, UIAlertView *alertView) {
//                    
                    [self initFirst];
                    
//                } cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//                [alertView show];
            }
        }];
    }
    else{
        NSLog(@"Download finished successfully.");
    }
}


-(void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask didWriteData:(int64_t)bytesWritten totalBytesWritten:(int64_t)totalBytesWritten totalBytesExpectedToWrite:(int64_t)totalBytesExpectedToWrite{
    
    if (totalBytesExpectedToWrite == NSURLSessionTransferSizeUnknown) {
        NSLog(@"Unknown transfer size");
    }
    else{
        // Locate the FileDownloadInfo object among all based on the taskIdentifier property of the task.
        int index = [self getFileDownloadInfoIndexWithTaskIdentifier:downloadTask.taskIdentifier];
        
        if(self.arrFileDownloadData.count ==0)
            return;
        
        FileDownloadInfo *fdi = [self.arrFileDownloadData objectAtIndex:index];
        
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            // Calculate the progress.
            fdi.downloadProgress = (float)totalBytesWritten / (float)totalBytesExpectedToWrite;
            
            // Get the progress view of the appropriate cell and update its progress.
//            UITableViewCell *cell = [self.tblFiles cellForRowAtIndexPath:[NSIndexPath indexPathForRow:index inSection:0]];
//            UIProgressView *progressView = (UIProgressView *)[cell viewWithTag:CellProgressBarTagValue];
//            progressView.progress = fdi.downloadProgress;
            float progress = (currentProgressDownloadPerFile + ((float)fdi.downloadProgress/(float)fileDownloadCount));
            NSLog(@"Progress : %f", progress);
            if(progress  >= progressView.progress || progressView.progress == 0)
                [progressView setProgress:progress];
            if(progress > 1){
                [session finishTasksAndInvalidate];
                NGREGAppDelegate *appDelegate = (NGREGAppDelegate *)[[UIApplication sharedApplication] delegate];
                [appDelegate setRootViewControllerToHomeViewController];
            }
            
        }];
    }
}


-(void)URLSessionDidFinishEventsForBackgroundURLSession:(NSURLSession *)session{
    NGREGAppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
    
    // Check if all download tasks have been finished.
    [self.session getTasksWithCompletionHandler:^(NSArray *dataTasks, NSArray *uploadTasks, NSArray *downloadTasks) {
        
        if ([downloadTasks count] == 0) {
            if (appDelegate.backgroundTransferCompletionHandler != nil) {
                // Copy locally the completion handler.
                void(^completionHandler)() = appDelegate.backgroundTransferCompletionHandler;
                
                // Make nil the backgroundTransferCompletionHandler.
                appDelegate.backgroundTransferCompletionHandler = nil;
                
                [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                    // Call the completion handler to tell the system that there are no other background transfers.
                    completionHandler();
                    
                    // Show a local notification when all downloads are over.
                   
                    if((indexDownload + 1) < [self.arrFileDownloadData count]){
                        currentProgressDownloadPerFile += (1.0/(float)fileDownloadCount);
                        [self downloadFileSessionAt:(indexDownload+1)];
                    }else{
                        currentProgressDownloadPerFile = 0;
                        [session finishTasksAndInvalidate];
                        UILocalNotification *localNotification = [[UILocalNotification alloc] init];
                        localNotification.alertBody = @"All files have been downloaded!";
                        [[UIApplication sharedApplication] presentLocalNotificationNow:localNotification];
                    }
                }];
            }
        }else{
//            UILocalNotification *localNotification = [[UILocalNotification alloc] init];
//            localNotification.alertBody = @"All downloading files have been paused!";
//            [[UIApplication sharedApplication] presentLocalNotificationNow:localNotification];
        }
    }];
}


@end
