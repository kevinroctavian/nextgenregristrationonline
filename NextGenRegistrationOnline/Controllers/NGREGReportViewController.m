//
//  NGREGReportViewController.m
//  NextGenRegistrationOnline
//
//  Created by Kevin Octavian on 1/24/14.
//  Copyright (c) 2014 Kevin R. Octavian. All rights reserved.
//

#import "NGREGReportViewController.h"
#import "NGREGBottomPanelViewController.h"
#import "NGREGAppDelegate.h"
#import "NGREGProfile+Additions.h"
#import "NGREGEngine.h"
#import "NGREGReport+Additions.h"
#import "NGREGEventPopoverViewController.h"
#import "NGREGSubmission+Additions.h"

@interface NGREGReportViewController ()<NGREGBottomPanelViewControllerDelegate, WYPopoverControllerDelegate>{
    NGREGAppDelegate *appDelegate;
    NSArray* events;
    WYPopoverController *_popoverSubBrandViewController;
    NGREGProfile *profile;
}


@property (weak, nonatomic) IBOutlet UILabel *reportLabel;
@property (weak, nonatomic) IBOutlet UILabel *eventLabel;
@property (weak, nonatomic) IBOutlet UILabel *existingRegistrantLabel;
@property (weak, nonatomic) IBOutlet UILabel *totalRegistrantLabel;
@property (weak, nonatomic) IBOutlet UILabel *pendingDataRegistrationLabel;
@property (weak, nonatomic) IBOutlet UILabel *pendingDataGameLabel;
@property (weak, nonatomic) IBOutlet UILabel *registrationsCategoryLabel;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *approvedLabel;
@property (weak, nonatomic) IBOutlet UILabel *rejectedLabel;
@property (weak, nonatomic) IBOutlet UILabel *submissionsLabel;

@property (weak, nonatomic) IBOutlet UILabel *totalRegistrationCaptionLabel;
@property (weak, nonatomic) IBOutlet UILabel *existingUserCaptionLabel;
@property (weak, nonatomic) IBOutlet UILabel *registrationCaptionLabel;
@property (weak, nonatomic) IBOutlet UILabel *pendingGameCaptionLabel;
@property (weak, nonatomic) IBOutlet UILabel *approvedCaptionLabel;
@property (weak, nonatomic) IBOutlet UILabel *rejectedCaptionLabel;
@property (weak, nonatomic) IBOutlet UILabel *totalSubmissionsLabel;

@property (weak, nonatomic) IBOutlet UILabel *unsyncedCategoryLabel;
@property (weak, nonatomic) IBOutlet UITextField *eventButton;

@property (weak, nonatomic) IBOutlet UIView *totalRegistrantView;
@property (weak, nonatomic) IBOutlet UIView *existingUserView;
@property (weak, nonatomic) IBOutlet UIView *pendingRegistrationView;
@property (weak, nonatomic) IBOutlet UIView *approvedView;
@property (weak, nonatomic) IBOutlet UIView *rejectedView;
@property (weak, nonatomic) IBOutlet UIView *pendingDataGameView;
@property (weak, nonatomic) IBOutlet UIView *totalSubmissionsView;


@property (strong, nonatomic) NGREGBottomPanelViewController *panelViewController;

@property (weak, nonatomic) IBOutlet UIImageView *bgImageView;

@end



@implementation NGREGReportViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setUI];
}

- (void)viewWillAppear:(BOOL)animated{
    [self setup];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Helpher methods

- (void)setup {
    
    appDelegate = (NGREGAppDelegate *)[[UIApplication sharedApplication] delegate];
    
    NSArray* ar = [NGREGProfile profileWithEmail:[[NSUserDefaults standardUserDefaults] valueForKey:kNGREGEmailNextGen] InManagedObjectContext:[appDelegate managedObjectContext]];
    profile = [ar lastObject];
    
    self.panelViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"BottomPanel"];
    [self.panelViewController setDelegate:self];
    [self addChildViewController:self.panelViewController];
    
    CGRect panelViewFrame = CGRectMake(0.0f, self.view.bounds.size.height - kPanelViewVisibleHeight, 1024.0f, 500);
    self.panelViewController.view.frame = panelViewFrame;
    [self.view addSubview:self.panelViewController.view];
    
    [self.panelViewController setDataArray:@[@{@"name":@"< HOME"}]];
    
    [self.panelViewController setTextSize:[[profile.customizeTemplates valueForKey:@"text-size"] integerValue] andTextColor:[profile.customizeTemplates valueForKey:@"text-label-menu-color"] andBackgroundColor:[profile.customizeTemplates valueForKey:@"text-label-menu-background-color"]andFontId:[[profile.customizeTemplates valueForKey:@"text-style"] integerValue] andBgUrl:[profile.customizeTemplates valueForKey:@"button-menu"] andMD5:[profile.customizeTemplates valueForKey:@"button-menu_md5"]];
    
    [self.reportLabel setTextColor:[NGREGUtil colorFromHexString:[profile.customizeTemplates objectForKey:@"text-label-menu-background-color"] ]];
    [self.eventLabel setTextColor:[NGREGUtil colorFromHexString:[profile.customizeTemplates objectForKey:@"text-label-menu-background-color"] ]];
    [self.eventButton setTextColor:[NGREGUtil colorFromHexString:[profile.customizeTemplates objectForKey:@"text-label-menu-background-color"] ]];
    [self.nameLabel setTextColor:[NGREGUtil colorFromHexString:[profile.customizeTemplates objectForKey:@"text-label-menu-background-color"] ]];
    [self.reportLabel setTextColor:[NGREGUtil colorFromHexString:[profile.customizeTemplates objectForKey:@"text-label-menu-color"] ]];
    
    [self.existingRegistrantLabel setTextColor:[NGREGUtil colorFromHexString:[profile.customizeTemplates objectForKey:@"text-label-menu-background-color"] ]];
    [self.totalRegistrantLabel setTextColor:[NGREGUtil colorFromHexString:[profile.customizeTemplates objectForKey:@"text-label-menu-background-color"] ]];
    [self.pendingDataRegistrationLabel setTextColor:[NGREGUtil colorFromHexString:[profile.customizeTemplates objectForKey:@"text-label-menu-background-color"] ]];
    [self.pendingDataGameLabel setTextColor:[NGREGUtil colorFromHexString:[profile.customizeTemplates objectForKey:@"text-label-menu-background-color"] ]];
    [self.rejectedLabel setTextColor:[NGREGUtil colorFromHexString:[profile.customizeTemplates objectForKey:@"text-label-menu-background-color"] ]];
    [self.approvedLabel setTextColor:[NGREGUtil colorFromHexString:[profile.customizeTemplates objectForKey:@"text-label-menu-background-color"] ]];
    [self.submissionsLabel setTextColor:[NGREGUtil colorFromHexString:[profile.customizeTemplates objectForKey:@"text-label-menu-background-color"] ]];
    
    
    [self.unsyncedCategoryLabel setTextColor:[NGREGUtil colorFromHexString:[profile.customizeTemplates objectForKey:@"text-label-color"] ]];
    [self.registrationsCategoryLabel setTextColor:[NGREGUtil colorFromHexString:[profile.customizeTemplates objectForKey:@"text-label-color"] ]];
    
    [self.totalRegistrationCaptionLabel setTextColor:[NGREGUtil colorFromHexString:[profile.customizeTemplates objectForKey:@"text-label-color"] ]];
    [self.totalSubmissionsLabel setTextColor:[NGREGUtil colorFromHexString:[profile.customizeTemplates objectForKey:@"text-label-color"] ]];
    [self.rejectedCaptionLabel setTextColor:[NGREGUtil colorFromHexString:[profile.customizeTemplates objectForKey:@"text-label-color"] ]];
    [self.approvedCaptionLabel setTextColor:[NGREGUtil colorFromHexString:[profile.customizeTemplates objectForKey:@"text-label-color"] ]];
    [self.existingUserCaptionLabel setTextColor:[NGREGUtil colorFromHexString:[profile.customizeTemplates objectForKey:@"text-label-color"] ]];
    [self.pendingGameCaptionLabel setTextColor:[NGREGUtil colorFromHexString:[profile.customizeTemplates objectForKey:@"text-label-color"] ]];
    [self.registrationCaptionLabel setTextColor:[NGREGUtil colorFromHexString:[profile.customizeTemplates objectForKey:@"text-label-color"] ]];
    
    // set text size
    [self.eventButton setFont:[UIFont systemFontOfSize:[[profile.customizeTemplates objectForKey:@"textbox-text-size"] intValue] ]];
    
    
    // set font
    [NGREGUtil setFontBoldFromThisView:self.reportLabel andIdFont:[[profile.customizeTemplates valueForKey:@"text-style"] integerValue]];
    [NGREGUtil setFontBoldFromThisView:self.eventButton andIdFont:[[profile.customizeTemplates valueForKey:@"text-style"] integerValue]];
    [NGREGUtil setFontBoldFromThisView:self.eventLabel andIdFont:[[profile.customizeTemplates valueForKey:@"text-style"] integerValue]];
    [NGREGUtil setFontBoldFromThisView:self.existingRegistrantLabel andIdFont:[[profile.customizeTemplates valueForKey:@"text-style"] integerValue]];
    [NGREGUtil setFontBoldFromThisView:self.totalRegistrantLabel andIdFont:[[profile.customizeTemplates valueForKey:@"text-style"] integerValue]];
    [NGREGUtil setFontBoldFromThisView:self.pendingDataGameLabel andIdFont:[[profile.customizeTemplates valueForKey:@"text-style"] integerValue]];
    [NGREGUtil setFontBoldFromThisView:self.pendingDataRegistrationLabel andIdFont:[[profile.customizeTemplates valueForKey:@"text-style"] integerValue]];
    [NGREGUtil setFontBoldFromThisView:self.unsyncedCategoryLabel andIdFont:[[profile.customizeTemplates valueForKey:@"text-style"] integerValue]];
    [NGREGUtil setFontBoldFromThisView:self.registrationsCategoryLabel andIdFont:[[profile.customizeTemplates valueForKey:@"text-style"] integerValue]];
    [NGREGUtil setFontBoldFromThisView:self.submissionsLabel andIdFont:[[profile.customizeTemplates valueForKey:@"text-style"] integerValue]];
    [NGREGUtil setFontBoldFromThisView:self.approvedLabel andIdFont:[[profile.customizeTemplates valueForKey:@"text-style"] integerValue]];
    [NGREGUtil setFontBoldFromThisView:self.rejectedLabel andIdFont:[[profile.customizeTemplates valueForKey:@"text-style"] integerValue]];
    [NGREGUtil setFontFromThisView:self.nameLabel andIdFont:[[profile.customizeTemplates valueForKey:@"text-style"] integerValue]];
    
    [NGREGUtil setFontFromThisView:self.registrationCaptionLabel andIdFont:[[profile.customizeTemplates valueForKey:@"text-style"] integerValue]];
    [NGREGUtil setFontFromThisView:self.rejectedCaptionLabel andIdFont:[[profile.customizeTemplates valueForKey:@"text-style"] integerValue]];
    [NGREGUtil setFontFromThisView:self.approvedCaptionLabel andIdFont:[[profile.customizeTemplates valueForKey:@"text-style"] integerValue]];
    [NGREGUtil setFontFromThisView:self.existingUserCaptionLabel andIdFont:[[profile.customizeTemplates valueForKey:@"text-style"] integerValue]];
    [NGREGUtil setFontFromThisView:self.pendingGameCaptionLabel andIdFont:[[profile.customizeTemplates valueForKey:@"text-style"] integerValue]];
    [NGREGUtil setFontFromThisView:self.totalRegistrationCaptionLabel andIdFont:[[profile.customizeTemplates valueForKey:@"text-style"] integerValue]];
    [NGREGUtil setFontFromThisView:self.totalSubmissionsLabel andIdFont:[[profile.customizeTemplates valueForKey:@"text-style"] integerValue]];
    
    self.totalRegistrantView.layer.borderColor = [NGREGUtil colorFromHexString:[profile.customizeTemplates objectForKey:@"text-label-color"] ].CGColor;
    self.rejectedView.layer.borderColor = [NGREGUtil colorFromHexString:[profile.customizeTemplates objectForKey:@"text-label-color"] ].CGColor;
    self.pendingDataGameView.layer.borderColor = [NGREGUtil colorFromHexString:[profile.customizeTemplates objectForKey:@"text-label-color"] ].CGColor;
    self.pendingRegistrationView.layer.borderColor = [NGREGUtil colorFromHexString:[profile.customizeTemplates objectForKey:@"text-label-color"] ].CGColor;
    self.approvedView.layer.borderColor = [NGREGUtil colorFromHexString:[profile.customizeTemplates objectForKey:@"text-label-color"] ].CGColor;
    self.existingUserView.layer.borderColor = [NGREGUtil colorFromHexString:[profile.customizeTemplates objectForKey:@"text-label-color"] ].CGColor;
    self.totalSubmissionsView.layer.borderColor = [NGREGUtil colorFromHexString:[profile.customizeTemplates objectForKey:@"text-label-color"] ].CGColor;
    self.totalRegistrantView.layer.borderWidth = 1.0f;
    self.rejectedView.layer.borderWidth = 1.0f;
    self.pendingRegistrationView.layer.borderWidth = 1.0f;
    self.pendingDataGameView.layer.borderWidth = 1.0f;
    self.approvedView.layer.borderWidth = 1.0f;
    self.existingUserView.layer.borderWidth = 1.0f;
    self.totalSubmissionsView.layer.borderWidth = 1.0f;
    
}

- (void)setUI{
    appDelegate = (NGREGAppDelegate *)[[UIApplication sharedApplication] delegate];
    
    NSArray* ar = [NGREGProfile profileWithEmail:[[NSUserDefaults standardUserDefaults] valueForKey:kNGREGEmailNextGen] InManagedObjectContext:[appDelegate managedObjectContext]];
    profile = [ar lastObject];
    
//    [self.bgImageView setImageWithURL:[NSURL URLWithString:[profile.customizeTemplates valueForKey:@"global"]]];
    
//    [self.bgImageView setImageWithURL:[NSURL URLWithString:[profile.customizeTemplates valueForKey:@"global"]] placeholderImage:nil options:SDWebImageRetryFailed];
    
    [self loadImage:self.bgImageView withUrl:[profile.customizeTemplates valueForKey:@"global"] andMD5:[profile.customizeTemplates valueForKey:@"global_md5"]];
 
    [self.existingRegistrantLabel setTextColor:[NGREGUtil colorFromHexString:[profile.customizeTemplates objectForKey:@"text-label-color"] ]];
    [self.totalRegistrantLabel setTextColor:[NGREGUtil colorFromHexString:[profile.customizeTemplates objectForKey:@"text-label-color"] ]];
    [self.pendingDataGameLabel setTextColor:[NGREGUtil colorFromHexString:[profile.customizeTemplates objectForKey:@"text-label-color"] ]];
    [self.pendingDataRegistrationLabel setTextColor:[NGREGUtil colorFromHexString:[profile.customizeTemplates objectForKey:@"text-label-color"] ]];
    
    events = profile.menu;
    if([events count]>0)
        [self loadReportEvent:events[0]];
    
}

- (void)loadReportRegistrationLocal:(NSString*) menuId andMenuName:(NSString*)menuName{
    NSArray* reports = [NGREGReport reportWithEmail:[[NSUserDefaults standardUserDefaults] valueForKey:kNGREGEmailNextGen] andMenuId:menuId inManagedObjectContext:[appDelegate managedObjectContext]];
    NGREGReport *report = [reports lastObject];
    
    NSString* menu = @"";
    if([self.menu objectForKey:@"name"] ){
        menu = [self.menu valueForKey:@"name"];
    }else{
        menu = [self.menu valueForKey:@"menuName"];
    }
    
    NSArray* submissions = [NGREGSubmission submissionWithEmail:[[NSUserDefaults standardUserDefaults] valueForKey:kNGREGEmailNextGen] brandId:[[NSUserDefaults standardUserDefaults] valueForKey:kNGREGBrandIDNextGen] eventId:menuName inManagedObjectContext:[appDelegate managedObjectContext]];
    NGREGSubmission *submission = [submissions lastObject];
    
    if(submission == nil){
        submission = [NSEntityDescription insertNewObjectForEntityForName:@"NGREGSubmission" inManagedObjectContext:[appDelegate managedObjectContext]];
        submission.email = [[NSUserDefaults standardUserDefaults] valueForKey:kNGREGEmailNextGen] ;
        submission.submissionTotal = [NSNumber numberWithInt:0];
        submission.eventId = menuName;
        submission.submissionVersion = [NSNumber numberWithInt:0];
        submission.brandId = [[NSUserDefaults standardUserDefaults] valueForKey:kNGREGBrandIDNextGen];
        [submission.managedObjectContext save:nil];
    }
    
//    NSString* submissionTotal = [[NSUserDefaults standardUserDefaults] valueForKey:kNGREGSubmissionTotalNextGen];
//    NSString* submissionVersion = [[NSUserDefaults standardUserDefaults] valueForKey:kNGREGSubmissionVersionNextGen];
    
    NSNumber* totalSubmission = submission.submissionTotal;
    
    if(report != nil){
        [self.existingRegistrantLabel setText:[NSString stringWithFormat:@"%@", report.existingRegistrant]];
        [self.totalRegistrantLabel setText:[NSString stringWithFormat:@"%@", report.totalRegistrant]];
        [self.rejectedLabel setText:[NSString stringWithFormat:@"%@", report.rejectedRegistrant]];
        [self.approvedLabel setText:[NSString stringWithFormat:@"%@", report.approvedRegistrant]];
        
        if([submission.submissionVersion intValue] == [report.submissionVersion intValue]){

        }else{
//            [[NSUserDefaults standardUserDefaults] setValue:report.submissionVersion forKey:kNGREGSubmissionVersionNextGen];
//            [[NSUserDefaults standardUserDefaults] setValue:report.submissionTotal forKey:kNGREGSubmissionTotalNextGen];
            submission.submissionVersion = report.submissionVersion;
            submission.submissionTotal = report.submissionTotal;
            [submission.managedObjectContext save:nil];
            
            totalSubmission = report.submissionTotal;
        }
        
    }
    else{
        [self.existingRegistrantLabel setText:@"0"];
        [self.totalRegistrantLabel setText:@"0"];
        [self.rejectedLabel setText:@"0"];
        [self.approvedLabel setText:@"0"];
        [self.submissionsLabel setText:@"0"];
    }
    
    
    [self.submissionsLabel setText:[NSString stringWithFormat:@"%d", [totalSubmission intValue]]];
}

- (void)loadReportEvent:(NSDictionary*) event{
    
    [[NGREGEngine sharedEngine] requestReportWithMenuId:[event valueForKey:@"menuid"] andCompletionHandler:^(id result, NSError *error) {
            [self loadReportRegistrationLocal:[event valueForKey:@"menuid"] andMenuName:[event valueForKey:@"name"]];
    }];
    
    [self.eventButton setText:[event valueForKey:@"name"]];
    [self.eventLabel setText:[event valueForKey:@"name"]];
    
    [self.registrationsCategoryLabel setText:@"Registrations"];
    [self.unsyncedCategoryLabel setText:@"Un-Synced Data"];
    
    [self.pendingDataGameLabel setText:[NSString stringWithFormat:@"%d", [NGREGSync syncsGameCountWithEmail:[[NSUserDefaults standardUserDefaults] valueForKey:kNGREGEmailNextGen] menu:[event valueForKey:@"name"] brandId:[[NSUserDefaults standardUserDefaults] valueForKey:kNGREGBrandIDNextGen]]]];
    [self.pendingDataRegistrationLabel setText:[NSString stringWithFormat:@"%d", [NGREGSync syncsRegistrationCountWithEmail:[[NSUserDefaults standardUserDefaults] valueForKey:kNGREGEmailNextGen] menu:[event valueForKey:@"name"] brandId:[[NSUserDefaults standardUserDefaults] valueForKey:kNGREGBrandIDNextGen]]]];
    
    [self.nameLabel setText:[NSString stringWithFormat:@"%@ %@", profile.name, profile.lastName == nil ? @"" : profile.lastName]];
    
    [self loadReportRegistrationLocal:[event valueForKey:@"menuid"] andMenuName:[event valueForKey:@"name"]];
        
}


#pragma mark - NGREGBottomPanelViewControllerDelegate methods

- (void)bottomPanelViewController:(NGREGBottomPanelViewController *)viewController didSelectMenuAtIndex:(NSInteger)index WithData:(id)menu{
    [self.navigationController popViewControllerAnimated:NO];
}

#pragma mark - Action Methods

- (IBAction)eventButtonClicked:(id)sender {
    if(_popoverSubBrandViewController != nil){
        [_popoverSubBrandViewController dismissPopoverAnimated:YES];
        _popoverSubBrandViewController.delegate = nil;
        _popoverSubBrandViewController = nil;
    }
    
    NGREGEventPopoverViewController *subBrandViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"EventPopover"];
    [subBrandViewController setEvents:events];
    
    [subBrandViewController setSelectedEvent:^(NSDictionary * eventSelected) {
        
        [self loadReportEvent:eventSelected];
        [_popoverSubBrandViewController dismissPopoverAnimated:YES];
        
    }];
    
    _popoverSubBrandViewController = [[WYPopoverController alloc] initWithContentViewController:subBrandViewController];
    
    subBrandViewController.preferredContentSize = CGSizeMake(262, 230); // iOS 7
    
    [[WYPopoverBackgroundView appearance] setFillTopColor:[UIColor whiteColor]];
    
    _popoverSubBrandViewController.passthroughViews = @[sender];
    _popoverSubBrandViewController.delegate = self;
    _popoverSubBrandViewController.popoverLayoutMargins = UIEdgeInsetsMake(0.0f, 0.0f, 0.0f, 0.0f);
    _popoverSubBrandViewController.wantsDefaultContentAppearance = NO;
    [_popoverSubBrandViewController presentPopoverFromRect:self.eventButton.bounds inView:self.eventButton permittedArrowDirections:WYPopoverArrowDirectionAny animated:YES];
}

#pragma mark - WYPopoverControllerDelegate methods


- (BOOL)popoverControllerShouldDismissPopover:(WYPopoverController *)popoverController {
    
    return YES;
}

- (void)popoverControllerDidDismissPopover:(WYPopoverController *)controller {
    
    if (controller == _popoverSubBrandViewController) {
        
        _popoverSubBrandViewController.delegate = nil;
        _popoverSubBrandViewController = nil;
    }
}

@end
