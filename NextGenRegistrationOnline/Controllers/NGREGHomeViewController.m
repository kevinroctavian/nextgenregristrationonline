//
//  NGREGHomeViewController.m
//  NextGenRegistrationOnline
//
//  Created by Kevin Octavian on 1/21/14.
//  Copyright (c) 2014 Kevin R. Octavian. All rights reserved.
//

#import "NGREGHomeViewController.h"
#import "NGREGEngine.h"
#import "NGREGBottomPanelViewController.h"
#import "NGREGAppDelegate.h"
#import "NGREGProfile+Additions.h"
#import "UIAlertView+BlockExtensions.h"
#import "NGREGLoginViewController.h"
#import "NGREGVideoViewController.h"
#import "NGREGCheckRegistrantViewController.h"
#import "FBEncryptorAES.h"
#import "NGREGBrand+Additions.h"
#import "NGREGReportViewController.h"
#import "NGREGThanksViewController.h"
#import "NGREGRegisterRegistrantViewController.h"
#import "UIImageView+FileDownload.h"
#import "NSData+MD5.h"

@interface NGREGHomeViewController ()<NGREGBottomPanelViewControllerDelegate, NGREGVideoViewControllerDelegate, NGREGCheckRegistrantViewControllerDelegate>{
    NGREGProfile* profile;
    NGREGAppDelegate *appDelegate ;
}

@property (strong, nonatomic) NGREGBottomPanelViewController *panelViewController;
@property (weak, nonatomic) IBOutlet UILabel *label;
@property (weak, nonatomic) IBOutlet UIImageView *bgHome;
@property (weak, nonatomic) IBOutlet UIImageView *bgPopover;
@property (weak, nonatomic) IBOutlet UIView *syncVIew;
@property (weak, nonatomic) IBOutlet UIView *syncBg;
@property (weak, nonatomic) IBOutlet UIProgressView *progressSync;
@property (weak, nonatomic) IBOutlet UILabel *labelSync;

@property (nonatomic, strong) NSMutableArray *filesArray;



@end

@implementation NGREGHomeViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
//    [[NGREGEngine sharedEngine]requestMyProfileWithCompletionHandler:^(id result, NSError *error) {
//        [self setUI];
//        
//    }];
//    
//    [self setUI];
    
//    NSString* font = @"";
//    for( NSString *familyName in [UIFont familyNames] ) {
//        for( NSString *fontName in [UIFont fontNamesForFamilyName:familyName] ) {
//            font = [NSString stringWithFormat:@"%@\n%@", font, fontName];
//        }
//    }
//    NSLog(@"Font : %@", font);
}


- (void)viewWillAppear:(BOOL)animated{
    
    // error test
//    [self.storyboard instantiateViewControllerWithIdentifier:@"error test"];
    
    [[NGREGEngine sharedEngine]requestMyProfileWithCompletionHandler:^(id result, NSError *error) {
        
        [self setUI];
        
    }];
    
//    [self setUI];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


#pragma mark - Helper

- (void)viewDidBecomeActive{
//    [self setUI];
}

- (void)setUI{
    
    appDelegate = (NGREGAppDelegate *)[[UIApplication sharedApplication] delegate];
    
    NSArray* ar = [NGREGProfile profileWithEmail:[[NSUserDefaults standardUserDefaults] valueForKey:kNGREGEmailNextGen] InManagedObjectContext:[appDelegate managedObjectContext]];
    profile = [ar lastObject];
    
    
    self.filesArray = [[NSMutableArray alloc] init];
    [self addFileSessionToDownload:@"image" andURL:[profile.customizeTemplates valueForKey:@"global"] andMD5:[profile.customizeTemplates valueForKey:@"global_md5"]];
    [self addFileSessionToDownload:@"image" andURL:[profile.customizeTemplates valueForKey:@"home"] andMD5:[profile.customizeTemplates valueForKey:@"home_md5"]];
    [self addFileSessionToDownload:@"image" andURL:[profile.customizeTemplates valueForKey:@"button-submit-disable"] andMD5:[profile.customizeTemplates valueForKey:@"button-submit-disable_md5"]];
    [self addFileSessionToDownload:@"image" andURL:[profile.customizeTemplates valueForKey:@"search"] andMD5:[profile.customizeTemplates valueForKey:@"search_md5"]];
    [self addFileSessionToDownload:@"image" andURL:[profile.customizeTemplates valueForKey:@"button-menu"] andMD5:[profile.customizeTemplates valueForKey:@"button-menu_md5"]];
    [self addFileSessionToDownload:@"image" andURL:[profile.customizeTemplates valueForKey:@"button-submit"] andMD5:[profile.customizeTemplates valueForKey:@"button-submit_md5"]];
    
    
    NSArray* fileVideo = profile.menu;
    for(int i=0;i<[fileVideo count];i++){
        NSDictionary* menu = profile.menu[i];
        
        NSString* url = [[menu valueForKey:@"video"] valueForKey:@"url"];
        NSString* md5 = [[menu valueForKey:@"video"] valueForKey:@"filename_md5"];
        
        [self addFileSessionToDownload:@"video" andURL:url andMD5:md5];
    }
    
    if([self cekFileIsExists]){
        [self navigateToDownloadPage];
    }else{
        
        if(!self.panelViewController){
            self.panelViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"BottomPanel"];
            [self.panelViewController setDelegate:self];
            [self addChildViewController:self.panelViewController];
            
            CGRect panelViewFrame = CGRectMake(0.0f, self.view.bounds.size.height - kPanelViewVisibleHeight, 1024.0f, 500);
            self.panelViewController.view.frame = panelViewFrame;
            [self.view addSubview:self.panelViewController.view];
        }
        
        NSMutableArray* data = [NSMutableArray arrayWithArray:profile.menu];
        [data addObject:@{@"name":@"SYNC"}];
        [data addObject:@{@"name":@"REPORT"}];
        [data addObject:@{@"name":@"LOGOUT"}];
        
        [self.panelViewController setDataArray:data];
        [self.panelViewController setTextSize:[[profile.customizeTemplates valueForKey:@"text-size"] integerValue] andTextColor:[profile.customizeTemplates valueForKey:@"text-label-menu-color"] andBackgroundColor:[profile.customizeTemplates valueForKey:@"text-label-menu-background-color"]andFontId:[[profile.customizeTemplates valueForKey:@"text-style"] integerValue] andBgUrl:[profile.customizeTemplates valueForKey:@"button-menu"] andMD5:[profile.customizeTemplates valueForKey:@"button-menu_md5"]];
        
        [self loadImage:self.bgHome withUrl:[profile.customizeTemplates valueForKey:@"home"] andMD5:[profile.customizeTemplates valueForKey:@"home_md5"]];
        
        [self cekLogging];
        
    }
    
}

- (void)addFileSessionToDownload:(NSString*)title andURL:(NSString*)url andMD5:(NSString*)md5{
        
    NSDictionary* dict = @{@"url":url, @"md5":md5, @"title":title};
    [self.filesArray addObject:dict];
        
}

- (BOOL)cekFileIsExists{
    
    for (int i =0; i<[self.filesArray count]; i++) {
        
        NSDictionary* dict = self.filesArray[i];
        
        NSString* url = [dict objectForKey:@"url"];
        NSString* md5 = [dict objectForKey:@"md5"];
        
        
        NSArray *URLs = [[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask];
        NSURL* docDirectoryURL = [URLs objectAtIndex:0];
        
        NSFileManager *fileManager = [NSFileManager defaultManager];
        
        NSArray *parts = [url componentsSeparatedByString:@"/"];
        NSString *filename = [parts objectAtIndex:[parts count]-1];
        NSURL *destinationURL = [docDirectoryURL URLByAppendingPathComponent:filename];
        
        if ([fileManager fileExistsAtPath:[destinationURL path]]) {
            NSData *nsData = [NSData dataWithContentsOfFile:[destinationURL path]];
            if (nsData){
                NSString* md5File = [nsData MD5];
                NSLog(@"MD5-nya adalah %@", md5File);
                if(![md5File isEqualToString:md5]){
                    return true;
                }
            }
        }else{
            return true;
        }
    }
    
    return false;
}

- (void)navigateToDownloadPage{
    [appDelegate setRootViewControllerToDownloadViewController];
}


- (void)navigateToRegisterRegistrant:(NSString*)menuName{
    
    NSDictionary* menu;
    
    for (NSDictionary* dict in profile.menu) {
        if([[dict valueForKey:@"name"] isEqualToString:menuName]){
            menu = [[NSDictionary alloc] initWithDictionary:dict];
            break;
        }
    }
    
    if(menu == nil)
        return;
    
    NGREGCheckRegistrantViewController *registerView = [self.storyboard instantiateViewControllerWithIdentifier:@"CheckRegistrant"];
    [registerView setMenu:menu];
    [registerView setDelegate:self];
    [self.navigationController pushViewController:registerView animated:NO];
    
    
//    NGREGRegisterRegistrantViewController *registrantController = [self.storyboard instantiateViewControllerWithIdentifier:@"RegisterRegistrant"];
//    [registrantController setRegistrant:[[NGREGCheckRegistrantResponse alloc] init]];
//    [registrantController setMenu:menu];
//    [registrantController setDidFinishRegisterRegistrant:^(NGREGCheckRegistrantResponse * registrant) {
//        [self checkRegistrantVideoViewController:self didFinish:menu withEmail:registrant.email];
////        [self.delegate checkRegistrantVideoViewController:self didFinish:self.menu withEmail:registrant.email];
//    }];
//    [self.navigationController pushViewController:registrantController animated:NO];

}

- (void)navigateToExternalApp:(NSDictionary*) menu withMenuType:(NSString*)menuType andSchemeUrl:(NSString*) scheme andRegistrantEmail:(NSString*)registrantEmail{
    UIApplication *ourApplication = [UIApplication sharedApplication];
    
    NSDictionary* name = @{@"menuType":menuType,
                           @"menuName":[menu valueForKey:@"name"],
                           @"userEmail":[[NSUserDefaults standardUserDefaults] valueForKey:kNGREGEmailNextGen],
                           @"userId":profile.profileId,
                           @"registrantEmail":registrantEmail,
                           @"promotionalsite":[NGREGBrand brandWithBrandId:profile.brandId inManagedObjectContext:[appDelegate managedObjectContext]].brandName,
                           };
    
    NSData* jsonData = [NSJSONSerialization dataWithJSONObject:name
                                                       options:0
                                                         error:Nil];
    
    NSString *JSONString = [[NSString alloc] initWithBytes:[jsonData bytes] length:[jsonData length] encoding:NSUTF8StringEncoding];
    
    NSString* encrypt = [FBEncryptorAES encryptBase64String:JSONString keyString:@"4r53n4L" separateLines:NO];
    
    NSString *URLEncodedText = [encrypt stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSString *ourPath = [[NSString stringWithFormat:@"%@://",scheme] stringByAppendingString:URLEncodedText];
    NSURL *ourURL = [NSURL URLWithString:ourPath];
    
    if ([ourApplication canOpenURL:ourURL]) {
        [[NGREGEngine sharedEngine] requestTrackActivityWithMenu:[menu valueForKey:@"name"] andPage:menuType andCompletionHandler:nil];
        [ourApplication openURL:ourURL];
    }
    else {
        //Display error
        //Connecting Application Not Found
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"Please make sure you have inserted the connecting app correctly." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertView show];
    }
}

- (void)showSyncView{
    [UIView transitionWithView:self.view
                      duration:0.3
                       options:(UIViewAnimationOptionTransitionCrossDissolve | UIViewAnimationOptionAllowAnimatedContent)
                    animations:^{
                        
                        BOOL oldState = [UIView areAnimationsEnabled];
                        [UIView setAnimationsEnabled:NO];
                        [self.syncVIew setHidden:NO];
                        [self.syncBg setHidden:NO];
                        [UIView setAnimationsEnabled:oldState];
                        
                    } completion:nil];
}

- (void)hideSyncView{
    
    [UIView transitionWithView:self.view
                      duration:0.3
                       options:(UIViewAnimationOptionTransitionCrossDissolve | UIViewAnimationOptionAllowAnimatedContent)
                    animations:^{
                        
                        BOOL oldState = [UIView areAnimationsEnabled];
                        [UIView setAnimationsEnabled:NO];
                        [self.syncVIew setHidden:YES];
                        [self.syncBg setHidden:YES];
                        [UIView setAnimationsEnabled:oldState];
                        
                    } completion:nil];
}

#pragma mark - Action methods

- (IBAction)dismissButtonClicked:(id)sender {
    [self hideSyncView];
}


#pragma mark - NGREGBottomPanelViewControllerDelegate methods

- (void)bottomPanelViewController:(NGREGBottomPanelViewController *)viewController didSelectMenuAtIndex:(NSInteger)index WithData:(id)menu{
    
    if([menu isKindOfClass:[NSDictionary class]]){
        NSString* menuString = [[menu valueForKey:@"name"]uppercaseString];
        if([menuString isEqualToString:@"SYNC"]){
            
            [self.labelSync setText:@"0%"];
            
            if([NGREGSync syncsCounterCountWithBrandId:[[NSUserDefaults standardUserDefaults] valueForKey:kNGREGBrandIDNextGen]] > 0){
                
                [self showSyncView];
                
                [NGREGSync syncWithCompletionHandler:^(float dataSucces, float allData, NSError *error) {
                    
                    float progress = dataSucces/allData;
                    [self.progressSync setProgress:progress];
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
//                        if(progress - (int)progress == 0)
                            [self.labelSync setText:[NSString stringWithFormat:@"%d%%", ((int)(progress * 100))]];
//                        else
//                            [self.labelSync setText:[NSString stringWithFormat:@"%0.2f%%", (progress * 100)]];
                    });
                    
                    if(dataSucces == allData){
                        [self hideSyncView];
                        [self.panelViewController refresh];
                    }
                    if(error != nil){
                        [self hideSyncView];
                        //Oops, We can’t Seem to Connect to the Internet
                        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"Please check your connection and try again" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                        [alertView show];
                        [self hideSyncView];
                        [self.panelViewController refresh];
                    }
                }];
                
            }else{
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"All Data is up to Date" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [alertView show];
            }
            
        }else if([menuString isEqualToString:@"REPORT"]){
            
            NGREGReportViewController *homeView = [self.storyboard instantiateViewControllerWithIdentifier:@"Report"];
            [homeView setMenu:menu];
            [self.navigationController pushViewController:homeView animated:NO];
            
        }else if([menuString isEqualToString:@"LOGOUT"]){
            
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"Are you sure?" completionBlock:^(NSUInteger buttonIndex, UIAlertView *alertView) {
                
                if ([[alertView buttonTitleAtIndex:buttonIndex] isEqualToString:@"Yes"]) {
                    [[NGREGEngine sharedEngine] requestLogoutWithCompletionHandler:^(id result, NSError *error) {
                        
                    }];
                    [[NSUserDefaults standardUserDefaults] setValue:@"" forKey:kNGREGEmailNextGen];
                    
                    [appDelegate setRootViewControllerToAuthenticationViewController];
                }
            } cancelButtonTitle:@"Cancel" otherButtonTitles:@"Yes", nil];
            [alertView show];
            
        }else{
            
            
//            NGREGCheckRegistrantViewController *videoView = [self.storyboard instantiateViewControllerWithIdentifier:@"CheckRegistrant"];
//            [videoView setMenu:menu];
//            [videoView setDelegate:self];
            
            NGREGVideoViewController *videoView = [self.storyboard instantiateViewControllerWithIdentifier:@"Video"];
            [videoView setDelegate:self];
            [videoView setMenu:menu];
//
            [self.navigationController pushViewController:videoView animated:NO];
            
        }
    }
    
}


#pragma mark - NGREGVideoViewControllerDelegate methods

- (void)videoViewController:(NGREGVideoViewController *)viewController didFinish:(NSDictionary*) menu{
    [self.navigationController popViewControllerAnimated:NO];
    
    if(![[[menu valueForKey:@"brosur"] valueForKey:@"status"]boolValue]){
        
        [self navigateToRegisterRegistrant:[menu valueForKey:@"name"]];

        return;
    }
    
    [self navigateToExternalApp:menu withMenuType:@"brosur" andSchemeUrl:[[menu valueForKey:@"brosur"] valueForKey:@"schemaid"] andRegistrantEmail:@""];
}


#pragma mark - NGREGCheckRegistrantViewControllerDelegate

- (void)checkRegistrantVideoViewController:(UIViewController *)viewController didFinish:(NSDictionary*) menu withRegistrant:(NGREGCheckRegistrantResponse*) registrant{
    [self.navigationController popToRootViewControllerAnimated:NO];
    
    if(![[[menu valueForKey:@"games"] valueForKey:@"status"]boolValue]){
            NGREGThanksViewController *homeView = [self.storyboard instantiateViewControllerWithIdentifier:@"Thanks"];
            [self.navigationController pushViewController:homeView animated:NO];
            return;
    }
    
    [self navigateToExternalApp:menu withMenuType:@"games" andSchemeUrl:[[menu valueForKey:@"games"] valueForKey:@"schemaid"] andRegistrantEmail:registrant.email];
}

@end
