//
//  NGREGVideoViewController.h
//  NextGenRegistrationOnline
//
//  Created by Kevin Octavian on 1/24/14.
//  Copyright (c) 2014 Kevin R. Octavian. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NGREGGenericViewController.h"

@protocol NGREGVideoViewControllerDelegate;

@interface NGREGVideoViewController : NGREGGenericViewController

@property (strong, nonatomic) NSDictionary* menu;

@property (assign, nonatomic) id <NGREGVideoViewControllerDelegate>delegate;

@end

@protocol NGREGVideoViewControllerDelegate <NSObject>
- (void)videoViewController:(NGREGVideoViewController *)viewController didFinish:(NSDictionary*) menu;
@end