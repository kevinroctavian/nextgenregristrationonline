//
//  NGREGCheckRegistrantViewController.m
//  NextGenRegistrationOnline
//
//  Created by Kevin Octavian on 1/21/14.
//  Copyright (c) 2014 Kevin R. Octavian. All rights reserved.
//

#import "NGREGCheckRegistrantViewController.h"
#import "NGREGEngine.h"
#import "NGREGCheckRegistrantResponse.h"
#import "UIView+Additions.h"
#import "DejalActivityView.h"
#import "NGREGAppDelegate.h"
#import "NGREGProfile+Additions.h"
#import "NGREGBottomPanelViewController.h"
#import "NGREGRegisterRegistrantViewController.h"
#import "AMPopTip.h"


@interface NGREGCheckRegistrantViewController ()<UITextFieldDelegate, NGREGBottomPanelViewControllerDelegate>{
    
    CGRect _originalViewFrame;
}

@property (weak, nonatomic) IBOutlet UILabel *checkYourDataLabel;
@property (weak, nonatomic) IBOutlet UILabel *emailLabel;
@property (weak, nonatomic) IBOutlet UILabel *orLabel;
@property (weak, nonatomic) IBOutlet UILabel *ktpLabel;
@property (weak, nonatomic) IBOutlet UILabel *checkYourCredentialsBelowLabel;

@property (weak, nonatomic) IBOutlet UIButton *nextButton;


@property (weak, nonatomic) IBOutlet UITextField *emailText;
@property (weak, nonatomic) IBOutlet UITextField *ktpText;
@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UIImageView *bgImageView;
@property (weak, nonatomic) IBOutlet UIImageView *bgContentView;
@property (weak, nonatomic) IBOutlet UIView *loadingView;

@property (nonatomic, strong) AMPopTip *popTip;

@property (strong, nonatomic) NGREGBottomPanelViewController *panelViewController;

@end

@implementation NGREGCheckRegistrantViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    _originalViewFrame = self.view.frame;
    
    if(![[[self.menu valueForKey:@"registration"] valueForKey:@"status"]boolValue]){
        NGREGCheckRegistrantResponse *registrant = [[NGREGCheckRegistrantResponse alloc] init];
        [registrant setEmail:@""];
        [self.delegate checkRegistrantVideoViewController:self didFinish:self.menu withRegistrant:registrant];
        return;
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    
    [self.emailText setDelegate:self];
    [self.ktpText setDelegate:self];
    
    [[NGREGEngine sharedEngine] requestTrackActivityWithMenu:[self.menu valueForKey:@"name"] andPage:@"register" andCompletionHandler:nil];
    
    [self setUI];
    
//    [[AMPopTip appearance] setFont:self.emailText.font];
    
    self.popTip = [AMPopTip popTip];
    self.popTip.shouldDismissOnTap = YES;
    
    self.popTip.popoverColor = [NGREGUtil colorFromHexString:@"#333333"];
    self.popTip.textColor = [NGREGUtil colorFromHexString:@"#ffffff"];
}

- (void)viewWillAppear:(BOOL)animated{
    [self setUI];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Action methods

- (IBAction)nextButtonClicked:(id)sender {
    
//    [self.popTip hide];
    
    NSString *message = @"";
    
    UIView* view;
    
    if(self.emailText.text.length == 0 && self.ktpText.text.length == 0){
        message = @"Fields cannot be empty. Please input your email address or KTP to continue";
        view = self.emailText;
        
        [self.popTip showText:message direction:AMPopTipDirectionNone maxWidth:400 inView:self.contentView fromFrame:view.frame];
        return;
    }
    
    if(self.emailText.text.length > 0)
        if(![NGREGUtil validateEmailWithString:self.emailText.text]){
            message = @"Oops! Please re-enter your email with the right format \n (e.g. example@email.com)";
            [self.emailText becomeFirstResponder];
                    view = self.emailText;
        }
    
    if(self.ktpText.text.length > 0 && self.ktpText.text.length < 7){
        message = @"Please enter your valid GIID number (KTP)";
        view = self.ktpText;
    }
    
    if(message.length > 0){
//        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
//        [alertView show];
        [self.popTip showText:message direction:AMPopTipDirectionDown maxWidth:350 inView:self.contentView fromFrame:view.frame];
        return;
    }
    
    
    
    if(self.emailText.text.length > 0){
        [self searchRegistrantByEmail];
    }else if(self.ktpText.text.length > 0){
        [self searchRegistrantByGIIDNumber];
    }
}

- (IBAction)hideKeyboardButtonClicked:(id)sender {
    [self.view endEditing:YES];
}


#pragma mark - Helpher methods

- (void)searchRegistrantByEmail{
    
    [self.view endEditing:YES];
    
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.35 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        
//        [self.loadingView setHidden:NO];
        [DejalBezelActivityView activityViewForView:self.view.superview];
        
        [[NGREGEngine sharedEngine] checkEntourageByEmail:self.emailText.text andCompletionHandler:^(NGREGCheckRegistrantResponse *entourage) {
            [DejalBezelActivityView removeViewAnimated:YES];
//            [self.loadingView setHidden:YES];
            [self navigateToRegistrantForm:entourage];
        }];
        
        //    [[NGREGEngine sharedEngine]requestDataRegistrantWithEMail:self.emailText.text andCompletionHandler:^(id result, NSError *error) {
        //        [DejalBezelActivityView removeViewAnimated:YES];
        //
        //        [self navigateToRegistrantForm:result];
        //
        //    }];
    });
    
}

- (void)searchRegistrantByGIIDNumber{
    
    [self.view endEditing:YES];
    
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.35 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        
//        [self.loadingView setHidden:NO];
        [DejalBezelActivityView activityViewForView:self.view.superview];
        
        
        [[NGREGEngine sharedEngine]checkEntourageByGIIDNumber:self.ktpText.text andCompletionHandler:^(NGREGCheckRegistrantResponse *entourage) {
            [DejalBezelActivityView removeViewAnimated:YES];
//            [self.loadingView setHidden:YES];
            
            [self navigateToRegistrantForm:entourage];
        }];
        
        //    [[NGREGEngine sharedEngine]requestDataRegistrantWithGIIDNumber:self.ktpText.text andCompletionHandler:^(id result, NSError *error) {
        //       [DejalBezelActivityView removeViewAnimated:YES];
        //
        //        [self navigateToRegistrantForm:result];
        //
        //    }];
        
    });
}

- (void)setUI{
    NGREGAppDelegate *appDelegate = (NGREGAppDelegate *)[[UIApplication sharedApplication] delegate];
    
    NSArray* ar = [NGREGProfile profileWithEmail:[[NSUserDefaults standardUserDefaults] valueForKey:kNGREGEmailNextGen] InManagedObjectContext:[appDelegate managedObjectContext]];
    NGREGProfile *profile = [ar lastObject];
    
    //    [self.bgContentView setImageWithURL:[NSURL URLWithString:[profile.customizeTemplates valueForKey:@"search"]]];
    //    [self.bgImageView setImageWithURL:[NSURL URLWithString:[profile.customizeTemplates valueForKey:@"search"]]];
//    [self.bgImageView setImageWithURL:[NSURL URLWithString:[profile.customizeTemplates valueForKey:@"search"]] placeholderImage:nil options:SDWebImageRetryFailed];
    
    [self loadImage:self.bgImageView withUrl:[profile.customizeTemplates valueForKey:@"search"] andMD5:[profile.customizeTemplates valueForKey:@"search_md5"]];
    
    self.panelViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"BottomPanel"];
    [self.panelViewController setDelegate:self];
    [self addChildViewController:self.panelViewController];
    
    CGRect panelViewFrame = CGRectMake(0.0f, self.view.bounds.size.height - kPanelViewVisibleHeight, 1024.0f, 500);
    self.panelViewController.view.frame = panelViewFrame;
    [self.view addSubview:self.panelViewController.view];
    
    [self.panelViewController setDataArray:@[@{@"name":@"< HOME"}]];
    
    [self.panelViewController setTextSize:[[profile.customizeTemplates valueForKey:@"text-size"] integerValue] andTextColor:[profile.customizeTemplates valueForKey:@"text-label-menu-color"] andBackgroundColor:[profile.customizeTemplates valueForKey:@"text-label-menu-background-color"]andFontId:[[profile.customizeTemplates valueForKey:@"text-style"] integerValue] andBgUrl:[profile.customizeTemplates valueForKey:@"button-menu"] andMD5:[profile.customizeTemplates valueForKey:@"button-menu_md5"]];
    
    // set text color
    [self.checkYourDataLabel setTextColor:[NGREGUtil colorFromHexString:[profile.customizeTemplates objectForKey:@"text-label-menu-background-color"] ]];
    [self.checkYourCredentialsBelowLabel setTextColor:[NGREGUtil colorFromHexString:[profile.customizeTemplates objectForKey:@"text-label-menu-background-color"] ]];
    [self.orLabel setTextColor:[NGREGUtil colorFromHexString:[profile.customizeTemplates objectForKey:@"text-label-menu-background-color"] ]];
    [self.emailLabel setTextColor:[NGREGUtil colorFromHexString:[profile.customizeTemplates objectForKey:@"text-label-color"] ]];
    
    [self.ktpLabel setTextColor:[NGREGUtil colorFromHexString:[profile.customizeTemplates objectForKey:@"text-label-color"] ]];
    
//    [self.nextButton setBackgroundImageWithURL:[NSURL URLWithString:[profile.customizeTemplates objectForKey:@"button-submit"]] forState:UIControlStateNormal placeholderImage:nil options:SDWebImageRetryFailed];
    
    [self loadImageButton:self.nextButton forState:UIControlStateNormal withUrl:[profile.customizeTemplates objectForKey:@"button-submit"] andMD5:[profile.customizeTemplates objectForKey:@"button-submit_md5"]];
    
    [self.nextButton setTintColor:[NGREGUtil colorFromHexString:[profile.customizeTemplates objectForKey:@"text-label-menu-color"] ]];
    [self.emailLabel setTextColor:[NGREGUtil colorFromHexString:[profile.customizeTemplates objectForKey:@"text-label-color"] ]];
    
    // set text size
    [self.emailLabel setFont:[UIFont systemFontOfSize:[[profile.customizeTemplates objectForKey:@"label-text-size"] intValue] ]];
    [self.orLabel setFont:[UIFont systemFontOfSize:[[profile.customizeTemplates objectForKey:@"label-text-size"] intValue] ]];
    [self.ktpLabel setFont:[UIFont systemFontOfSize:[[profile.customizeTemplates objectForKey:@"label-text-size"] intValue] ]];
    [self.ktpText setFont:[UIFont systemFontOfSize:[[profile.customizeTemplates objectForKey:@"textbox-text-size"] intValue] ]];
    [self.emailText setFont:[UIFont systemFontOfSize:[[profile.customizeTemplates objectForKey:@"textbox-text-size"] intValue] ]];
    [self.checkYourDataLabel setFont:[UIFont systemFontOfSize:[[profile.customizeTemplates objectForKey:@"title-text-search-size"] intValue] ]];
    [self.checkYourCredentialsBelowLabel setFont:[UIFont systemFontOfSize:[[profile.customizeTemplates objectForKey:@"subtitle-text-search-size"] intValue] ]];
    [self.nextButton.titleLabel setFont:[UIFont systemFontOfSize:[[profile.customizeTemplates objectForKey:@"textbox-text-size"] intValue] ]];
    
    
    // set textbox background
    [self.ktpText setBackgroundColor:[NGREGUtil colorFromHexString:[profile.customizeTemplates objectForKey:@"form-cell-background"]]];
    [self.emailText setBackgroundColor:[NGREGUtil colorFromHexString:[profile.customizeTemplates objectForKey:@"form-cell-background"]]];
    
    // set textbox text color
    [self.ktpText setTextColor:[NGREGUtil colorFromHexString:[profile.customizeTemplates objectForKey:@"form-cell-text-color"]]];
    [self.emailText setTextColor:[NGREGUtil colorFromHexString:[profile.customizeTemplates objectForKey:@"form-cell-text-color"]]];
    
    // set font
    [NGREGUtil setFontBoldFromThisView:self.nextButton andIdFont:[[profile.customizeTemplates valueForKey:@"text-style"] integerValue]];
    [NGREGUtil setFontBoldFromThisView:self.checkYourDataLabel andIdFont:[[profile.customizeTemplates valueForKey:@"text-style"] integerValue]];
    [NGREGUtil setFontBoldFromThisView:self.checkYourCredentialsBelowLabel andIdFont:[[profile.customizeTemplates valueForKey:@"text-style"] integerValue]];
    [NGREGUtil setFontBoldFromThisView:self.orLabel andIdFont:[[profile.customizeTemplates valueForKey:@"text-style"] integerValue]];
    [NGREGUtil setFontFromThisView:self.emailLabel andIdFont:[[profile.customizeTemplates valueForKey:@"text-style"] integerValue]];
    [NGREGUtil setFontFromThisView:self.ktpLabel andIdFont:[[profile.customizeTemplates valueForKey:@"text-style"] integerValue]];
    [NGREGUtil setFontFromThisView:self.ktpText andIdFont:[[profile.customizeTemplates valueForKey:@"text-style"] integerValue]];
    [NGREGUtil setFontFromThisView:self.emailText andIdFont:[[profile.customizeTemplates valueForKey:@"text-style"] integerValue]];
    
    
    
}

- (void)navigateToRegistrantForm:(NGREGCheckRegistrantResponse*) registrant{
    NGREGRegisterRegistrantViewController *registrantController = [self.storyboard instantiateViewControllerWithIdentifier:@"RegisterRegistrant"];
    [registrantController setRegistrant:registrant];
    [registrantController setMenu:self.menu];
    [registrantController setDidFinishRegisterRegistrant:^(NGREGCheckRegistrantResponse * registrant) {
        [self.delegate checkRegistrantVideoViewController:self didFinish:self.menu withRegistrant:registrant];
    }];
    [self.navigationController pushViewController:registrantController animated:NO];
}

#pragma mark - Keyboard Management

- (void)keyboardWillShow:(NSNotification *)notification {
    
    [self.popTip hide];
    
    NSDictionary* userInfo = [notification userInfo];
    
    // we don't use SDK constants here to be universally compatible with all SDKs ≥ 3.0
    NSValue* keyboardFrameValue = [userInfo objectForKey:@"UIKeyboardBoundsUserInfoKey"];
    if (!keyboardFrameValue) {
        keyboardFrameValue = [userInfo objectForKey:@"UIKeyboardFrameEndUserInfoKey"];
    }
    
    CGRect frame = CGRectMake(0, -80.0f, _originalViewFrame.size.width, _originalViewFrame.size.height);
    
    CGFloat animationDuration = [[userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] floatValue];
    [UIView animateWithDuration:animationDuration animations:^{
        
        self.view.frame = frame;
    }];
}

- (void)keyboardWillHide:(NSNotification *)notification {
    
	NSDictionary* userInfo = [notification userInfo];
    CGFloat animationDuration = [[userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] floatValue];
    
    [UIView animateWithDuration:0.3 animations:^{
        
		self.view.frame = _originalViewFrame;
        
    } completion:^(BOOL finished) {
        
    }];
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    //    if(textField == self.emailText){
    //        [self searchRegistrantByEmail];
    //    }
    //    if(textField == self.ktpText){
    //        [self searchRegistrantByGIIDNumber];
    //    }
    [self nextButtonClicked:nil];
    
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    if(textField.text.length > 0){
        [self.popTip hide];
    }
    
    if(textField == self.ktpText){
        NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
        
        NSString *expression = @"^[0-9]*$";
        
        NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:expression
                                                                               options:NSRegularExpressionCaseInsensitive
                                                                                 error:nil];
        NSUInteger numberOfMatches = [regex numberOfMatchesInString:newString
                                                            options:0
                                                              range:NSMakeRange(0, [newString length])];
        if (numberOfMatches == 0)
            return NO;
        
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        return (newLength > (textField == self.ktpText ? MAXLENGTH_GIID_NUMBER : MAXLENGTH_PHONE_NUMBER)) ? NO : YES;
    }
    
    return YES;
}

#pragma mark - NGREGBottomPanelViewControllerDelegate methods

- (void)bottomPanelViewController:(NGREGBottomPanelViewController *)viewController didSelectMenuAtIndex:(NSInteger)index WithData:(id)menu{
    [self.navigationController popViewControllerAnimated:NO];
}

@end
