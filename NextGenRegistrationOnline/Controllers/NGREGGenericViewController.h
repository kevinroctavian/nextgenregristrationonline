//
//  NGREGGenericViewController.h
//  NextGenRegistrationOnline
//
//  Created by Kevin Octavian on 10/15/14.
//  Copyright (c) 2014 Kevin R. Octavian. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NGREGAppDelegate.h"
#import "NGREGProfile+Additions.h"
#import "NGREGEngine.h"
#import "DejalActivityView.h"
#import "NGREGLoginResponse.h"
#import "NGREGBrand+Additions.h"
#import "NGREGAppDelegate.h"
#import "UIView+Additions.h"
#import "NGREGBrandPopoverViewController.h"
#import "WYPopoverController.h"
#import "NGREGUtil.h"
#import "Constant.h"

@interface NGREGGenericViewController : UIViewController

@property (nonatomic, strong) NSURL *docDirectoryURL;

- (void)cekLogging;

- (void) loadImage:(UIImageView*)imageView withUrl:(NSString*)url andMD5:(NSString*) md5;
- (void) loadImageButton:(UIButton*)button forState:(UIControlState)state withUrl:(NSString*)url andMD5:(NSString*) md5;

@end
