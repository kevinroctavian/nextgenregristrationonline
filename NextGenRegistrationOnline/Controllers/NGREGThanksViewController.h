//
//  NGREGThanksViewController.h
//  NextGenRegistrationOnline
//
//  Created by Kevin Octavian on 2/19/14.
//  Copyright (c) 2014 Kevin R. Octavian. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NGREGRootViewController.h"
#import "NGREGCheckRegistrantResponse.h"
#import "NGREGGenericViewController.h"

@interface NGREGThanksViewController : NGREGGenericViewController

@property (strong, nonatomic) NGREGCheckRegistrantResponse* registrant;

@end
