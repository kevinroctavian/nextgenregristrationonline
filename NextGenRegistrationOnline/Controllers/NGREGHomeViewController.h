//
//  NGREGHomeViewController.h
//  NextGenRegistrationOnline
//
//  Created by Kevin Octavian on 1/21/14.
//  Copyright (c) 2014 Kevin R. Octavian. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NGREGGenericViewController.h"

@interface NGREGHomeViewController : NGREGGenericViewController

- (void)navigateToRegisterRegistrant:(NSString*)menu;
- (void)viewDidBecomeActive;

@end
