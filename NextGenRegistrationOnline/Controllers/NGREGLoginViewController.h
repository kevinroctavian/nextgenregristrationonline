//
//  NGREGLoginViewController.h
//  NextGenRegistrationOnline
//
//  Created by Kevin Octavian on 1/16/14.
//  Copyright (c) 2014 Kevin R. Octavian. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NGREGRootViewController.h"

@interface NGREGLoginViewController : NGREGRootViewController

@end
