//
//  NGREGRegisterRegistrantViewController.m
//  NextGenRegistrationOnline
//
//  Created by Kevin Octavian on 1/29/14.
//  Copyright (c) 2014 Kevin R. Octavian. All rights reserved.
//

#import "NGREGRegisterRegistrantViewController.h"
#import "UIView+Additions.h"
#import "NGREGSignInViewController.h"
#import "NGREGCity+Additions.h"
#import "NGREGSubBrand+Additions.h"
#import "NGREGCityResponse.h"
#import "NGREGSubBrandPopoverViewController.h"
#import "NGREGCityPickerViewController.h"
#import "NGREGDatePickerPopupViewController.h"
#import "NGREGReport+Additions.h"
#import "CSVParser.h"
#import "NGREGTextField.h"
#import "NGREGBottomPanelViewController.h"
#import "NGREGError+Additions.h"
#import "NGREGRegistrant+Additions.h"
#import "AMPopTip.h"

#define DISABLE_TEXT @"#AAAAAA"
#define NORMAL_TEXT @"#FFFFFF"

@interface NGREGRegisterRegistrantViewController ()<NGREGBottomPanelViewControllerDelegate, UITextFieldDelegate, NGREGCityPickerViewControllerDelegate, WYPopoverControllerDelegate>{
    CGRect _originalViewFrame;
    NGREGAppDelegate *appDelegate;
    NSArray *cities, *subBrands;
    NSDictionary *_selectedCity;
    NSDictionary *_selectedSubBrands;
    NGREGCityPickerViewController *_cityPicker;
    WYPopoverController *_popoverSubBrandViewController, *_popoverViewController;
    BOOL genderTypeFemale;
    WYPopoverController *_popoverDateOfBirthViewController;
    NGREGProfile* profile;
}

@property (strong, nonatomic) NGREGBottomPanelViewController *panelViewController;

@property (weak, nonatomic) IBOutlet UIImageView *bgImage;
@property (weak, nonatomic) IBOutlet UIView *centerView;

@property (weak, nonatomic) IBOutlet UITextField *fullNameText;
@property (weak, nonatomic) IBOutlet UITextField *dateBirthText;
@property (weak, nonatomic) IBOutlet UITextField *monthBirthText;
@property (weak, nonatomic) IBOutlet UITextField *yearBirthText;
@property (weak, nonatomic) IBOutlet UITextField *giidNumberText;
@property (weak, nonatomic) IBOutlet UITextField *emailText;
@property (weak, nonatomic) IBOutlet UITextField *cityText;
@property (weak, nonatomic) IBOutlet NGREGTextField *phoneText;
@property (weak, nonatomic) IBOutlet UITextField *subBrandsText;
@property (weak, nonatomic) IBOutlet UITextField *twitterText;

@property (weak, nonatomic) IBOutlet UIButton *updateButton;
@property (weak, nonatomic) IBOutlet UIButton *confirmButton;

@property (weak, nonatomic) IBOutlet UILabel *registrationLabel;
@property (weak, nonatomic) IBOutlet UILabel *fullNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateOfBirthLabel;
@property (weak, nonatomic) IBOutlet UILabel *giidNumberLabel;
@property (weak, nonatomic) IBOutlet UILabel *cityLabel;
@property (weak, nonatomic) IBOutlet UILabel *maleLabel;
@property (weak, nonatomic) IBOutlet UILabel *femaleLabel;
@property (weak, nonatomic) IBOutlet UILabel *emailLabel;
@property (weak, nonatomic) IBOutlet UILabel *mobilePhoneLabel;
@property (weak, nonatomic) IBOutlet UILabel *subBrandsLabel;
@property (weak, nonatomic) IBOutlet UILabel *examplePhoneNumberLabel;
@property (weak, nonatomic) IBOutlet UILabel *twitterLabel;

@property (weak, nonatomic) IBOutlet UIButton *radioMale;
@property (weak, nonatomic) IBOutlet UIButton *radioFemale;

@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UIView *loadingView;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

@property (nonatomic, strong) AMPopTip *popTip;

@property (weak, nonatomic) IBOutlet UILabel *whereAreYouLabel;

@end

@implementation NGREGRegisterRegistrantViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    appDelegate = (NGREGAppDelegate *)[[UIApplication sharedApplication] delegate];
    [self setUI];
    
    [self.fullNameText setDelegate:self];
    [self.giidNumberText setDelegate:self];
    [self.emailText setDelegate:self];
    [self.phoneText setDelegate:self];
    [self.cityText setDelegate:self];
    [self.dateBirthText setDelegate:self];
    [self.monthBirthText setDelegate:self];
    [self.yearBirthText setDelegate:self];
    [self.subBrandsText setDelegate:self];
    [self.twitterText setDelegate:self];
    
    //    [[NSNotificationCenter defaultCenter] addObserver:self
    //                                             selector:@selector(keyboardWillShow:)
    //                                                 name:UIKeyboardWillShowNotification
    //                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardEndShow:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    //    [[NSNotificationCenter defaultCenter] addObserver:self
    //                                             selector:@selector(keyboardEndShow:)
    //                                                 name:UIKeyboardDidHideNotification
    //                                               object:nil];
    
    [self.radioFemale setSelected:NO];
    [self.radioMale setSelected:NO];
    
    if(self.registrant){
        [self.fullNameText setText:self.registrant.fullName];
        [self.giidNumberText setText:self.registrant.gIIDNumber];
        [self.emailText setText:self.registrant.email];
        [self.phoneText setText:self.registrant.mobile];
        [self.twitterText setText:self.registrant.socialAccount];
        
        if(self.registrant.sex.length == 0){
            [self.radioFemale setSelected:NO];
            [self.radioMale setSelected:NO];
        }else{
            genderTypeFemale = [[self.registrant.sex uppercaseString]isEqualToString:@"F"];
            [self changeGender];
        }
        
        if([self.registrant.dateOfBirth length] > 0){
            
            //            self.dateBirthText.text = self.registrant.dateOfBirth;
            
            NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
            
//            [dateFormat setDateFormat:@"yyyy-MM-dd"];
            [dateFormat setDateFormat:@"MM/dd/yyy"];
            
            NSDate *date = [dateFormat dateFromString:self.registrant.dateOfBirth];
            [dateFormat setDateFormat:@"dd MMM yyyy"];
            self.dateBirthText.text = [dateFormat stringFromDate:date];
            
            //            [dateFormat setDateFormat:@"MM"];
            //            self.monthBirthText.text = [dateFormat stringFromDate:date];
            //
            //            [dateFormat setDateFormat:@"yyyy"];
            //            self.yearBirthText.text = [dateFormat stringFromDate:date];
        }
        
        if([self.registrant.registrationID length] >0 | [self.registrant.fullName length] >0 | [self.registrant.firstName length] >0){
            self.registrationLabel.text = @"CONFIRMATION";
            [self.fullNameText setEnabled:NO];
            [self.giidNumberText setEnabled:NO];
            [self.emailText setEnabled:NO];
            [self.phoneText setEnabled:NO];
            [self.cityText setEnabled:NO];
            [self.dateBirthText setEnabled:NO];
            [self.monthBirthText setEnabled:NO];
            [self.yearBirthText setEnabled:NO];
            [self.subBrandsText setEnabled:NO];
            [self.twitterText setEnabled:NO];
            
            [self.fullNameText setBackgroundColor:[NGREGUtil colorFromHexString:[profile.customizeTemplates objectForKey:@"form-cell-background-disable"]]];
            [self.giidNumberText setBackgroundColor:[NGREGUtil colorFromHexString:[profile.customizeTemplates objectForKey:@"form-cell-background-disable"]]];
            [self.emailText setBackgroundColor:[NGREGUtil colorFromHexString:[profile.customizeTemplates objectForKey:@"form-cell-background-disable"]]];
            [self.phoneText setBackgroundColor:[NGREGUtil colorFromHexString:[profile.customizeTemplates objectForKey:@"form-cell-background-disable"]]];
            [self.cityText setBackgroundColor:[NGREGUtil colorFromHexString:[profile.customizeTemplates objectForKey:@"form-cell-background-disable"]]];
            [self.dateBirthText setBackgroundColor:[NGREGUtil colorFromHexString:[profile.customizeTemplates objectForKey:@"form-cell-background-disable"]]];
            [self.monthBirthText setBackgroundColor:[NGREGUtil colorFromHexString:[profile.customizeTemplates objectForKey:@"form-cell-background-disable"]]];
            [self.yearBirthText setBackgroundColor:[NGREGUtil colorFromHexString:[profile.customizeTemplates objectForKey:@"form-cell-background-disable"]]];
            [self.subBrandsText setBackgroundColor:[NGREGUtil colorFromHexString:[profile.customizeTemplates objectForKey:@"form-cell-background-disable"]]];
            [self.twitterText setBackgroundColor:[NGREGUtil colorFromHexString:[profile.customizeTemplates objectForKey:@"form-cell-background-disable"]]];
            
            [self.confirmButton setTitle:@"CONFIRM" forState:UIControlStateNormal];
            //            [self.confirmButton setEnabled:NO];
        }else{
            self.registrationLabel.text = @"REGISTRATION";
            [self.confirmButton setTitle:@"NEXT" forState:UIControlStateNormal];
            [self.confirmButton setEnabled:YES];
            [self.updateButton setTitle:@"BACK" forState:UIControlStateNormal];
            [self.updateButton setEnabled:YES];
            //            [self.updateButton setHidden:YES];
        }
    }
    
    //    [[NGREGEngine sharedEngine]requestListSubBrandsWithCompletionHandler:^(id result, NSError *error) {
    //        NGREGCityResponse *res = result;
    //        NSLog(@"Sub brands : %@", res.data);
    //        [self loadLocalSubBrands];
    //    }];
    //
    //    [[NGREGEngine sharedEngine]requestListCityWithCompletionHandler:^(id result, NSError *error) {
    //        [self loadLocalCity];
    //    }];
    
    [self loadLocalSubBrands];
    [self loadLocalCity];
    
    //    [self.scrollView addSubview:self.contentView];
    //
    //    // DO NOT change the property translatesAutoresizingMaskIntoConstraints for contentView, which defaults to YES;
    //    self.contentView.constant = 500;
    //
    //    // Set the content size of myScrollView to match the size of the content view:
    [self.scrollView setContentSize:CGSizeMake(1024.0f, 555.0f)];
    //
    
    // by pass ke game
    //    self.didFinishRegisterRegistrant(self.registrant);
    
//    [[AMPopTip appearance] setFont:self.emailText.font];
    
    self.popTip = [AMPopTip popTip];
    self.popTip.shouldDismissOnTap = YES;
    self.popTip.shouldDismissOnTapOutside = YES;
    
    self.popTip.popoverColor = [NGREGUtil colorFromHexString:@"#333333"];
    self.popTip.textColor = [NGREGUtil colorFromHexString:@"#ffffff"];
    
//    self.scrollView.panGestureRecognizer.delaysTouchesBegan = self.scrollView.delaysContentTouches;
//    UITapGestureRecognizer *nothingTap = [[UITapGestureRecognizer alloc] init];
//    nothingTap.delaysTouchesBegan = YES;
//    [self.scrollView addGestureRecognizer:nothingTap];
    
    _originalViewFrame = self.scrollView.frame;
    
    
}

- (void)viewWillAppear:(BOOL)animated{
    NSArray* ar = [NGREGProfile profileWithEmail:[[NSUserDefaults standardUserDefaults] valueForKey:kNGREGEmailNextGen] InManagedObjectContext:[appDelegate managedObjectContext]];
     profile = [ar lastObject];
    
    //    [self.bgImage setImageWithURL:[NSURL URLWithString:[profile.customizeTemplates valueForKey:@"global"]]];
//    [self.bgImage setImageWithURL:[NSURL URLWithString:[profile.customizeTemplates valueForKey:@"global"]] placeholderImage:nil options:SDWebImageRetryFailed];
    
    [self loadImage:self.bgImage withUrl:[profile.customizeTemplates valueForKey:@"global"] andMD5:[profile.customizeTemplates valueForKey:@"global_md5"]];
    
    self.panelViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"BottomPanel"];
    [self.panelViewController setDelegate:self];
    [self addChildViewController:self.panelViewController];
    
    CGRect panelViewFrame = CGRectMake(0.0f, self.view.bounds.size.height - kPanelViewVisibleHeight, 1024.0f, 500);
    self.panelViewController.view.frame = panelViewFrame;
    [self.view addSubview:self.panelViewController.view];
    
    // set panel bottom
    [self.panelViewController setDataArray:@[@{@"name":@"< HOME"}]];
    
    [self.panelViewController setTextSize:[[profile.customizeTemplates valueForKey:@"text-size"] integerValue] andTextColor:[profile.customizeTemplates valueForKey:@"text-label-menu-color"] andBackgroundColor:[profile.customizeTemplates valueForKey:@"text-label-menu-background-color"]andFontId:[[profile.customizeTemplates valueForKey:@"text-style"] integerValue] andBgUrl:[profile.customizeTemplates valueForKey:@"button-menu"] andMD5:[profile.customizeTemplates valueForKey:@"button-menu_md5"]];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Action methods

- (IBAction)dateOfBirthClicked:(UIButton*)sender {
    //    [self.scrollView setContentOffset:CGPointMake(0,100) animated:YES];
    
    [self.popTip hide];
    
    if(!self.dateBirthText.enabled)
        return;
    
    [self.view endEditing:YES];
    
    if(_popoverDateOfBirthViewController != nil){
        [_popoverDateOfBirthViewController dismissPopoverAnimated:NO];
    }
    
    NGREGDatePickerPopupViewController *datePickerViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"Date Picker"];
    datePickerViewController.isBirthday = YES;
    
    _popoverDateOfBirthViewController = [[WYPopoverController alloc] initWithContentViewController:datePickerViewController];
    
    NSString* dateString = @"";
    
    
    
    if([self.dateBirthText.text length]>0)
        dateString = self.dateBirthText.text;
    
    datePickerViewController.date = dateString;
    [datePickerViewController setDone:^(NSString * dateString, NSDate * date) {
        
        //        self.dateBirthText.text = dateString;
        
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        
        [dateFormat setDateFormat:@"dd MMM yyyy"];
        self.dateBirthText.text = [dateFormat stringFromDate:date];
        
        [_popoverDateOfBirthViewController dismissPopoverAnimated:YES];
        _popoverDateOfBirthViewController.delegate = nil;
    }];
    
    
    datePickerViewController.preferredContentSize = CGSizeMake(kBEATDatePickerContentWidth, kBEATDatePickerContentHeight);
    
    [[WYPopoverBackgroundView appearance] setFillTopColor:[UIColor whiteColor]];
    
    _popoverDateOfBirthViewController.passthroughViews = @[sender];
    _popoverDateOfBirthViewController.popoverLayoutMargins = UIEdgeInsetsMake(0.0f, 0.0f, 0.0f, 0.0f);
    _popoverDateOfBirthViewController.wantsDefaultContentAppearance = NO;
    [_popoverDateOfBirthViewController presentPopoverFromRect:sender.bounds inView:sender permittedArrowDirections:WYPopoverArrowDirectionAny animated:YES];
}

- (IBAction)cityClicked:(id)sender {
    
    if (_popoverViewController == nil) {
        
        _cityPicker = [self.storyboard instantiateViewControllerWithIdentifier:@"City Picker"];
        _cityPicker.delegate = self;
        
        _cityPicker.preferredContentSize = CGSizeMake(230.0f, 180.0f);
        
        [[WYPopoverBackgroundView appearance] setFillTopColor:[UIColor clearColor]];
        
        _popoverViewController = [[WYPopoverController alloc] initWithContentViewController:_cityPicker];
        _popoverViewController.delegate = self;
        _popoverViewController.passthroughViews = @[self.cityText];
        _popoverViewController.popoverLayoutMargins = UIEdgeInsetsMake(0.0f, 0.0f, 0.0f, 0.0f);
        _popoverViewController.wantsDefaultContentAppearance = NO;
        [_popoverViewController presentPopoverFromRect:self.cityText.bounds inView:self.cityText permittedArrowDirections:WYPopoverArrowDirectionUp animated:YES];
        
        
    } else {
        
        [_popoverViewController dismissPopoverAnimated:YES];
        _popoverViewController.delegate = nil;
        _popoverViewController = nil;
    }
}

- (IBAction)hideKeyboard:(id)sender {
    [self.view endEditing:YES];
}

- (IBAction)updateButtonClicked:(id)sender {
    [self.view endEditing:YES];
    
    [self.popTip hide];
    if([self.updateButton.titleLabel.text isEqualToString:@"BACK"]){
        [self.navigationController popViewControllerAnimated:NO];
        return;
    }
    
    if([self.registrant.registrationID length] >0){
        [self.fullNameText setEnabled:NO];
        [self.giidNumberText setEnabled:NO];
        [self.dateBirthText setEnabled:NO];
        
        [self.fullNameText setBackgroundColor:[NGREGUtil colorFromHexString:[profile.customizeTemplates objectForKey:@"form-cell-background-disable"]]];
        [self.giidNumberText setBackgroundColor:[NGREGUtil colorFromHexString:[profile.customizeTemplates objectForKey:@"form-cell-background-disable"]]];
        [self.dateBirthText setBackgroundColor:[NGREGUtil colorFromHexString:[profile.customizeTemplates objectForKey:@"form-cell-background-disable"]]];
        
    }else{
        [self.fullNameText setEnabled:YES];
        [self.fullNameText becomeFirstResponder];
        [self.giidNumberText setEnabled:YES];
        [self.dateBirthText setEnabled:YES];
        
        [self.fullNameText setBackgroundColor:[NGREGUtil colorFromHexString:[profile.customizeTemplates objectForKey:@"form-cell-background"]]];
        [self.giidNumberText setBackgroundColor:[NGREGUtil colorFromHexString:[profile.customizeTemplates objectForKey:@"form-cell-background"]]];
        [self.dateBirthText setBackgroundColor:[NGREGUtil colorFromHexString:[profile.customizeTemplates objectForKey:@"form-cell-background"]]];
    }
    
    [self.emailText setEnabled:YES];
    [self.phoneText setEnabled:YES];
    [self.cityText setEnabled:YES];
    [self.monthBirthText setEnabled:YES];
    [self.yearBirthText setEnabled:YES];
    [self.subBrandsText setEnabled:YES];
    [self.confirmButton setEnabled:YES];
    [self.twitterText setEnabled:YES];
    
    [self.emailText setBackgroundColor:[NGREGUtil colorFromHexString:[profile.customizeTemplates objectForKey:@"form-cell-background"]]];
    [self.phoneText setBackgroundColor:[NGREGUtil colorFromHexString:[profile.customizeTemplates objectForKey:@"form-cell-background"]]];
    [self.cityText setBackgroundColor:[NGREGUtil colorFromHexString:[profile.customizeTemplates objectForKey:@"form-cell-background"]]];
    [self.monthBirthText setBackgroundColor:[NGREGUtil colorFromHexString:[profile.customizeTemplates objectForKey:@"form-cell-background"]]];
    [self.yearBirthText setBackgroundColor:[NGREGUtil colorFromHexString:[profile.customizeTemplates objectForKey:@"form-cell-background"]]];
    [self.subBrandsText setBackgroundColor:[NGREGUtil colorFromHexString:[profile.customizeTemplates objectForKey:@"form-cell-background"]]];
    [self.twitterText setBackgroundColor:[NGREGUtil colorFromHexString:[profile.customizeTemplates objectForKey:@"form-cell-background"]]];
}

- (IBAction)confirmButtonClicked:(id)sender {
    
    [self.popTip hide];
    
    if(![self validationEntourage])
        return;
    
    if([self.confirmButton.titleLabel.text isEqualToString:@"NEXT"]){
        
        self.registrationLabel.text = @"CONFIRMATION";
        [self.fullNameText setEnabled:NO];
        [self.giidNumberText setEnabled:NO];
        [self.emailText setEnabled:NO];
        [self.phoneText setEnabled:NO];
        [self.cityText setEnabled:NO];
        [self.dateBirthText setEnabled:NO];
        [self.monthBirthText setEnabled:NO];
        [self.yearBirthText setEnabled:NO];
        [self.subBrandsText setEnabled:NO];
        [self.twitterText setEnabled:NO];
        
        
        [self.fullNameText setBackgroundColor:[NGREGUtil colorFromHexString:[profile.customizeTemplates objectForKey:@"form-cell-background-disable"]]];
        [self.giidNumberText setBackgroundColor:[NGREGUtil colorFromHexString:[profile.customizeTemplates objectForKey:@"form-cell-background-disable"]]];
        [self.emailText setBackgroundColor:[NGREGUtil colorFromHexString:[profile.customizeTemplates objectForKey:@"form-cell-background-disable"]]];
        [self.phoneText setBackgroundColor:[NGREGUtil colorFromHexString:[profile.customizeTemplates objectForKey:@"form-cell-background-disable"]]];
        [self.cityText setBackgroundColor:[NGREGUtil colorFromHexString:[profile.customizeTemplates objectForKey:@"form-cell-background-disable"]]];
        [self.dateBirthText setBackgroundColor:[NGREGUtil colorFromHexString:[profile.customizeTemplates objectForKey:@"form-cell-background-disable"]]];
        [self.monthBirthText setBackgroundColor:[NGREGUtil colorFromHexString:[profile.customizeTemplates objectForKey:@"form-cell-background-disable"]]];
        [self.yearBirthText setBackgroundColor:[NGREGUtil colorFromHexString:[profile.customizeTemplates objectForKey:@"form-cell-background-disable"]]];
        [self.subBrandsText setBackgroundColor:[NGREGUtil colorFromHexString:[profile.customizeTemplates objectForKey:@"form-cell-background-disable"]]];
        [self.twitterText setBackgroundColor:[NGREGUtil colorFromHexString:[profile.customizeTemplates objectForKey:@"form-cell-background-disable"]]];
        
        [self.confirmButton setTitle:@"CONFIRM" forState:UIControlStateNormal];
        [self.updateButton setHidden:NO];
        [self.updateButton setTitle:@"UPDATE" forState:UIControlStateNormal];
    }else{
        
        NSString* email = [[NSUserDefaults standardUserDefaults] valueForKey:kNGREGEmailNextGen];
        NSString* brandId = [[NSUserDefaults standardUserDefaults] valueForKey:kNGREGBrandIDNextGen];
        
        self.registrant.userId = email;
        self.registrant.eventId = brandId;
        
//        for (int i = 0; i < 100; i++) {
        
            [NGREGRegistrant registrantWithRegistrant:self.registrant inManagedObjectContext:[appDelegate managedObjectContext]];
//        }
        
        [self cekLogging];
        
        if([self.registrant.registrationID length] >0){
            NSString* menu = nil;
            
            if([self.menu objectForKey:@"name"] ){
                menu = [self.menu valueForKey:@"name"];
            }else{
                menu = [self.menu valueForKey:@"menuName"];
            }
            
            //            [NGREGReport reportExistingRegistrantWithEmail:[[NSUserDefaults standardUserDefaults] valueForKey:kNGREGEmailNextGen] andMenu:menu inManagedObjectContext:[appDelegate managedObjectContext]];
            
            [[NGREGEngine sharedEngine] requestRegisterRegistrant:self.registrant atEvent:menu withCompletionHandler:^(id result, NSError *error) {
                
            }];
            self.didFinishRegisterRegistrant(self.registrant);
        }else{
            NGREGSignInViewController *signView = [self.storyboard instantiateViewControllerWithIdentifier:@"SignIn"];
            [signView setRegistrant:self.registrant];
            [signView setMenu:self.menu];
            [signView setDidFinishRegisterRegistrant:^(NGREGCheckRegistrantResponse * registrant) {
                self.didFinishRegisterRegistrant(registrant);
            }];
            
            [self.navigationController pushViewController:signView animated:NO];
        }
        
    }
}

- (IBAction)subBrandClicked:(id)sender {
    if(!self.subBrandsText.enabled)
        return;
    
    
    [self.popTip hide];
    
    //    [self.view endEditing:YES];
    
    if(_popoverSubBrandViewController != nil){
        [_popoverSubBrandViewController dismissPopoverAnimated:YES];
        _popoverSubBrandViewController.delegate = nil;
        _popoverSubBrandViewController = nil;
    }
    
    NGREGSubBrandPopoverViewController *subBrandViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"SubBrandPopover"];
    [subBrandViewController setBrands:subBrands];
    
    [subBrandViewController setSelectedBrand:^(NSDictionary * brandSelected) {
        
        _selectedSubBrands = brandSelected;
        [self.subBrandsText setText:[brandSelected objectForKey:@"SubBrandName"]];
        [_popoverSubBrandViewController dismissPopoverAnimated:YES];
        
    }];
    
    _popoverSubBrandViewController = [[WYPopoverController alloc] initWithContentViewController:subBrandViewController];
    
    subBrandViewController.preferredContentSize = CGSizeMake(300, 230); // iOS 7
    
    [[WYPopoverBackgroundView appearance] setFillTopColor:[UIColor whiteColor]];
    
    _popoverSubBrandViewController.passthroughViews = @[sender];
    _popoverSubBrandViewController.delegate = self;
    _popoverSubBrandViewController.popoverLayoutMargins = UIEdgeInsetsMake(0.0f, 0.0f, 0.0f, 0.0f);
    _popoverSubBrandViewController.wantsDefaultContentAppearance = NO;
    [_popoverSubBrandViewController presentPopoverFromRect:self.subBrandsText.bounds inView:self.subBrandsText permittedArrowDirections:WYPopoverArrowDirectionAny animated:YES];
}

- (IBAction)changeGenderMale:(id)sender {
    
    [self.popTip hide];
    if(!self.fullNameText.enabled)
        return;
    
    genderTypeFemale = false;
    [self changeGender];
}

- (IBAction)changeGenderFemale:(id)sender {
    
    [self.popTip hide];
    if(!self.fullNameText.enabled)
        return;
    
    genderTypeFemale = true;
    [self changeGender];
}


#pragma mark - Keyboard Management

- (void)keyboardWillShow:(NSNotification *)notification {
    
    [self.popTip hide];
    
    NSDictionary* userInfo = [notification userInfo];
    
    // we don't use SDK constants here to be universally compatible with all SDKs ≥ 3.0
    NSValue* keyboardFrameValue = [userInfo objectForKey:@"UIKeyboardBoundsUserInfoKey"];
    if (!keyboardFrameValue) {
        keyboardFrameValue = [userInfo objectForKey:@"UIKeyboardFrameEndUserInfoKey"];
    }
    
    //    _originalViewFrame = self.view.frame;
    //
    //    CGRect frame = CGRectMake(_originalViewFrame.origin.x, -110.0f, _originalViewFrame.size.width, _originalViewFrame.size.height);
    //
    //    CGFloat animationDuration = [[userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] floatValue];
    //    [UIView animateWithDuration:animationDuration animations:^{
    //
    //        self.view.frame = frame;
    //    }];
    
    
    CGRect frame = CGRectMake(_originalViewFrame.origin.x, _originalViewFrame.origin.y, _originalViewFrame.size.width, 290);
    
    CGFloat animationDuration = [[userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] floatValue];
    [UIView animateWithDuration:animationDuration animations:^{
        
        self.scrollView.frame = frame;
//        self.contentView.frame = CGRectMake(self.contentView.frame.origin.x, self.contentView.frame.origin.y, self.contentView.frame.size.width, 845);
        [self.scrollView setTranslatesAutoresizingMaskIntoConstraints:YES];
        [self.contentView setTranslatesAutoresizingMaskIntoConstraints:YES];
    } completion:^(BOOL finished) {
        
        [self.scrollView setTranslatesAutoresizingMaskIntoConstraints:YES];
        [self.contentView setTranslatesAutoresizingMaskIntoConstraints:YES];
    }];
    
}

- (void)keyboardWillHide:(NSNotification *)notification {
    
	NSDictionary* userInfo = [notification userInfo];
    CGFloat animationDuration = [[userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] floatValue];
    
    [UIView animateWithDuration:animationDuration animations:^{
        
        self.scrollView.frame = _originalViewFrame;
//        self.contentView.frame = CGRectMake(self.contentView.frame.origin.x, self.contentView.frame.origin.y, self.contentView.frame.size.width, 555);
        [self.scrollView setTranslatesAutoresizingMaskIntoConstraints:YES];
        [self.contentView setTranslatesAutoresizingMaskIntoConstraints:YES];
        
    } completion:^(BOOL finished) {
        
        self.scrollView.frame = _originalViewFrame;
        [self.scrollView setTranslatesAutoresizingMaskIntoConstraints:YES];
        [self.contentView setTranslatesAutoresizingMaskIntoConstraints:YES];
        
    }];
}

- (void)keyboardDidShow:(NSNotification *)notification {
    
}

- (void)keyboardEndShow:(NSNotification *)notification {
    
    
        [UIView animateWithDuration:0.3f animations:^{
    
            self.scrollView.frame = _originalViewFrame;
            [self.scrollView setTranslatesAutoresizingMaskIntoConstraints:YES];
    
    
        } completion:^(BOOL finished) {
            [self.scrollView setTranslatesAutoresizingMaskIntoConstraints:YES];
            [self.scrollView setContentOffset:CGPointMake(0,0) animated:YES];
            
        }];
}

#pragma mark - Helpher methods

- (void)changeGender{
    if(genderTypeFemale){
        [self.radioFemale setSelected:YES];
        [self.radioMale setSelected:NO];
    }else{
        [self.radioFemale setSelected:NO];
        [self.radioMale setSelected:YES];
    }
}

- (void)loadLocalCity{
    
    NSString *cityFile = [[NSBundle mainBundle] pathForResource:@"city_registration_entourage" ofType:@"csv"];
    [CSVParser parseCSVIntoArrayOfDictionariesFromFile:cityFile
                          withSeparatedCharacterString:@","
                                  quoteCharacterString:nil
                                             withBlock:^(NSArray *array, NSError *error) {
                                                 cities = array;
                                                 
                                                 if([self.registrant.city length]>0){
                                                     
                                                     NSArray* subbrand1 = [cities filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"cityId==%@",self.registrant.city]];
                                                     if(subbrand1.count > 0){
                                                         self.cityText.text = [[[subbrand1 objectAtIndex:0]objectForKey:@"cityName"]capitalizedString];
                                                         _selectedCity =[subbrand1 objectAtIndex:0];
                                                     }
                                                     
                                                 }                                             }];
    
}

- (void)loadLocalSubBrands{
    
    NSString *file = [[NSBundle mainBundle] pathForResource:@"subbrand_prod" ofType:@"csv"];
    [CSVParser parseCSVIntoArrayOfDictionariesFromFile:file
                          withSeparatedCharacterString:@","
                                  quoteCharacterString:nil
                                             withBlock:^(NSArray *array, NSError *error) {
                                                 //                                                 NSPredicate* predicate = [NSPredicate predicateWithFormat:@"Data8==%@",@"1"];
                                                 //                                                 subBrands = [array filteredArrayUsingPredicate:predicate];
                                                 
                                                 subBrands = array;
                                                 
                                                 if([self.registrant.brand1Id length] > 0){
                                                     
                                                     NSArray* subbrand1 = [subBrands filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"SubBrandId==%@",self.registrant.brand1Id]];
                                                     if(subbrand1.count > 0){
                                                         self.subBrandsText.text = [[[subbrand1 objectAtIndex:0]objectForKey:@"SubBrandName"]capitalizedString];
                                                         _selectedSubBrands =[subbrand1 objectAtIndex:0];
                                                     }
                                                     if(self.subBrandsText.text.length == 0){
                                                         self.subBrandsText.text = [[[array lastObject]objectForKey:@"SubBrandName"]capitalizedString];
                                                         _selectedSubBrands =[subBrands lastObject];
                                                     }
                                                 }
                                                 
                                             }];
    
}

- (void)setUI{
    
    NSArray* ar = [NGREGProfile profileWithEmail:[[NSUserDefaults standardUserDefaults] valueForKey:kNGREGEmailNextGen] InManagedObjectContext:[appDelegate managedObjectContext]];
     profile = [ar lastObject];
    
    // set text color
    [self.registrationLabel setTextColor:[NGREGUtil colorFromHexString:[profile.customizeTemplates valueForKey:@"text-label-menu-background-color"] ]];
    [self.emailLabel setTextColor:[NGREGUtil colorFromHexString:[profile.customizeTemplates valueForKey:@"text-label-color"] ]];
    [self.fullNameLabel setTextColor:[NGREGUtil colorFromHexString:[profile.customizeTemplates valueForKey:@"text-label-color"] ]];
    [self.giidNumberLabel setTextColor:[NGREGUtil colorFromHexString:[profile.customizeTemplates valueForKey:@"text-label-color"] ]];
    [self.subBrandsLabel setTextColor:[NGREGUtil colorFromHexString:[profile.customizeTemplates valueForKey:@"text-label-color"] ]];
    [self.mobilePhoneLabel setTextColor:[NGREGUtil colorFromHexString:[profile.customizeTemplates valueForKey:@"text-label-color"] ]];
    [self.dateOfBirthLabel setTextColor:[NGREGUtil colorFromHexString:[profile.customizeTemplates valueForKey:@"text-label-color"] ]];
    [self.cityLabel setTextColor:[NGREGUtil colorFromHexString:[profile.customizeTemplates valueForKey:@"text-label-color"] ]];
    [self.maleLabel setTextColor:[NGREGUtil colorFromHexString:[profile.customizeTemplates valueForKey:@"text-label-color"] ]];
    [self.femaleLabel setTextColor:[NGREGUtil colorFromHexString:[profile.customizeTemplates valueForKey:@"text-label-color"] ]];
    [self.twitterLabel setTextColor:[NGREGUtil colorFromHexString:[profile.customizeTemplates valueForKey:@"text-label-color"] ]];
    [self.examplePhoneNumberLabel setTextColor:[NGREGUtil colorFromHexString:[profile.customizeTemplates valueForKey:@"text-label-color"] ]];
    [self.whereAreYouLabel setTextColor:[NGREGUtil colorFromHexString:[profile.customizeTemplates valueForKey:@"text-label-color"] ]];
//    
//    [self.updateButton setBackgroundImageWithURL:[NSURL URLWithString:[profile.customizeTemplates valueForKey:@"button-submit"]] forState:UIControlStateNormal placeholderImage:nil options:SDWebImageRetryFailed];
    [self.updateButton setTintColor:[NGREGUtil colorFromHexString:[profile.customizeTemplates valueForKey:@"text-label-menu-color"] ]];
//    [self.confirmButton setBackgroundImageWithURL:[NSURL URLWithString:[profile.customizeTemplates valueForKey:@"button-submit"]] forState:UIControlStateNormal placeholderImage:nil options:SDWebImageRetryFailed];
    [self.confirmButton setTintColor:[NGREGUtil colorFromHexString:[profile.customizeTemplates valueForKey:@"text-label-menu-color"] ]];
    
    [self loadImageButton:self.updateButton forState:UIControlStateNormal withUrl:[profile.customizeTemplates objectForKey:@"button-submit"] andMD5:[profile.customizeTemplates objectForKey:@"button-submit_md5"]];
    [self loadImageButton:self.confirmButton forState:UIControlStateNormal withUrl:[profile.customizeTemplates objectForKey:@"button-submit"] andMD5:[profile.customizeTemplates objectForKey:@"button-submit_md5"]];
    
    // set text size label
    [self.emailLabel setFont:[UIFont systemFontOfSize:[[profile.customizeTemplates objectForKey:@"label-text-size"] intValue] ]];
    [self.fullNameLabel setFont:[UIFont systemFontOfSize:[[profile.customizeTemplates objectForKey:@"label-text-size"] intValue] ]];
    [self.giidNumberLabel setFont:[UIFont systemFontOfSize:[[profile.customizeTemplates objectForKey:@"label-text-size"] intValue] ]];
    [self.subBrandsLabel setFont:[UIFont systemFontOfSize:[[profile.customizeTemplates objectForKey:@"label-text-size"] intValue] ]];
    [self.mobilePhoneLabel setFont:[UIFont systemFontOfSize:[[profile.customizeTemplates objectForKey:@"label-text-size"] intValue] ]];
    [self.dateOfBirthLabel setFont:[UIFont systemFontOfSize:[[profile.customizeTemplates objectForKey:@"label-text-size"] intValue] ]];
    [self.cityLabel setFont:[UIFont systemFontOfSize:[[profile.customizeTemplates objectForKey:@"label-text-size"] intValue] ]];
    [self.maleLabel setFont:[UIFont systemFontOfSize:[[profile.customizeTemplates objectForKey:@"label-text-size"] intValue] ]];
    [self.femaleLabel setFont:[UIFont systemFontOfSize:[[profile.customizeTemplates objectForKey:@"label-text-size"] intValue] ]];
    [self.twitterLabel setFont:[UIFont systemFontOfSize:[[profile.customizeTemplates objectForKey:@"label-text-size"] intValue] ]];
    [self.examplePhoneNumberLabel setFont:[UIFont systemFontOfSize:[[profile.customizeTemplates objectForKey:@"label-text-size"] intValue] ]];
    [self.whereAreYouLabel setFont:[UIFont systemFontOfSize:[[profile.customizeTemplates objectForKey:@"label-text-size"] intValue] ]];
    
    // set textbox textsize
    [self.emailText setFont:[UIFont systemFontOfSize:[[profile.customizeTemplates objectForKey:@"textbox-text-size"] intValue] ]];
    [self.fullNameText setFont:[UIFont systemFontOfSize:[[profile.customizeTemplates objectForKey:@"textbox-text-size"] intValue] ]];
    [self.giidNumberText setFont:[UIFont systemFontOfSize:[[profile.customizeTemplates objectForKey:@"textbox-text-size"] intValue] ]];
    [self.subBrandsText setFont:[UIFont systemFontOfSize:[[profile.customizeTemplates objectForKey:@"textbox-text-size"] intValue] ]];
    [self.phoneText setFont:[UIFont systemFontOfSize:[[profile.customizeTemplates objectForKey:@"textbox-text-size"] intValue] ]];
    [self.dateBirthText setFont:[UIFont systemFontOfSize:[[profile.customizeTemplates objectForKey:@"textbox-text-size"] intValue] ]];
    [self.cityText setFont:[UIFont systemFontOfSize:[[profile.customizeTemplates objectForKey:@"textbox-text-size"] intValue] ]];
    [self.twitterText setFont:[UIFont systemFontOfSize:[[profile.customizeTemplates objectForKey:@"textbox-text-size"] intValue] ]];
    
    // set button textsize
    [self.updateButton.titleLabel setFont:[UIFont systemFontOfSize:[[profile.customizeTemplates objectForKey:@"textbox-text-size"] intValue] ]];
    [self.confirmButton.titleLabel setFont:[UIFont systemFontOfSize:[[profile.customizeTemplates objectForKey:@"textbox-text-size"] intValue] ]];
    
    // set textbox background
    [self.emailText setBackgroundColor:[NGREGUtil colorFromHexString:[profile.customizeTemplates objectForKey:@"form-cell-background"]]];
    [self.fullNameText setBackgroundColor:[NGREGUtil colorFromHexString:[profile.customizeTemplates objectForKey:@"form-cell-background"]]];
    [self.giidNumberText setBackgroundColor:[NGREGUtil colorFromHexString:[profile.customizeTemplates objectForKey:@"form-cell-background"]]];
    [self.subBrandsText setBackgroundColor:[NGREGUtil colorFromHexString:[profile.customizeTemplates objectForKey:@"form-cell-background"]]];
    [self.phoneText setNormalBackgroundColor:[NGREGUtil colorFromHexString:[profile.customizeTemplates objectForKey:@"form-cell-background"]]];
    [self.dateBirthText setBackgroundColor:[NGREGUtil colorFromHexString:[profile.customizeTemplates objectForKey:@"form-cell-background"]]];
    [self.cityText setBackgroundColor:[NGREGUtil colorFromHexString:[profile.customizeTemplates objectForKey:@"form-cell-background"]]];
    [self.twitterText setBackgroundColor:[NGREGUtil colorFromHexString:[profile.customizeTemplates objectForKey:@"form-cell-background"]]];
    
    // set textbox disable background
    [self.phoneText setDisableBackgroundColor:[NGREGUtil colorFromHexString:[profile.customizeTemplates objectForKey:@"form-cell-background-disable"]]];
    
    // set textbox text color
    [self.emailText setTextColor:[NGREGUtil colorFromHexString:[profile.customizeTemplates objectForKey:@"form-cell-text-color"]]];
    [self.fullNameText setTextColor:[NGREGUtil colorFromHexString:[profile.customizeTemplates objectForKey:@"form-cell-text-color"]]];
    [self.giidNumberText setTextColor:[NGREGUtil colorFromHexString:[profile.customizeTemplates objectForKey:@"form-cell-text-color"]]];
    [self.subBrandsText setTextColor:[NGREGUtil colorFromHexString:[profile.customizeTemplates objectForKey:@"form-cell-text-color"]]];
    [self.phoneText setTextColor:[NGREGUtil colorFromHexString:[profile.customizeTemplates objectForKey:@"form-cell-text-color"]]];
    [self.dateBirthText setTextColor:[NGREGUtil colorFromHexString:[profile.customizeTemplates objectForKey:@"form-cell-text-color"]]];
    [self.cityText setTextColor:[NGREGUtil colorFromHexString:[profile.customizeTemplates objectForKey:@"form-cell-text-color"]]];
    [self.twitterText setTextColor:[NGREGUtil colorFromHexString:[profile.customizeTemplates objectForKey:@"form-cell-text-color"]]];
    
    
    // set font
    [NGREGUtil setFontFromThisView:self.emailText andIdFont:[[profile.customizeTemplates valueForKey:@"text-style"] integerValue]];
    [NGREGUtil setFontFromThisView:self.fullNameLabel andIdFont:[[profile.customizeTemplates valueForKey:@"text-style"] integerValue]];
    [NGREGUtil setFontFromThisView:self.fullNameText andIdFont:[[profile.customizeTemplates valueForKey:@"text-style"] integerValue]];
    [NGREGUtil setFontFromThisView:self.dateBirthText andIdFont:[[profile.customizeTemplates valueForKey:@"text-style"] integerValue]];
    [NGREGUtil setFontFromThisView:self.dateOfBirthLabel andIdFont:[[profile.customizeTemplates valueForKey:@"text-style"] integerValue]];
    [NGREGUtil setFontFromThisView:self.maleLabel andIdFont:[[profile.customizeTemplates valueForKey:@"text-style"] integerValue]];
    [NGREGUtil setFontFromThisView:self.femaleLabel andIdFont:[[profile.customizeTemplates valueForKey:@"text-style"] integerValue]];
    [NGREGUtil setFontFromThisView:self.giidNumberLabel andIdFont:[[profile.customizeTemplates valueForKey:@"text-style"] integerValue]];
    [NGREGUtil setFontFromThisView:self.giidNumberText andIdFont:[[profile.customizeTemplates valueForKey:@"text-style"] integerValue]];
    [NGREGUtil setFontFromThisView:self.emailLabel andIdFont:[[profile.customizeTemplates valueForKey:@"text-style"] integerValue]];
    [NGREGUtil setFontFromThisView:self.cityText andIdFont:[[profile.customizeTemplates valueForKey:@"text-style"] integerValue]];
    [NGREGUtil setFontFromThisView:self.cityLabel andIdFont:[[profile.customizeTemplates valueForKey:@"text-style"] integerValue]];
    [NGREGUtil setFontFromThisView:self.mobilePhoneLabel andIdFont:[[profile.customizeTemplates valueForKey:@"text-style"] integerValue]];
    [NGREGUtil setFontFromThisView:self.phoneText andIdFont:[[profile.customizeTemplates valueForKey:@"text-style"] integerValue]];
    [NGREGUtil setFontFromThisView:self.examplePhoneNumberLabel andIdFont:[[profile.customizeTemplates valueForKey:@"text-style"] integerValue]];
//    [NGREGUtil setFontFromThisView:self.whereAreYouLabel andIdFont:[[profile.customizeTemplates valueForKey:@"text-style"] integerValue]];
    [NGREGUtil setFontFromThisView:self.subBrandsLabel andIdFont:[[profile.customizeTemplates valueForKey:@"text-style"] integerValue]];
    [NGREGUtil setFontFromThisView:self.subBrandsText andIdFont:[[profile.customizeTemplates valueForKey:@"text-style"] integerValue]];
    [NGREGUtil setFontFromThisView:self.twitterLabel andIdFont:[[profile.customizeTemplates valueForKey:@"text-style"] integerValue]];
    [NGREGUtil setFontFromThisView:self.twitterText andIdFont:[[profile.customizeTemplates valueForKey:@"text-style"] integerValue]];
    
    [NGREGUtil setFontBoldFromThisView:self.registrationLabel andIdFont:[[profile.customizeTemplates valueForKey:@"text-style"] integerValue]];
    [NGREGUtil setFontBoldFromThisView:self.updateButton andIdFont:[[profile.customizeTemplates valueForKey:@"text-style"] integerValue]];
    [NGREGUtil setFontBoldFromThisView:self.confirmButton andIdFont:[[profile.customizeTemplates valueForKey:@"text-style"] integerValue]];
    
}


- (BOOL)validationEntourage{
    
    NSString* message = @"";
    UIView* view;
    
    if(self.fullNameText.text.length == 0 && self.dateBirthText.text.length == 0 && self.subBrandsText.text.length == 0 && self.phoneText.text.length == 0 && self.cityText.text.length == 0 && self.giidNumberText.text.length == 0 && self.emailText.text.length == 0 ){
        message = [[profile.messageError valueForKey:@"allfield"] valueForKey:@"mandatory_msg"];
        view = self.centerView;
        
        [self.popTip showText:message direction:AMPopTipDirectionNone maxWidth:420 inView:self.contentView fromFrame:view.frame];
        return false;
    }
    
    if(self.fullNameText.text.length == 0){
        if(message.length == 0){
            message = [[profile.messageError valueForKey:@"full_name"] valueForKey:@"mandatory_msg"];
        view = self.fullNameText;
        }
    }
    
    // firstname hanya karakter dan spasi
    if ([self.fullNameText.text rangeOfCharacterFromSet:[NSCharacterSet decimalDigitCharacterSet]].location != NSNotFound) {
        if(message.length == 0){
            //            message = @"Please fill in the right format";
            message = [[profile.messageError valueForKey:@"full_name"] valueForKey:@"wrong_format"];
//            [self.fullNameLabel becomeFirstResponder];
            view = self.fullNameText;
        }
    }
    
    if([self.fullNameText.text length] > 0){
        if([[self.fullNameText.text substringWithRange:NSMakeRange(0, 1)] isEqualToString:@" "]){
            if(message.length == 0){
                //                message = @"Please fill in the right format";
                message = [[profile.messageError valueForKey:@"full_name"] valueForKey:@"wrong_format"];
//                [self.fullNameText becomeFirstResponder];
                view = self.fullNameText;
            }
        }
    }

    if(self.dateBirthText.text.length == 0){
        if(message.length == 0){
            message = [[profile.messageError valueForKey:@"date_birth"] valueForKey:@"mandatory_msg"];
            view = self.dateBirthText;
        }
    }
    
    if(!self.radioFemale.selected && !self.radioMale.selected){
        if(message.length == 0){
            message = [[profile.messageError valueForKey:@"gender"] valueForKey:@"mandatory_msg"];
            view = self.radioMale;
        }
    }
    
    if(self.giidNumberText.text.length == 0){
        if(message.length == 0){
            message = [[profile.messageError valueForKey:@"gidnumber"] valueForKey:@"mandatory_msg"];
            view = self.giidNumberText;
        }
    }
    
    if(self.giidNumberText.text.length < 7){
        if(message.length == 0){
            message = [[profile.messageError valueForKey:@"gidnumber"] valueForKey:@"wrong_format"];
                view = self.giidNumberText;
        }
    }
    
    
    if(self.cityText.text.length == 0){
        if(message.length == 0){
            message = [[profile.messageError valueForKey:@"city"] valueForKey:@"mandatory_msg"];
                    view = self.cityText;
        }
    }
    
    // validitasi city
    bool validCity = false;
    for(int i = 0; i < cities.count ; i++){
        NSDictionary* dict = cities[i];
        if([[[dict objectForKey:@"cityName"] uppercaseString] isEqualToString:[self.cityText.text uppercaseString]]){
            validCity = true;
        }
    }
    if(!validCity && message.length == 0){
        if(message.length == 0){
            message = [[profile.messageError valueForKey:@"city"] valueForKey:@"wrong_format"];
                    view = self.cityText;
        }
    }
    
    if(self.emailText.text.length == 0 ){
        if(message.length == 0){
            message = [[profile.messageError valueForKey:@"email"] valueForKey:@"mandatory_msg"];
                    view = self.emailText;
        }
    }
    
    if(![NGREGUtil validateEmailWithString:self.emailText.text]){
        if(message.length == 0){
            message = [[profile.messageError valueForKey:@"email"] valueForKey:@"wrong_format"];
                    view = self.emailText;
        }
    }
    
    if(self.phoneText.text.length == 0){
        if(message.length == 0){
            message = [[profile.messageError valueForKey:@"mobile_phone"] valueForKey:@"mandatory_msg"];
                    view = self.phoneText;
        }
    }
    
    if(self.phoneText.text.length < 5){
        if(message.length == 0){
            message = [[profile.messageError valueForKey:@"mobile_phone"] valueForKey:@"wrong_format"];
                    view = self.phoneText;
        }
    }
    
    if(self.subBrandsText.text.length == 0){
        if(message.length == 0){
            message = [[profile.messageError valueForKey:@"favorite_brand"] valueForKey:@"mandatory_msg"];
                    view = self.subBrandsText;
        }
    }
    
    // twitter harus lebih dari 1 karakter
    if([self.twitterText.text length] > 0 && [self.twitterText.text length] < 3){
        if(message.length == 0){
            message = [[profile.messageError valueForKey:@"twiiter"] valueForKey:@"wrong_format"];
//            [self.twitterText becomeFirstResponder];
                    view = self.twitterText;
        }
    }
        
    
    if(message.length > 0){
//        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
//        [alertView show];
        if(view == self.fullNameText){
            [self.popTip showText:message direction:AMPopTipDirectionDown maxWidth:450 inView:self.contentView fromFrame:view.frame];
        }else
            [self.popTip showText:message direction:AMPopTipDirectionDown maxWidth:350 inView:self.contentView fromFrame:view.frame];
        return false;
    }
    
    self.registrant.fullName = self.fullNameText.text;
    self.registrant.city = _selectedCity[@"cityId"];
    self.registrant.brand1Id = [_selectedSubBrands objectForKey:@"SubBrandId"];
    self.registrant.socialAccount = self.twitterText.text.length > 0 ? self.twitterText.text : @"";
    self.registrant.mobile = self.phoneText.text.length > 0 ? self.phoneText.text : @"";
    self.registrant.email = self.emailText.text;
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    
    [dateFormat setDateFormat:@"dd MMM yyyy"];
    
    NSDate *date = [dateFormat dateFromString:self.dateBirthText.text];
    [dateFormat setDateFormat:@"yyyy-MM-dd"];
    
    self.registrant.dateOfBirth = [dateFormat stringFromDate:date];
    
    if(genderTypeFemale)
        self.registrant.sex = @"F";
    else
        self.registrant.sex = @"M";
    
    if([self.registrant.gIIDType length]==0)
        self.registrant.gIIDType = @"K";
    
    if([self.registrant.gIIDType isEqualToString:@"1"])
        self.registrant.gIIDType = @"K";
    
    if([self.registrant.gIIDType isEqualToString:@"2"])
        self.registrant.gIIDType = @"S";
    
    self.registrant.gIIDNumber = self.giidNumberText.text;
    
    return true;
}


#pragma mark - NGREGBottomPanelViewControllerDelegate methods

- (void)bottomPanelViewController:(NGREGBottomPanelViewController *)viewController didSelectMenuAtIndex:(NSInteger)index WithData:(id)menu{
    [self.navigationController popToRootViewControllerAnimated:NO];
}


#pragma mark - UITextFieldDelegate methods
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    //    if(self.view.frame.origin.y == 0)
    //        _originalViewFrame = self.view.frame;
    
    CGFloat ySize = 280.0f;
    
    if(textField == self.fullNameText){
        ySize = 40.0f;
    }
    
    if( textField == self.giidNumberText || textField == self.cityText)
        ySize = 150.0f;
    
    if(textField == self.emailText)
        ySize = 220.0f;
    
    if(textField == self.twitterText)
        ySize = 320.0f;
    
    
    [self.scrollView setContentOffset:CGPointMake(0,ySize) animated:YES];
    
    //    CGRect frame = CGRectMake(self.contentView.frame.origin.x, ySize, self.contentView.frame.size.width, self.contentView.frame.size.height);
    //
    //    CGFloat animationDuration = 0.3f;
    //    [UIView animateWithDuration:animationDuration animations:^{
    //
    //        self.scrollView.
    //    }];
    
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    if(textField.text.length > 0){
        [self.popTip hide];
    }
    
    if(textField == self.cityText){
        if (![_popoverViewController isPopoverVisible]) {
            [self cityClicked:nil];
        }
        [_cityPicker showSuggestionsFor:textField shouldChangeCharactersInRange:range replacementString:string];
    }
    
    
    if(textField == self.giidNumberText || textField == self.phoneText){
        NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
        
        NSString *expression = @"^[0-9]*$";
        
        NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:expression
                                                                               options:NSRegularExpressionCaseInsensitive
                                                                                 error:nil];
        NSUInteger numberOfMatches = [regex numberOfMatchesInString:newString
                                                            options:0
                                                              range:NSMakeRange(0, [newString length])];
        if (numberOfMatches == 0)
            return NO;
        
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        return (newLength > (textField == self.giidNumberText ? MAXLENGTH_GIID_NUMBER : MAXLENGTH_PHONE_NUMBER)) ? NO : YES;
    }
    
    if(textField == self.fullNameText){
        NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
        
        NSString *expression = @"^[a-zA-Z\\s]*$";
        
        NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:expression
                                                                               options:NSRegularExpressionCaseInsensitive
                                                                                 error:nil];
        NSUInteger numberOfMatches = [regex numberOfMatchesInString:newString
                                                            options:0
                                                              range:NSMakeRange(0, [newString length])];
        if (numberOfMatches == 0)
            return NO;
        
        
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        return !(newLength > MAXLENGTH_FULL_NAME);
    }
    
    if(textField == self.twitterText && [self.twitterText.text length] > 0){
        if(![[self.twitterText.text substringWithRange:NSMakeRange(0, 1)] isEqualToString:@"@"]){
            [self.twitterText setText:[NSString stringWithFormat:@"@%@", self.twitterText.text]];
        }
        if(self.twitterText.text.length > 1){
            
            NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
            
             newString = [newString substringWithRange:NSMakeRange(1, newString.length - 1)];
            
            NSString *expression = @"^[a-zA-Z0-9_\\s]*$";
            
            NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:expression
                                                                                   options:NSRegularExpressionCaseInsensitive
                                                                                     error:nil];
            NSUInteger numberOfMatches = [regex numberOfMatchesInString:newString
                                                                options:0
                                                                  range:NSMakeRange(0, [newString length])];
            if (numberOfMatches == 0)
                return NO;
        }
    }
    
    return YES;
}


#pragma mark - NGREGCityPickerViewControllerDelegate

- (NSArray *)cityPickerViewControllerCityArray:(NGREGCityPickerViewController *)controller {
    
    return cities;
}

- (void)cityPickerViewController:(NGREGCityPickerViewController *)controller didSelectCity:(NSDictionary*)city{
    
    
    self.cityText.text = [city[@"cityName"]capitalizedString];
    
    _selectedCity = city;
    
    [_popoverViewController dismissPopoverAnimated:YES];
    _popoverViewController.delegate = nil;
    _popoverViewController = nil;
}

#pragma mark - WYPopoverControllerDelegate methods


- (BOOL)popoverControllerShouldDismissPopover:(WYPopoverController *)popoverController {
    
    return YES;
}

- (void)popoverControllerDidDismissPopover:(WYPopoverController *)controller {
    
    if (controller == _popoverSubBrandViewController) {
        
        _popoverSubBrandViewController.delegate = nil;
        _popoverSubBrandViewController = nil;
    }
}

@end
