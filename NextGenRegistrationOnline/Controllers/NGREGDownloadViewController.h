//
//  NGREGDownloadViewController.h
//  NextGenRegistrationOnline
//
//  Created by Kevin Octavian on 2/10/14.
//  Copyright (c) 2014 Kevin R. Octavian. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NGREGRootViewController.h"

@interface NGREGDownloadViewController : NGREGRootViewController

@end
