//
//  NGREGRootViewController.m
//  NextGenRegistrationOnline
//
//  Created by Kevin Octavian on 1/17/14.
//  Copyright (c) 2014 Kevin R. Octavian. All rights reserved.
//

#import "NGREGRootViewController.h"

@interface NGREGRootViewController ()

@end

@implementation NGREGRootViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
