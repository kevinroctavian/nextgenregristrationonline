//
//  NGREGEngine.m
//  NextGenRegistrationOnline
//
//  Created by Kevin Octavian on 1/16/14.
//  Copyright (c) 2014 Kevin R. Octavian. All rights reserved.
//

#import "NGREGEngine.h"
#import "Endpoints.h"
#import "NGREGLoginResponse.h"
#import "NGREGCheckRegistrantResponse.h"
#import "NGREGResponse.h"
#import "NGREGAppDelegate.h"
#import "NGREGRootViewController.h"
#import "NGREGCity+Additions.h"
#import "NGREGSubBrand+Additions.h"
#import "NGREGCityResponse.h"
#import "UIDevice+Additions.h"
#import "NGREGSubmission+Additions.h"
#import "Constant.h"
#import "NGREGMopLoginResponse.h"
#import "NGREGSearchEntourageXMLParser.h"
#import "NGREGRegisterDeviceParser.h"
#import "BPXLUUIDHandler.h"
#import "NGREGRegistrant+Additions.h"
#import "NGREGError+Additions.h"
#include "OpenUDID.h"

static BOOL EngineInitialized = NO;

@implementation NGREGEngine


+ (id)sharedEngine {
    
    static dispatch_once_t predicate;
    static NGREGEngine *sharedObject = nil;
    
    dispatch_once(&predicate, ^{
        sharedObject = [[NGREGEngine alloc] init];
    });
    
    return sharedObject;
}

- (id)init {
    
    if ((self = [super init])) {
        if (!EngineInitialized) {
            [NGREGEngine initializeEngine];
        }
    }
    return self;
}

+ (void)initializeEngine {
    
    RKObjectManager* objectManager = [RKObjectManager managerWithBaseURL:[NSURL URLWithString:NGREG_API_BASE_URL]];
    
    
    // Enable Activity Indicator Spinner
    [AFNetworkActivityIndicatorManager sharedManager].enabled = YES;
    
    // Initialize managed object store
    NSManagedObjectModel *managedObjectModel = [NSManagedObjectModel mergedModelFromBundles:nil];
    RKManagedObjectStore *managedObjectStore = [[RKManagedObjectStore alloc] initWithManagedObjectModel:managedObjectModel];
    objectManager.managedObjectStore = managedObjectStore;
    
    // Update date format so that we can parse dates properly
    NSArray *dateFormats = @[@"MM/dd/yyyy", @"yyyy-MM-dd'T'HH:mm:ss'Z'", @"yyyy-MM-dd", @"yyyy-MM-dd hh:mm:ss"];
    RKCompoundValueTransformer *compoundValueTransformer = [RKValueTransformer defaultValueTransformer];
    for (NSString *dateFormat in dateFormats) {
        NSDateFormatter *dateFormatter = [NSDateFormatter new];
        dateFormatter.dateFormat = dateFormat;
        [compoundValueTransformer addValueTransformer:dateFormatter];
    }
    
    // Use JSON
    [objectManager setAcceptHeaderWithMIMEType:RKMIMETypeJSON];
    objectManager.requestSerializationMIMEType = RKMIMETypeFormURLEncoded;
    
    /**
     Complete Core Data stack initialization
     */
    [managedObjectStore createPersistentStoreCoordinator];
    NSString *storePath = [RKApplicationDataDirectory() stringByAppendingPathComponent:@"NGReg.sqlite"];
    NSString *seedPath = [[NSBundle mainBundle] pathForResource:@"RKSeedDatabase" ofType:@"sqlite"];
    NSError *error;
    NSPersistentStore *persistentStore = [managedObjectStore addSQLitePersistentStoreAtPath:storePath fromSeedDatabaseAtPath:seedPath withConfiguration:nil options:nil error:&error];
    NSAssert(persistentStore, @"Failed to add persistent store with error: %@", error);
    
    // Create the managed object contexts
    [managedObjectStore createManagedObjectContexts];
    
    // Configure a managed object cache to ensure we do not create duplicate objects
    managedObjectStore.managedObjectCache = [[RKInMemoryManagedObjectCache alloc] initWithManagedObjectContext:managedObjectStore.persistentStoreManagedObjectContext];
    
#ifdef DEBUG
    RKLogConfigureByName("RestKit/Network", RKLogLevelTrace);
    RKLogConfigureByName("RestKit/ObjectMapping", RKLogLevelTrace);
    RKLogConfigureByName("RestKit/CoreData", RKLogLevelTrace);
#endif
    
    // Setup our object mappings
    
    // login account
    RKObjectMapping *loginMapping = [RKObjectMapping mappingForClass:[NGREGLoginResponse class]];
    [loginMapping addAttributeMappingsFromDictionary:@{@"status"            : @"status",
                                                       @"msg"           : @"msg",
                                                       @"code"         : @"code",
                                                       @"adminname"         : @"adminname",
                                                       @"adminuser"         : @"adminuser",
                                                       @"no_version"         : @"messageVersion",
                                                       @"version"         : @"version",
                                                       }];
    RKResponseDescriptor *loginDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:loginMapping
                                                                                         method:RKRequestMethodPOST
                                                                                    pathPattern:NGREG_API_LOGIN_ACCOUNT
                                                                                        keyPath:nil
                                                                                    statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    [objectManager addResponseDescriptor:loginDescriptor];
    
    
    // brand list
    RKEntityMapping *brandMapping = [RKEntityMapping mappingForEntityForName:@"NGREGBrand" inManagedObjectStore:managedObjectStore];
    [brandMapping addAttributeMappingsFromDictionary:  @{@"brandid"                 : @"brandId",
                                                         @"brandname"               : @"brandName"}];
    brandMapping.identificationAttributes = @[@"brandId"];
    
    RKResponseDescriptor *brandResponseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:brandMapping
                                                                                                 method:RKRequestMethodPOST
                                                                                            pathPattern:NGREG_API_BRAND_LIST
                                                                                                keyPath:nil
                                                                                            statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    [objectManager addResponseDescriptor:brandResponseDescriptor];
    
    // brand theme
    RKEntityMapping *themeMapping = [RKEntityMapping mappingForEntityForName:@"NGREGTheme" inManagedObjectStore:managedObjectStore];
    [themeMapping addAttributeMappingsFromDictionary:  @{@"id"                 : @"brandId",
                                                         @"global"                 : @"globalImageUrl",
                                                         @"home"               : @"homeImageUrl",
                                                         @"text-color"                 : @"textColor",
                                                         @"text-size"               : @"textSize",
                                                         @"login-brand-name"               : @"loginBrandName",
                                                         @"formCellTextSize"               : @"formCellBg",
                                                         @"form-cell-background-disable"               : @"formCellBgDisable",
                                                         @"form-cell-text"               : @"formCellTextSize"}];
    themeMapping.identificationAttributes = @[@"brandId"];
    
    RKResponseDescriptor *themeResponseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:themeMapping
                                                                                                 method:RKRequestMethodPOST
                                                                                            pathPattern:NGREG_API_BRAND_THEME
                                                                                                keyPath:nil
                                                                                            statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    [objectManager addResponseDescriptor:themeResponseDescriptor];
    
    // profile
    RKEntityMapping *profileMapping = [RKEntityMapping mappingForEntityForName:@"NGREGProfile" inManagedObjectStore:managedObjectStore];
    [profileMapping addAttributeMappingsFromDictionary:  @{@"profile.brand"                 : @"brandId",
                                                           @"profile.customize_templates"                 : @"customizeTemplates",
                                                           @"profile.message_error"                 : @"messageError",
                                                           @"profile.last_name"               : @"lastName",
                                                           @"profile.email"                 : @"email",
                                                           @"profile.name"               : @"name",
                                                           @"profile.id"               : @"profileId",
                                                           @"profile.username"               : @"userName",
                                                           @"profile.customize_flow.menu"               : @"menu"}];
    profileMapping.identificationAttributes = @[@"userName"];
    
    RKResponseDescriptor *profileResponseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:profileMapping
                                                                                                   method:RKRequestMethodGET
                                                                                              pathPattern:NGREG_API_MY_PROFILE
                                                                                                  keyPath:nil
                                                                                              statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    [objectManager addResponseDescriptor:profileResponseDescriptor];
    
    // check registrant
    RKObjectMapping *registrantMapping = [RKObjectMapping mappingForClass:[NGREGCheckRegistrantResponse class]];
    [registrantMapping addAttributeMappingsFromDictionary:@{@"RegistrationID"            : @"registrationID",
                                                            @"FirstName"         : @"firstName",
                                                            @"LastName"           : @"lastName",
                                                            @"DateOfBirth"           : @"dateOfBirth",
                                                            @"Email"           : @"email",
                                                            @"GIIDType"           : @"gIIDType",
                                                            @"GIIDNumber"           : @"gIIDNumber",
                                                            @"Mobile"           : @"mobile",
                                                            @"Gender"           : @"sex",
                                                            @"CityID"           : @"city",
                                                            @"FullName"           : @"fullName",
                                                            @"SubBrand1_ID"           : @"brand1Id",
                                                            @"SpareText5"           : @"spareText5",
                                                            @"SpareText2"           : @"socialAccount"}];
    RKResponseDescriptor *registrantEMailDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:registrantMapping
                                                                                                   method:RKRequestMethodPOST
                                                                                              pathPattern:NGREG_API_REGISTRANT_CHECK_EMAIL
                                                                                                  keyPath:nil
                                                                                              statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    [objectManager addResponseDescriptor:registrantEMailDescriptor];
    
    RKResponseDescriptor *registrantGiidNumberDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:registrantMapping
                                                                                                        method:RKRequestMethodPOST
                                                                                                   pathPattern:NGREG_API_REGISTRANT_CHECK_GIIDNUMBER
                                                                                                       keyPath:nil
                                                                                                   statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    [objectManager addResponseDescriptor:registrantGiidNumberDescriptor];
    
    
    // auto logout
    //    RKObjectMapping *logoutMapping = [RKObjectMapping mappingForClass:[NGREGResponse class]];
    //    [logoutMapping addAttributeMappingsFromDictionary:@{@"status"            : @"status",
    //                                                       @"code"         : @"code",
    //                                                       @"message"           : @"message"}];
    //    RKResponseDescriptor *profileLogoutDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:logoutMapping
    //                                                                                         method:RKRequestMethodGET
    //                                                                                    pathPattern:NGREG_API_MY_PROFILE
    //                                                                                        keyPath:nil
    //                                                                                    statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    //    [objectManager addResponseDescriptor:profileLogoutDescriptor];
    
    
    // city
    RKEntityMapping *cityMapping = [RKEntityMapping mappingForEntityForName:@"NGREGCity" inManagedObjectStore:managedObjectStore];
    [cityMapping addAttributeMappingsFromDictionary:  @{@"id"                 : @"cityId",
                                                        @"cityidmop"          : @"cityIdMop",
                                                        @"provinceName"       : @"provinceName",
                                                        @"city"               : @"city",
                                                        @"provinceid"         : @"provinceId",
                                                        @"coor"               : @"coor",
                                                        @"n_status"           : @"nStatus"}];
    cityMapping.identificationAttributes = @[@"cityIdMop"];
    RKObjectMapping *cityResponseMapping = [RKObjectMapping mappingForClass:[NGREGCityResponse class]];
    [cityResponseMapping addAttributeMappingsFromDictionary:@{@"result"      : @"result"}];
    [cityResponseMapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"data" toKeyPath:@"data" withMapping:cityMapping]];
    RKResponseDescriptor *cityResponseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:cityResponseMapping
                                                                                                method:RKRequestMethodPOST
                                                                                           pathPattern:NGREG_API_REGISTRANT_GET_CITY
                                                                                               keyPath:nil
                                                                                           statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    
    [objectManager addResponseDescriptor:cityResponseDescriptor];
    
    
    // sub brands
    RKEntityMapping *subBrandsMapping = [RKEntityMapping mappingForEntityForName:@"NGREGSubBrand" inManagedObjectStore:managedObjectStore];
    [subBrandsMapping addAttributeMappingsFromDictionary:  @{@"theid"              : @"theId",
                                                             @"code"               : @"code",
                                                             @"preferenceid"       : @"preferenceId",
                                                             @"preference"         : @"preference",
                                                             @"brand_name"         : @"brandName",
                                                             @"subbrandname"       : @"subBrandName",
                                                             @"n_status"           : @"nStatus"}];
    subBrandsMapping.identificationAttributes = @[@"preferenceId"];
    RKObjectMapping *subbrandResponseMapping = [RKObjectMapping mappingForClass:[NGREGCityResponse class]];
    [subbrandResponseMapping addAttributeMappingsFromDictionary:@{@"result"      : @"result"}];
    [subbrandResponseMapping addPropertyMapping:[RKRelationshipMapping relationshipMappingFromKeyPath:@"data" toKeyPath:@"data" withMapping:subBrandsMapping]];
    RKResponseDescriptor *subbrandResponseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:subbrandResponseMapping
                                                                                                    method:RKRequestMethodPOST
                                                                                               pathPattern:NGREG_API_REGISTRANT_GET_SUB_BRANDS
                                                                                                   keyPath:nil
                                                                                               statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    
    [objectManager addResponseDescriptor:subbrandResponseDescriptor];
    
    // report
    RKEntityMapping *reportMapping = [RKEntityMapping mappingForEntityForName:@"NGREGReport" inManagedObjectStore:managedObjectStore];
    [reportMapping addAttributeMappingsFromDictionary:  @{@"entourage.1"              : @"approvedRegistrant",
                                                          @"entourage.2"               : @"rejectedRegistrant",
                                                          @"existing.2"       : @"existingRegistrant",
                                                          @"total"         : @"totalRegistrant",
                                                          @"menuid"         : @"menuId",
                                                          @"email"       : @"email",
                                                          @"submission.version"       : @"submissionVersion",
                                                          @"submission.total"       : @"submissionTotal",
                                                          @"submission.lastdate"       : @"submissionLastDate",
                                                          }];
    reportMapping.identificationAttributes = @[@"menuId"];
    RKResponseDescriptor *reportDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:reportMapping
                                                                                          method:RKRequestMethodPOST
                                                                                     pathPattern:NGREG_API_REGISTRANT_REPORT
                                                                                         keyPath:nil
                                                                                     statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    
    [objectManager addResponseDescriptor:reportDescriptor];
    
    // register registrant
    RKObjectMapping *registerRegistrantMapping = [RKObjectMapping mappingForClass:[NGREGResponse class]];
    [registerRegistrantMapping addAttributeMappingsFromDictionary:@{@"result"            : @"status"}];
    RKResponseDescriptor *registerRegistrantDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:registerRegistrantMapping
                                                                                                      method:RKRequestMethodPOST
                                                                                                 pathPattern:NGREG_API_REGISTRANT_REGISTER
                                                                                                     keyPath:nil
                                                                                                 statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    [objectManager addResponseDescriptor:registerRegistrantDescriptor];
    
    // tracking activity
    RKObjectMapping *trackingActivityMapping = [RKObjectMapping mappingForClass:[NGREGResponse class]];
    [trackingActivityMapping addAttributeMappingsFromDictionary:@{@"status"            : @"status",
                                                                  @"message"            : @"message"}];
    RKResponseDescriptor *trackingActivityDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:trackingActivityMapping
                                                                                                    method:RKRequestMethodPOST
                                                                                               pathPattern:NGREG_API_TRACK_ACTIVITY
                                                                                                   keyPath:nil
                                                                                               statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    [objectManager addResponseDescriptor:trackingActivityDescriptor];
    
    // tracking game
    RKObjectMapping *trackingGameMapping = [RKObjectMapping mappingForClass:[NGREGResponse class]];
    [trackingGameMapping addAttributeMappingsFromDictionary:@{@"gamesstatus.status"            : @"status",
                                                              @"gamesstatus.msg"            : @"message"}];
    RKResponseDescriptor *trackingGameDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:trackingGameMapping
                                                                                                method:RKRequestMethodPOST
                                                                                           pathPattern:NGREG_API_TRACK_GAMES
                                                                                               keyPath:nil
                                                                                           statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    [objectManager addResponseDescriptor:trackingGameDescriptor];
    
    // /service/mop/login
    RKObjectMapping *mopLoginMapping = [RKObjectMapping mappingForClass:[NGREGMopLoginResponse class]];
    [mopLoginMapping addAttributeMappingsFromDictionary:@{@"Result"            : @"status",
                                                          @"SessionID"         : @"sessionID",
                                                          @"Message"           : @"message"}];
    RKResponseDescriptor *mopLoginDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:mopLoginMapping
                                                                                            method:RKRequestMethodPOST
                                                                                       pathPattern:BEAT_API_MOP_LOGIN
                                                                                           keyPath:nil
                                                                                       statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    [objectManager addResponseDescriptor:mopLoginDescriptor];
    
    
    RKObjectMapping *crashMapping = [RKObjectMapping mappingForClass:[NGREGLoginResponse class]];
    [crashMapping addAttributeMappingsFromDictionary:@{@"status"            : @"status",
                                                       @"msg"           : @"msg"
                                                       }];
    RKResponseDescriptor *crashDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:crashMapping
                                                                                         method:RKRequestMethodPOST
                                                                                    pathPattern:NGREG_API_CRASH_REPORT
                                                                                        keyPath:nil
                                                                                    statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    [objectManager addResponseDescriptor:crashDescriptor];
    
    
    EngineInitialized = YES;
}

#pragma mark - Image downloader

- (NSString *)imageAtURLString:(NSString *)urlString withCompletionHandler:(NGREGRequestCompletionHandler)block {
    
    NSURL *url = [NSURL URLWithString:urlString];
    
    dispatch_queue_t downloadQueue = dispatch_queue_create("com.kana.nextgenreg.imagedownloader", nil);
    dispatch_async(downloadQueue, ^{
        
        NSData *responseData = [NSData dataWithContentsOfURL:url];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            if (responseData) {
                block(responseData, nil);
            } else {
                
            }
        });
    });
    
    return nil;
}

#pragma mark - API Methods

- (NSString *)requestLoginWithUsername:(NSString*)username andPassword:(NSString*)pwd andBrandId:(NSString*)brandId andWithCompletionHandler:(NGREGRequestCompletionHandler)block {
    
    NSString *deviceId = [[BPXLUUIDHandler UUID]stringByReplacingOccurrencesOfString:@"-" withString:@""];
    
//    NSString *serialNumber = [[UIDevice currentDevice] serialNumber];
//
////    NSString* serialNumber = [OpenUDID value]; // 1244b6f0a0656262d8018b8e4aa39f4987e179e9
    NSString *deviceName = [[UIDevice currentDevice] name];
    NSString* serialNumber = deviceId;
//
    
    
//    NSString *serialNumber = @"DMPK4CZAF18W";
//    NSString *deviceId = @"98EF7DFC952546EA8B3AC70B5A17E10C";
//    NSString *deviceName = @"Fantomas";
    
    
    NSDictionary *parameter = @{@"username":username,
                                @"password":pwd,
                                @"brand":brandId,
                                @"serialnumber":deviceId,
                                @"deviceid":serialNumber,
                                @"devicedescription ": deviceName,
                                };
    
    NSLog(@"Parameter login : %@", parameter);
    
    
    [[RKObjectManager sharedManager] postObject:nil path:NGREG_API_LOGIN_ACCOUNT parameters:parameter success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
        
        if (block) {
            NGREGLoginResponse* resposnse = [mappingResult firstObject];
            
            NSString* regDevice = [[NSUserDefaults standardUserDefaults] valueForKey:kNGREGRegisterDevice];
            //            if([regDevice isEqualToString:@"1"]){
            //                block(resposnse, nil);
            //                return;
            //            }
            
            if(resposnse != nil){
                
                if([resposnse.status intValue] == 1){
                    
                    NSString* name = @"";
                    NSString* admin = @"";
                    
                    NSString *nameChar = resposnse.adminuser;
                    NSString *passwordChar = resposnse.adminname;
                    
                    
                    if(nameChar.length == 0 && passwordChar.length == 0){
                        block(resposnse, nil);
                        return;
                    }
                    
                    for (int i=0;i<nameChar.length;i++) {
                        if(i%2 == 0){
                            name = [name stringByAppendingString:[nameChar substringWithRange:NSMakeRange(i, 1)]];
                        }
                    }
                    
                    for (int i=0;i<passwordChar.length;i++) {
                        if(i%2 == 0){
                            admin = [admin stringByAppendingString:[passwordChar substringWithRange:NSMakeRange(i, 1)]];
                        }
                    }
                    
                    
                    __block NGREGRequestCompletionHandler _block = block;
                    [self registerDeviceWithUsername:name andPassword:admin withCompletionHandler:^(bool success) {
                        if(success){
                            [[NSUserDefaults standardUserDefaults] setValue:@"1" forKey:kNGREGRegisterDevice];
//                            _block(resposnse, nil);
                            [self requestMopLoginWithUsername:username andPassword:pwd withCompletionHandler:^(id result, NSError *error) {
                                NGREGMopLoginResponse* res = result;
                                if([res.status intValue] == 1){
                                    _block(resposnse, nil);
                                }else{
                                    NGREGLoginResponse* resFail = [[NGREGLoginResponse alloc]init];
                                    _block(resFail, nil);
                                    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:res.message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                                    [alertView show];
                                    return;
                                }
                            }];
                            return;
                        }else{
                            NGREGLoginResponse* resFail = [[NGREGLoginResponse alloc]init];
                            _block(resFail, nil);
                            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"Your Device Not Registered" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                            [alertView show];
                            return;
                            
                        }
                    }];
                    
                    
                    [[NSUserDefaults standardUserDefaults] setValue:name forKey:kNGREGUsernameMOP];
                    [[NSUserDefaults standardUserDefaults] setValue:admin forKey:kNGREGPasswordMOP];
                    
                }else{
                    [self requestMopLoginWithUsername:username andPassword:pwd withCompletionHandler:^(id result, NSError *error) {
                        NGREGMopLoginResponse* res = result;
                        if([res.status intValue] == 1){
                            block(resposnse, nil);
                        }else{
                            NGREGLoginResponse* resFail = [[NGREGLoginResponse alloc]init];
                            block(resFail, nil);
                            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:res.message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                            [alertView show];
                            return;
                        }
                    }];
                }
                
            }else{
                [self requestMopLoginWithUsername:username andPassword:pwd withCompletionHandler:^(id result, NSError *error) {
                    NGREGMopLoginResponse* res = result;
                    if([res.status intValue] == 1){
                        block(resposnse, nil);
                    }else{
                        NGREGLoginResponse* resFail = [[NGREGLoginResponse alloc]init];
                        block(resFail, nil);
                        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:res.message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                        [alertView show];
                        return;
                    }
                }];
            }
        }
        
    } failure:^(RKObjectRequestOperation *operation, NSError *error) {
        
        if (block) {
            block(nil, error);
        }
    }];
    
    return nil;
}

- (NSString *)requestBrandListWithCompletionHandler:(NGREGRequestCompletionHandler)block {
    
    NSString *deviceId = [[BPXLUUIDHandler UUID]stringByReplacingOccurrencesOfString:@"-" withString:@""];
    NSString *deviceName = [[UIDevice currentDevice] name];
    NSString* serialNumber = deviceId;
    
    NSDictionary *parameter = @{};
    
    [[RKObjectManager sharedManager] postObject:nil path:NGREG_API_BRAND_LIST parameters:parameter success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
        
        if (block) {
            block([mappingResult array], nil);
        }
        
    } failure:^(RKObjectRequestOperation *operation, NSError *error) {
        
        if (block) {
            block(nil, error);
        }
    }];
    
    return nil;
}

- (NSString *)requestBrandThemeWithBrandId:(NSString*)brandId andCompletionHandler:(NGREGRequestCompletionHandler)block {
    
    NSDictionary *parameter = @{@"brandid":brandId};
    
    [[RKObjectManager sharedManager] postObject:nil path:NGREG_API_BRAND_THEME parameters:parameter success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
        
        if (block) {
            block([mappingResult firstObject], nil);
        }
        
    } failure:^(RKObjectRequestOperation *operation, NSError *error) {
        
        if (block) {
            block(nil, error);
        }
    }];
    
    return nil;
}

- (NSString *)requestMyProfileWithCompletionHandler:(NGREGRequestCompletionHandler)block {
    
    NSDictionary *parameter = @{};
    
    [[RKObjectManager sharedManager] getObject:nil path:NGREG_API_MY_PROFILE parameters:parameter success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
        
        id response = [mappingResult firstObject];
        if([response isKindOfClass:[NGREGResponse class]] || response == nil){
            [[NSUserDefaults standardUserDefaults] setValue:@"" forKey:kNGREGEmailNextGen];
            NGREGAppDelegate* appDelegate = (NGREGAppDelegate *)[[UIApplication sharedApplication] delegate];
            [appDelegate setRootViewControllerToAuthenticationViewController];
            return;
        }
        
        if (block) {
            block([mappingResult firstObject], nil);
        }
        
    } failure:^(RKObjectRequestOperation *operation, NSError *error) {
        
        if (block) {
            block(nil, error);
        }
    }];
    
    return nil;
}

- (NSString *)requestDataRegistrantWithEMail:(NSString*)email andCompletionHandler:(NGREGRequestCompletionHandler)block {
    
    NSDictionary *parameter = @{@"email":email};
    
    [[RKObjectManager sharedManager] postObject:nil path:NGREG_API_REGISTRANT_CHECK_EMAIL parameters:parameter success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
        
        id result = [mappingResult firstObject];
        
        if(result == nil){
            NGREGCheckRegistrantResponse* checkRegistrant = [[NGREGCheckRegistrantResponse alloc]init];
            checkRegistrant.email = email;
            result = checkRegistrant;
        }
        
        if (block) {
            block(result, nil);
        }
        
    } failure:^(RKObjectRequestOperation *operation, NSError *error) {
        
        NGREGCheckRegistrantResponse* checkRegistrant = [[NGREGCheckRegistrantResponse alloc]init];
        checkRegistrant.email = email;
        
        if (block) {
            block(checkRegistrant, error);
        }
    }];
    
    return nil;
}

- (NSString *)requestDataRegistrantWithGIIDNumber:(NSString*)giidnumber andCompletionHandler:(NGREGRequestCompletionHandler)block {
    
    NSDictionary *parameter = @{@"giidnumber":giidnumber};
    
    [[RKObjectManager sharedManager] postObject:nil path:NGREG_API_REGISTRANT_CHECK_GIIDNUMBER parameters:parameter success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
        
        id result = [mappingResult firstObject];
        
        if(result == nil){
            NGREGCheckRegistrantResponse* checkRegistrant = [[NGREGCheckRegistrantResponse alloc]init];
            checkRegistrant.gIIDNumber = giidnumber;
            result = checkRegistrant;
        }
        
        if (block) {
            block(result, nil);
        }
        
    } failure:^(RKObjectRequestOperation *operation, NSError *error) {
        
        NGREGCheckRegistrantResponse* checkRegistrant = [[NGREGCheckRegistrantResponse alloc]init];
        checkRegistrant.gIIDNumber = giidnumber;
        
        if (block) {
            block(checkRegistrant, error);
        }
    }];
    
    return nil;
}

- (NSString *)requestTrackActivityWithMenu:(NSString*)menu andPage:(NSString*)page andCompletionHandler:(NGREGRequestCompletionHandler)block {
    
    NSDate* sourceDate = [NSDate date];
    
    
    
    NSTimeZone* sourceTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
    NSTimeZone* destinationTimeZone = [NSTimeZone systemTimeZone];
    
    NSInteger sourceGMTOffset = [sourceTimeZone secondsFromGMTForDate:sourceDate];
    NSInteger destinationGMTOffset = [destinationTimeZone secondsFromGMTForDate:sourceDate];
    NSTimeInterval interval = destinationGMTOffset - sourceGMTOffset;
    
    NSDate* now = [[NSDate alloc] initWithTimeInterval:interval sinceDate:sourceDate];
    
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc]init];
    
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    NSString* stringFormat = [dateFormatter stringFromDate:sourceDate];
    
    NSDictionary *parameter = @{@"activity":[NSString stringWithFormat:@"%@_%@",page, [menu stringByReplacingOccurrencesOfString:@" " withString:@""]], @"datetimes":stringFormat};
    
    [[RKObjectManager sharedManager] postObject:nil path:NGREG_API_TRACK_ACTIVITY parameters:parameter success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
        
        if (block) {
            //            block([mappingResult firstObject], nil);
        }
        
    } failure:^(RKObjectRequestOperation *operation, NSError *error) {
        
        NGREGAppDelegate* appDelegate = (NGREGAppDelegate *)[[UIApplication sharedApplication] delegate];
        [NGREGSync syncWithData:parameter email:[[NSUserDefaults standardUserDefaults] valueForKey:kNGREGEmailNextGen] menu:menu brandId:[[NSUserDefaults standardUserDefaults] valueForKey:kNGREGBrandIDNextGen] type:kNGREGTrackingActivity andUrl:NGREG_API_TRACK_ACTIVITY inManagedObjectContext:[appDelegate managedObjectContext]];
        
        if (block) {
            block(nil, error);
        }
    }];
    
    return nil;
}

- (NSString *)requestTrackGameWithData:(NSDictionary*)data andCompletionHandler:(NGREGRequestCompletionHandler)block {
    
    NSDictionary *parameter = @{@"gamesid":[data valueForKey:@"gamesId"],
                                @"userid":[data valueForKey:@"userId"],
                                @"registrantmail":[data valueForKey:@"registrantEmail"],
                                @"usermail":[data valueForKey:@"userEmail"],
                                @"playtime":[data valueForKey:@"startTime"],
                                @"win":[data valueForKey:@"win"],
                                @"promotionalsite":[data valueForKey:@"promotionalsite"]};
    
    [[RKObjectManager sharedManager] postObject:nil path:NGREG_API_TRACK_GAMES parameters:parameter success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
        
        if (block) {
            //            block([mappingResult firstObject], nil);
        }
        
    } failure:^(RKObjectRequestOperation *operation, NSError *error) {
        
        NGREGAppDelegate* appDelegate = (NGREGAppDelegate *)[[UIApplication sharedApplication] delegate];
        [NGREGSync syncWithData:parameter email:[[NSUserDefaults standardUserDefaults] valueForKey:kNGREGEmailNextGen] menu:[data valueForKey:@"menuName"] brandId:[[NSUserDefaults standardUserDefaults] valueForKey:kNGREGBrandIDNextGen] type:kNGREGGame andUrl:NGREG_API_TRACK_GAMES inManagedObjectContext:[appDelegate managedObjectContext]];
        
        if (block) {
            block(nil, error);
        }
    }];
    
    return nil;
}

- (NSString *)requestListCityWithCompletionHandler:(NGREGRequestCompletionHandler)block {
    
    NSDictionary *parameter = @{};
    
    [[RKObjectManager sharedManager] postObject:nil path:NGREG_API_REGISTRANT_GET_CITY parameters:parameter success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
        
        if (block) {
            block([mappingResult firstObject], nil);
        }
        
    } failure:^(RKObjectRequestOperation *operation, NSError *error) {
        
        if (block) {
            block(nil, error);
        }
    }];
    
    return nil;
}

- (NSString *)requestListSubBrandsWithCompletionHandler:(NGREGRequestCompletionHandler)block {
    
    NSDictionary *parameter = @{};
    
    [[RKObjectManager sharedManager] postObject:nil path:NGREG_API_REGISTRANT_GET_SUB_BRANDS parameters:parameter success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
        
        if (block) {
            block([mappingResult firstObject], nil);
        }
        
    } failure:^(RKObjectRequestOperation *operation, NSError *error) {
        
        if (block) {
            block(nil, error);
        }
    }];
    
    return nil;
}

- (NSString*)requestRegisterRegistrant:(NGREGCheckRegistrantResponse *)entourage atEvent:(NSString*)event withCompletionHandler:(NGREGRequestCompletionHandler)block {
    
    NGREGAppDelegate* appDelegate = (NGREGAppDelegate *)[[UIApplication sharedApplication] delegate];
    
    NSDictionary *parameter = @{@"birthday": entourage.dateOfBirth,
                                @"giidnumber": entourage.gIIDNumber,
                                @"Brand1_ID": entourage.brand1Id,
                                @"phone_number": entourage.mobile,
                                @"companymobile": entourage.mobile,
                                @"giidtype": entourage.gIIDType,
                                @"Brand1SUB_ID": entourage.brand1Id,
                                @"name": entourage.fullName,
                                @"email": entourage.email,
                                @"sex": entourage.sex,
                                @"socialaccount": entourage.socialAccount,
                                @"city": entourage.city,
                                @"menuname":event};
    
    
    NSLog(@"Parameter : %@", parameter);
    
    NSArray* submissions = [NGREGSubmission submissionWithEmail:[[NSUserDefaults standardUserDefaults] valueForKey:kNGREGEmailNextGen] brandId:[[NSUserDefaults standardUserDefaults] valueForKey:kNGREGBrandIDNextGen] eventId:event inManagedObjectContext:[appDelegate managedObjectContext]];
    NGREGSubmission *submission = [submissions lastObject];
    
    if(submission == nil){
        submission = [NSEntityDescription insertNewObjectForEntityForName:@"NGREGSubmission" inManagedObjectContext:[appDelegate managedObjectContext]];
        submission.email = [[NSUserDefaults standardUserDefaults] valueForKey:kNGREGEmailNextGen] ;
        submission.submissionTotal = [NSNumber numberWithInt:0];
        submission.submissionVersion = [NSNumber numberWithInt:0];
        submission.eventId = event;
        submission.brandId = [[NSUserDefaults standardUserDefaults] valueForKey:kNGREGBrandIDNextGen];
        [submission.managedObjectContext save:nil];
    }
    
    submission.submissionTotal = [NSNumber numberWithInt:([submission.submissionTotal intValue] + 1)];
    [submission.managedObjectContext save:nil];
    
    //    int submissionTotal = [[[NSUserDefaults standardUserDefaults] valueForKey:kNGREGSubmissionTotalNextGen] intValue];
    //    [[NSUserDefaults standardUserDefaults] setValue:[NSString stringWithFormat:@"%d", (++submissionTotal)] forKey:kNGREGSubmissionTotalNextGen];
    
    
    if(entourage.signDSTImage != nil || entourage.signImage != nil){
        NSMutableURLRequest *request = [[RKObjectManager sharedManager] multipartFormRequestWithObject:nil method:RKRequestMethodPOST path:NGREG_API_REGISTRANT_REGISTER parameters:parameter constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
            
            if(entourage.signImage != nil)
                [formData appendPartWithFileData:UIImagePNGRepresentation(entourage.signImage)
                                            name:@"signature"
                                        fileName:@"signature.png"
                                        mimeType:@"image/png"];
            
            if(entourage.signDSTImage != nil)
                [formData appendPartWithFileData:UIImagePNGRepresentation(entourage.signDSTImage)
                                            name:@"signatureba"
                                        fileName:@"signatureba.png"
                                        mimeType:@"image/png"];
            
        }];
        
        RKObjectRequestOperation *operation = [[RKObjectManager sharedManager] objectRequestOperationWithRequest:request success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
            
//            [NGREGRegistrant registrantWithRegistrant:entourage inManagedObjectContext:[appDelegate managedObjectContext]];
            
            if (block) {
                //            block([mappingResult firstObject], nil);
            }
            
        } failure:^(RKObjectRequestOperation *operation, NSError *error) {
            
            
            [NGREGSync syncWithData:parameter email:[[NSUserDefaults standardUserDefaults] valueForKey:kNGREGEmailNextGen] menu:event brandId:[[NSUserDefaults standardUserDefaults] valueForKey:kNGREGBrandIDNextGen] type:kNGREGRegistration andUrl:NGREG_API_REGISTRANT_REGISTER inManagedObjectContext:[appDelegate managedObjectContext]];
            
            if (block) {
                block(nil, error);
            }
        }];
        
        [[RKObjectManager sharedManager] enqueueObjectRequestOperation:operation];
        
    }
    else {
        
        [[RKObjectManager sharedManager] postObject:nil path:NGREG_API_REGISTRANT_REGISTER parameters:parameter success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
            
//             [NGREGRegistrant registrantWithRegistrant:entourage inManagedObjectContext:[appDelegate managedObjectContext]];
            
            if (block) {
                //                block([mappingResult firstObject], nil);
            }
            
        } failure:^(RKObjectRequestOperation *operation, NSError *error) {
            
            NGREGAppDelegate* appDelegate = (NGREGAppDelegate *)[[UIApplication sharedApplication] delegate];
            [NGREGSync syncWithData:parameter email:[[NSUserDefaults standardUserDefaults] valueForKey:kNGREGEmailNextGen] menu:event brandId:[[NSUserDefaults standardUserDefaults] valueForKey:kNGREGBrandIDNextGen] type:kNGREGRegistration andUrl:NGREG_API_REGISTRANT_REGISTER inManagedObjectContext:[appDelegate managedObjectContext]];
            
            if (block) {
                block(nil, error);
            }
        }];
    }
    
    return nil;
    
}

- (NSString *)requestSync:(NGREGSync *)sync withCompletionHandler:(NGREGRequestCompletionHandler)block{
    
    NSDictionary *parameter = sync.data;
    
    [[RKObjectManager sharedManager] postObject:nil path:sync.url parameters:parameter success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
        
        //        id response = [mappingResult firstObject];
        //        if([response isKindOfClass:[NGREGResponse class]] || response == nil){
        //            [[NSUserDefaults standardUserDefaults] setValue:@"" forKey:kNGREGEmailNextGen];
        //            NGREGAppDelegate* appDelegate = (NGREGAppDelegate *)[[UIApplication sharedApplication] delegate];
        //            [appDelegate setRootViewControllerToAuthenticationViewController];
        //            block(nil, [[NSError alloc] init]);
        //            return;
        //        }
        
        NSManagedObjectContext *context = sync.managedObjectContext;
        [context deleteObject:sync];
        
        NSError *error = nil;
        [context save:&error];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:kNGREGSync object:nil userInfo:nil];
        
        block(nil, nil);
        
    } failure:^(RKObjectRequestOperation *operation, NSError *error) {
        
        [[NSNotificationCenter defaultCenter] postNotificationName:kNGREGSync object:nil userInfo:nil];
        
        block(nil, error);
    }];
    
    return nil;
}

- (NSString *)requestLogoutWithCompletionHandler:(NGREGRequestCompletionHandler)block{
    
    NSDictionary *parameter = @{};
    
    [[RKObjectManager sharedManager] getObject:nil path:NGREG_API_LOGOUT parameters:parameter success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
        
        block(nil, nil);
        
    } failure:^(RKObjectRequestOperation *operation, NSError *error) {
        
        block(nil, nil);
    }];
    
    return nil;
}

- (NSString *)requestReportWithMenuId:(NSString*)menuId andCompletionHandler:(NGREGRequestCompletionHandler)block{
    
    NSDictionary *parameter = @{@"menuid":menuId};
    
    [[RKObjectManager sharedManager] postObject:nil path:NGREG_API_REGISTRANT_REPORT parameters:parameter success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
        
        if (block) {
            block([mappingResult firstObject], nil);
        }
        
    } failure:^(RKObjectRequestOperation *operation, NSError *error) {
        
        block(nil, error);
    }];
    
    return nil;
}

- (NSString *)requestCrashReportWithBrand:(NSString*)brand andUserId:(NSString*)userid andTime:(NSDate*)sourceDate andReason:(NSString*)reason andDetail:(NSString*)detail andCompletionHandler:(NGREGRequestCompletionHandler)block{
    
    
    NGREGAppDelegate* appDelegate = (NGREGAppDelegate *)[[UIApplication sharedApplication] delegate];
    
    NSTimeZone* sourceTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
    NSTimeZone* destinationTimeZone = [NSTimeZone systemTimeZone];
    
    NSInteger sourceGMTOffset = [sourceTimeZone secondsFromGMTForDate:sourceDate];
    NSInteger destinationGMTOffset = [destinationTimeZone secondsFromGMTForDate:sourceDate];
    NSTimeInterval interval = destinationGMTOffset - sourceGMTOffset;
    
    NSDate* time = [[NSDate alloc] initWithTimeInterval:interval sinceDate:sourceDate];
    
    if([NGREGError errorWithDate:time inManagedObjectContext:[appDelegate managedObjectContext]].count > 0){
        return @"";
    }
    
    
//    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
//    [dateFormat setDateFormat:@"dd MMM yyyy hh:mm:ss"];
//    
    if(brand == nil)
        brand = @"";
    
    if(userid == nil)
        userid = @"";
    
    NSDictionary *parameter = @{@"brand":brand,
                                @"userid":userid,
                                @"time_error":time,
                                @"reason":reason,
                                @"token":@"nextgen",
                                @"detail":detail};
    
    
    NSLog(@"Parameter Crash Report : %@", parameter);
    
    [[RKObjectManager sharedManager] postObject:nil path:NGREG_API_CRASH_REPORT parameters:parameter success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
        
        
        // save ke db
        [NGREGError errorWithBrand:brand  userId:userid timeError:time reason:reason andDetail:detail inManagedObjectContext:[appDelegate managedObjectContext]];
        
        if (block) {
            block([mappingResult firstObject], nil);
        }
        
    } failure:^(RKObjectRequestOperation *operation, NSError *error) {
        
        [NGREGSync syncWithData:parameter email:[[NSUserDefaults standardUserDefaults] valueForKey:kNGREGEmailNextGen] menu:@"crash report" brandId:[[NSUserDefaults standardUserDefaults] valueForKey:kNGREGBrandIDNextGen] type:kNGREGTrackingActivity andUrl:NGREG_API_TRACK_ACTIVITY inManagedObjectContext:[appDelegate managedObjectContext]];
        
        block(nil, error);
    }];
    
    return nil;
}

#pragma mark - MOP

- (void)requestMopSecretTokenWithUsername:(NSString*)username andPassword:(NSString*)pwd withCompletionHandler:(NGREGRequestCompletionHandler)block {


//    NSString *serialNumber = [[UIDevice currentDevice]serialNumber] ;
    NSString *deviceId = [[BPXLUUIDHandler UUID]stringByReplacingOccurrencesOfString:@"-" withString:@""];
        NSString* serialNumber = deviceId;
    NSString *deviceName = [[UIDevice currentDevice] name];

    
//    NSString *serialNumber = @"DMPK4CZAF18W";
//    NSString *deviceId = @"98EF7DFC952546EA8B3AC70B5A17E10C";
//    NSString *deviceName = @"Fantomas";
    
    NSString *urlString = [NSString stringWithFormat:@"%@%@", NGREG_API_BASE_URL, BEAT_API_MOP_SECRET_TOKEN];
    NSString *post = [NSString stringWithFormat:@"username=%@&password=%@&deviceid=%@&devicedesc=%@&serialnumber=%@", username, pwd, serialNumber, deviceName, deviceId];
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%d", [postData length]];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:postData];
    [request setTimeoutInterval:30.0f];
    
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    [NSURLConnection sendAsynchronousRequest:request queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
        
        if (error == nil) {
            block(data, nil);
        } else {
            block(nil, error);
        }
    }];
}

- (void)registerDeviceWithUsername:(NSString *)username andPassword:(NSString*)pwd withCompletionHandler:(successBlock)block {
    
    if ([username length] == 0 || [pwd length] == 0) {
        return;
    }
    
//    username = @"inong@kana.co.id";
//    pwd = @"beatbeat";
    
    [self requestMopSecretTokenWithUsername:username andPassword:pwd withCompletionHandler:^(id result, NSError *error) {
        
        error = nil;
        NSDictionary *responseDict = [NSJSONSerialization JSONObjectWithData:result options:NSJSONReadingMutableContainers error:&error];
        
        NSLog(@"Response Secret Token : %@", responseDict);
        
        if(responseDict == nil)
            block(NO);
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            NSString *token = responseDict[@"usethis"];
            
            NSString *deviceId = [[BPXLUUIDHandler UUID]stringByReplacingOccurrencesOfString:@"-" withString:@""];
    NSString* serialNumber = deviceId;
//            NSString *serialNumber = [[UIDevice currentDevice] serialNumber];
            NSString *deviceName = [[UIDevice currentDevice] name];
            
            [[NSUserDefaults standardUserDefaults] setValue:token forKey:kBEATSecurityToken];
            
            NSString *urlString = BEAT_AWS_MOP_ADMIN;
            
            
//            NSString *serialNumber = @"DMPK4CZAF18W";
//            NSString *deviceId = @"98EF7DFC952546EA8B3AC70B5A17E10C";
//            NSString *deviceName = @"Fantomas";
            
            NSString *post = [NSString stringWithFormat:kBEATRegisterDeviceFormatString, token, username, pwd, serialNumber, deviceName, deviceId];
            NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
            NSString *postLength = [NSString stringWithFormat:@"%d", [postData length]];
            
            NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]];
            [request setValue:@"text/xml; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
            [request setValue:@"UserRegisterDevice" forHTTPHeaderField:@"soapaction"];
            [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
            [request setHTTPMethod:@"POST"];
            [request setHTTPBody:postData];
            [request setTimeoutInterval:10.0f];
            
            NSOperationQueue *queue = [[NSOperationQueue alloc] init];
            [NSURLConnection sendAsynchronousRequest:request queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    NSString *stringResponse = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                    NSLog(@"registerDevice xml response: %@", stringResponse);
                    NGREGRegisterDeviceParser* parser = [[NGREGRegisterDeviceParser alloc] init];
                    
                    [parser parseXMLToResultFromRegisterDeviceData:data withCompleteion:^(bool sucess) {
                        block(sucess);
                    }];
                });
                
            }];
        });
    }];
}

- (void)checkEntourageByGIIDNumber:(NSString*)number andCompletionHandler:(entourageBlock)block {
    
    //    NSString* username = [[NSUserDefaults standardUserDefaults] valueForKey:kNGREGUsernameMOP];
    //    NSString* password = [[NSUserDefaults standardUserDefaults] valueForKey:kNGREGPasswordMOP];
    
    NSString* username = [[NSUserDefaults standardUserDefaults] valueForKey:kNGREGEmailNextGen];
    NSString* password = [[NSUserDefaults standardUserDefaults] valueForKey:kNGREGPasswordNextGen];
    
    //    username = @"isngadi@gmail.com";
    //    password = @"beatbeat";
    
    
    [self requestMopLoginWithUsername:username andPassword:password withCompletionHandler:^(id result, NSError *error) {
        
        if(error != nil){
            NGREGCheckRegistrantResponse* checkRegistrant = [[NGREGCheckRegistrantResponse alloc]init];
            checkRegistrant.gIIDNumber = number;
            result = checkRegistrant;
            block(checkRegistrant);
            return;
        }
        
        NGREGMopLoginResponse* res = result;
        
        NSString *securityToken = [[NSUserDefaults standardUserDefaults] valueForKey:kBEATSecurityToken];
        
        
        NSString* xml = [NSString stringWithFormat:kBEATSearchEntourageByGIIDNumberFormatString, number];
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@", BEAT_AWS_MOP_ADMIN, BEAT_AWS_MOP_SEARCH_ENTOURAGE_BY_GIID];
        
        NSString *post = [NSString stringWithFormat:@"SecurityToken=%@&SessionID=%@&xml=%@", securityToken, res.sessionID, xml];
        
        NSLog(@"URL Search by giid : %@", urlString);
        NSLog(@"Post Search by giid : %@", post);
        NSLog(@"Params Search by giid : %@", xml);
        
        NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
        NSString *postLength = [NSString stringWithFormat:@"%d", [postData length]];
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:10.0f];
        //        [request setValue:@"text/xml; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
        [request setValue:securityToken forHTTPHeaderField:@"SecurityToken"];
        [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
        [request setHTTPMethod:@"POST"];
        [request setHTTPBody:postData];
        [request setTimeoutInterval:10.0f];
        
        NSOperationQueue *queue = [[NSOperationQueue alloc] init];
        [NSURLConnection sendAsynchronousRequest:request queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
            
            NSString *stringResponse = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
            NSLog(@"search entourage xml response: %@", stringResponse);
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                if(stringResponse.length == 0){
                    NGREGCheckRegistrantResponse * entourage = [[NGREGCheckRegistrantResponse alloc] init];
                    entourage.gIIDNumber = number;
                    block(entourage);
                }else{
                    
                    NGREGSearchEntourageXMLParser* parser = [[NGREGSearchEntourageXMLParser alloc]init];
                    [parser parseXMLToGetEntourageFromData:data withCompleteion:^(NGREGCheckRegistrantResponse * entourage) {
                        if(entourage.fullName.length > 0){
                            block(entourage);
                        }else{
                            entourage.gIIDNumber = number;
                            block(entourage);
                        }
                        
                    }];
                }
                
            });
        }];
        
        
    }];
}

- (void)checkEntourageByEmail:(NSString*)email andCompletionHandler:(entourageBlock)block {
    
    //    NSString* username = [[NSUserDefaults standardUserDefaults] valueForKey:kNGREGUsernameMOP];
    //    NSString* password = [[NSUserDefaults standardUserDefaults] valueForKey:kNGREGPasswordMOP];
    //
    //    username = @"isngadi@gmail.com";
    //    password = @"beatbeat";
    
    NSString* username = [[NSUserDefaults standardUserDefaults] valueForKey:kNGREGEmailNextGen];
    NSString* password = [[NSUserDefaults standardUserDefaults] valueForKey:kNGREGPasswordNextGen];
    
    [self requestMopLoginWithUsername:username andPassword:password withCompletionHandler:^(id result, NSError *error) {
        
        if(error != nil){
            NGREGCheckRegistrantResponse* checkRegistrant = [[NGREGCheckRegistrantResponse alloc]init];
            checkRegistrant.email = email;
            result = checkRegistrant;
            block(checkRegistrant);
            return;
        }
        
        NGREGMopLoginResponse* res = result;
        
        NSString *securityToken = [[NSUserDefaults standardUserDefaults] valueForKey:kBEATSecurityToken];
        
        
        NSString* xml = [NSString stringWithFormat:kBEATSearchEntourageByEmailFormatString, email];
        
        NSString *urlString = [NSString stringWithFormat:@"%@%@", BEAT_AWS_MOP_ADMIN, BEAT_AWS_MOP_SEARCH_ENTOURAGE_BY_EMAIL];
        
        NSString *post = [NSString stringWithFormat:@"SecurityToken=%@&SessionID=%@&xml=%@", securityToken, res.sessionID, xml];
        
        
        NSLog(@"URL Search by email : %@", urlString);
        NSLog(@"Params Search by email : %@", xml);
        
        
        NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
        NSString *postLength = [NSString stringWithFormat:@"%d", [postData length]];
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:10.0f];
        //        [request setValue:@"text/xml; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
        [request setValue:securityToken forHTTPHeaderField:@"SecurityToken"];
        [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
        [request setHTTPMethod:@"POST"];
        [request setHTTPBody:postData];
        [request setTimeoutInterval:10.0f];
        
        NSOperationQueue *queue = [[NSOperationQueue alloc] init];
        [NSURLConnection sendAsynchronousRequest:request queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                NSString *stringResponse = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                NSLog(@"search entourage xml response: %@", stringResponse);
                
                
                if(stringResponse.length == 0){
                    NGREGCheckRegistrantResponse * entourage = [[NGREGCheckRegistrantResponse alloc] init];
                    entourage.email = email;
                    block(entourage);
                }else{
                    NGREGSearchEntourageXMLParser* parser = [[NGREGSearchEntourageXMLParser alloc]init];
                    [parser parseXMLToGetGIIDNumberFromData:data withCompleteion:^(NSString * giidNumber) {
                        
                        if([giidNumber isEqualToString:@""]){
                            NGREGCheckRegistrantResponse* entourage = [[NGREGCheckRegistrantResponse alloc] init];
                            entourage.email = email;
                            block(entourage);
                        }else
                            [self checkEntourageByGIIDNumber:giidNumber andCompletionHandler:block];
                    }];
                }
                
            });
            
        }];
        
        
    }];
}

- (NSString *)requestMopLoginWithUsername:(NSString *)username andPassword:(NSString *)password withCompletionHandler:(NGREGRequestCompletionHandler)block {
    
    //    NSString *deviceUDID = [[UIDevice currentDevice]serialNumber] ;
    //    NSString *deviceUGDID = [[UIDevice currentDevice] uniqueGlobalDeviceIdentifier];
    //    NSString *deviceName = [[UIDevice currentDevice] name];
    //
    //    NSString *urlString = [NSString stringWithFormat:@"%@%@", BEAT_API_BASE_URL, BEAT_API_MOP_LOGIN];
    //    NSString *post = [NSString stringWithFormat:@"username=%@&password=%@&deviceid=%@&devicedesc=%@&serialnumber=%@", username, password, deviceUDID, deviceName, deviceUGDID];
    //    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    //    NSString *postLength = [NSString stringWithFormat:@"%d", [postData length]];
    //
    //    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]];
    //    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    //    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    //    [request setHTTPMethod:@"POST"];
    //    [request setHTTPBody:postData];
    //
    //    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    //    [NSURLConnection sendAsynchronousRequest:request queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
    //
    //        if (error == nil) {
    //            error = nil;
    //            NSDictionary *responseDict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
    //
    //            block(data, nil);
    //        } else {
    //            block(nil, error);
    //        }
    //    }];
    
//    username = @"inong@kana.co.id";
//    password = @"beatbeat";
    
    NSString *deviceId = [[BPXLUUIDHandler UUID]stringByReplacingOccurrencesOfString:@"-" withString:@""];
      NSString* serialNumber = deviceId;
//    NSString *serialNumber = [[UIDevice currentDevice] serialNumber];
    NSString *deviceName = [[UIDevice currentDevice] name];
    
    
//    NSString *serialNumber = @"DMPK4CZAF18W";
//    NSString *deviceId = @"98EF7DFC952546EA8B3AC70B5A17E10C";
//    NSString *deviceName = @"Fantomas";

    
    NSDictionary *parameter = @{@"username": username,
                                @"password": password,
                                //                                @"deviceid": [[[[UIDevice currentDevice] identifierForVendor] UUIDString] stringByReplacingOccurrencesOfString:@"-" withString:@""],
                                
                                @"serialnumber":deviceId,
                                @"deviceid":serialNumber,
                                @"devicedesc": deviceName};
    
    
    NSLog(@"Parameter Login Mop : %@", parameter);
    
    [[RKObjectManager sharedManager] postObject:nil path:BEAT_API_MOP_LOGIN parameters:parameter success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
        
        if (block) { //0f607264fc6318a92b9e13c65db7cd3c
            block([mappingResult firstObject], nil);
        }
        
    } failure:^(RKObjectRequestOperation *operation, NSError *error) {
        
        if (block) {            block(nil, error);
       
  }
    }];
    
    return nil;
}


@end
