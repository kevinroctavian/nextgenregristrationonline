//
//  NGREGReport.h
//  NextGenRegistrationOnline
//
//  Created by Kevin Octavian on 2/9/14.
//  Copyright (c) 2014 Kevin R. Octavian. All rights reserved.
//

#import <CoreData/CoreData.h>

@interface NGREGReport : NSManagedObject

@property (nonatomic,strong) NSString * email;
@property (nonatomic,strong) NSNumber * existingRegistrant;
@property (nonatomic,strong) NSString * menu;
@property (nonatomic,strong) NSString * menuId;
@property (nonatomic,strong) NSNumber * totalRegistrant;
@property (nonatomic,strong) NSNumber * approvedRegistrant;
@property (nonatomic,strong) NSNumber * rejectedRegistrant;
@property (nonatomic,strong) NSNumber * submissionVersion;
@property (nonatomic,strong) NSNumber * submissionTotal;
@property (nonatomic,strong) NSDate * submissionLastDate;

@end
