//
//  NGREGSync+Additions.m
//  NextGenRegistrationOnline
//
//  Created by Kevin Octavian on 2/10/14.
//  Copyright (c) 2014 Kevin R. Octavian. All rights reserved.
//

#import "NGREGSync+Additions.h"
#import "NGREGAppDelegate.h"
#import "NGREGEngine.h"
#import "NGREGRootViewController.h"

@implementation NGREGSync (Additions)

+ (NGREGSync *)syncWithData:(NSDictionary *)data email:(NSString *)email menu:(NSString *)menu brandId:(NSString*)brandId type:(NSString *)type andUrl:(NSString*)url inManagedObjectContext:(NSManagedObjectContext *)context {
    
    NGREGSync *sync = [NSEntityDescription insertNewObjectForEntityForName:@"NGREGSync" inManagedObjectContext:context];
    sync.data = data;
    sync.event = menu;
    sync.email = email;
    sync.type = type;
    sync.url = url;
    sync.brandId = brandId;
    
    NSError *saveError = nil;
    [sync.managedObjectContext save:&saveError];
    
    if (saveError != nil) {
        NSLog(@"Save sync error: %@", [saveError localizedDescription]);
    }
    
    return sync;
}

+ (NSArray *)syncsInManagedObjectContext:(NSManagedObjectContext *)context {
    
    NSString* email = [[NSUserDefaults standardUserDefaults] valueForKey:kNGREGEmailNextGen];
    NSString* brandId = [[NSUserDefaults standardUserDefaults] valueForKey:kNGREGBrandIDNextGen];
    
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"NGREGSync"];
    request.predicate = [NSPredicate predicateWithFormat:@"email == %@ AND brandId = %@", email, brandId];

    NSError *error = nil;
    NSArray *syncs = [context executeFetchRequest:request error:&error];
    
    return syncs;
}

+ (void)sync:(NSArray*)sync atIndex:(NSInteger)index withCompletionHandler:(NGREGSyncCompletionHandler)block{
    
    if(index >= [sync count]){
        block(index, [sync count], nil);
        return;
    }
    
    __block NSInteger indexBlock = index;
    __block NGREGSyncCompletionHandler blockBlock = block;
    
    [[NGREGEngine sharedEngine] requestSync:sync[index] withCompletionHandler:^(id result, NSError *error) {
        
        if(index == [sync count] || error != nil){
            block(index, [sync count],error);
        }else{
            block(index + 1, [sync count],nil);
            [self sync:sync atIndex:++indexBlock withCompletionHandler:blockBlock];
        }
    }];
}

+ (void)syncWithCompletionHandler:(NGREGSyncCompletionHandler)block{
    
    NGREGAppDelegate *appDelegate = (NGREGAppDelegate *)[[UIApplication sharedApplication] delegate];
    NSArray *syncs = [NGREGSync syncsInManagedObjectContext:[appDelegate persistentStoreManagedObjectContext]];
    
    NSString* email = [[NSUserDefaults standardUserDefaults] valueForKey:kNGREGEmailNextGen];
    NSString* password = [[NSUserDefaults standardUserDefaults] valueForKey:kNGREGPasswordNextGen];
    NSString* brandId = [[NSUserDefaults standardUserDefaults] valueForKey:kNGREGBrandIDNextGen];
    
    [[NGREGEngine sharedEngine]requestLoginWithUsername:email andPassword:password andBrandId:brandId andWithCompletionHandler:^(id result, NSError *error) {
        if(error == nil)
            [self sync:syncs atIndex:0 withCompletionHandler:block];
        else
            block(0, [syncs count],error);
    }];
    
}

+ (NSInteger)syncsCount {
    
    NGREGAppDelegate *appDelegate = (NGREGAppDelegate *)[[UIApplication sharedApplication] delegate];
    NSArray *syncs = [NGREGSync syncsInManagedObjectContext:[appDelegate persistentStoreManagedObjectContext]];
    
    return [syncs count];
}

+ (NSInteger)syncsCounterCountWithBrandId:(NSString*) brandId {
    
    NSString* email = [[NSUserDefaults standardUserDefaults] valueForKey:kNGREGEmailNextGen];
    
    NGREGAppDelegate *appDelegate = (NGREGAppDelegate *)[[UIApplication sharedApplication] delegate];
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"NGREGSync"];
    request.predicate = [NSPredicate predicateWithFormat:@"email == %@ AND ( type == %@ OR type == %@) AND brandId = %@",email, kNGREGGame, kNGREGRegistration, brandId];
    
    NSError *error = nil;
    NSArray *badges = [[appDelegate persistentStoreManagedObjectContext] executeFetchRequest:request error:&error];
    
    return [badges count];

}

+ (NSInteger)syncsGameCountWithEmail:(NSString *)email menu:(NSString *)menu brandId:(NSString*)brandId{
    
    NGREGAppDelegate *appDelegate = (NGREGAppDelegate *)[[UIApplication sharedApplication] delegate];
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"NGREGSync"];
    request.predicate = [NSPredicate predicateWithFormat:@"email == %@ AND event == %@ AND type == %@ AND brandId = %@", email, menu, kNGREGGame, brandId];
    
    NSError *error = nil;
    NSArray *badges = [[appDelegate persistentStoreManagedObjectContext] executeFetchRequest:request error:&error];
    
    return [badges count];
}

+ (NSInteger)syncsRegistrationCountWithEmail:(NSString *)email menu:(NSString *)menu brandId:(NSString*)brandId{
    
    NGREGAppDelegate *appDelegate = (NGREGAppDelegate *)[[UIApplication sharedApplication] delegate];
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"NGREGSync"];
    request.predicate = [NSPredicate predicateWithFormat:@"email == %@ AND event == %@ AND type == %@ AND brandId = %@", email, menu, kNGREGRegistration, brandId];
    
    NSError *error = nil;
    NSArray *badges = [[appDelegate persistentStoreManagedObjectContext] executeFetchRequest:request error:&error];
    
    return [badges count];
}


@end
