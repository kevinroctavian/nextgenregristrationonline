//
//  NGREGSync+Additions.h
//  NextGenRegistrationOnline
//
//  Created by Kevin Octavian on 2/10/14.
//  Copyright (c) 2014 Kevin R. Octavian. All rights reserved.
//

#import "NGREGSync.h"

#define kNGREGGame                  @"game"
#define kNGREGRegistration          @"registration"
#define kNGREGTrackingActivity      @"trackingActivity"
#define kNGREGSync                  @"kNGREGSync"

typedef void (^NGREGSyncCompletionHandler)(float dataSucces, float allData, __strong NSError *error);

@interface NGREGSync (Additions)

+ (NGREGSync *)syncWithData:(NSDictionary *)data email:(NSString *)email menu:(NSString *)menu brandId:(NSString*)brandId type:(NSString *)type andUrl:(NSString*)url inManagedObjectContext:(NSManagedObjectContext *)context;
+ (NSArray *)syncsInManagedObjectContext:(NSManagedObjectContext *)context;
+ (void)syncWithCompletionHandler:(NGREGSyncCompletionHandler)block;
+ (NSInteger)syncsCount;
+ (NSInteger)syncsGameCountWithEmail:(NSString *)email menu:(NSString *)menu brandId:(NSString*)brandId;
+ (NSInteger)syncsRegistrationCountWithEmail:(NSString *)email menu:(NSString *)menu brandId:(NSString*)brandId;
+ (NSInteger)syncsCounterCountWithBrandId:(NSString*) brandId;

@end
