//
//  NGREGBrand.m
//  NextGenRegistrationOnline
//
//  Created by Kevin Octavian on 1/16/14.
//  Copyright (c) 2014 Kevin R. Octavian. All rights reserved.
//

#import "NGREGBrand.h"

@implementation NGREGBrand

@dynamic brandId;
@dynamic brandName;

@end
