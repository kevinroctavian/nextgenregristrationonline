//
//  NGREGSubmission.h
//  NextGenRegistrationOnline
//
//  Created by Kevin Octavian on 3/20/14.
//  Copyright (c) 2014 Kevin R. Octavian. All rights reserved.
//

#import <CoreData/CoreData.h>

@interface NGREGSubmission : NSManagedObject

@property (nonatomic, retain) NSString * brandId;
@property (nonatomic, retain) NSString * email;
@property (nonatomic, retain) NSString * eventId;
@property (nonatomic, retain) NSNumber * submissionTotal;
@property (nonatomic, retain) NSNumber * submissionVersion;

@end
