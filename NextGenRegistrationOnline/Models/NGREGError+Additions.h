//
//  NGREGError+Additions.h
//  NextGenRegistrationOnline
//
//  Created by Kevin Octavian on 10/7/14.
//  Copyright (c) 2014 Kevin R. Octavian. All rights reserved.
//

#import "NGREGError.h"

@interface NGREGError (Additions)

+ (NGREGError *)errorWithBrand:(NSString*)brand userId:(NSString*)userId timeError:(NSDate *)timeError  reason:(NSString *)reason andDetail:(NSString*)detail inManagedObjectContext:(NSManagedObjectContext *)context ;
+ (NSArray *)errorWithDate:(NSDate*)time inManagedObjectContext:(NSManagedObjectContext *)context ;

@end
