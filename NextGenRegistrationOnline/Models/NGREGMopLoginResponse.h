//
//  NGREGMopLoginResponse.h
//  NextGenRegistrationOnline
//
//  Created by Kevin Octavian on 4/10/14.
//  Copyright (c) 2014 Kevin R. Octavian. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NGREGMopLoginResponse : NSObject

@property (nonatomic, strong) NSNumber * status;
@property (nonatomic, strong) NSString * message;
@property (nonatomic, strong) NSString * sessionID;

@end
