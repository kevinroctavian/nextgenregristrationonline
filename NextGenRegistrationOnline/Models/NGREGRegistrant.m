//
//  NGREGRegistrant.m
//  NextGenRegistrationOnline
//
//  Created by Kevin Octavian on 10/10/14.
//  Copyright (c) 2014 Kevin R. Octavian. All rights reserved.
//

#import "NGREGRegistrant.h"

@implementation NGREGRegistrant

@dynamic brand1Id;
@dynamic city;
@dynamic dateOfBirth;
@dynamic email;
@dynamic firstName;
@dynamic fullName;
@dynamic gIIDNumber;
@dynamic gIIDType;
@dynamic lastName;
@dynamic mobile;
@dynamic registrationID;
@dynamic sex;
@dynamic signDSTImage;
@dynamic signImage;
@dynamic socialAccount;
@dynamic spareText5;
@dynamic subBrand1Id;
@dynamic userId;
@dynamic eventId;

@end
