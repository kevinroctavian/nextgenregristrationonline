//
//  NGREGTheme+Additions.h
//  NextGenRegistrationOnline
//
//  Created by Kevin Octavian on 1/17/14.
//  Copyright (c) 2014 Kevin R. Octavian. All rights reserved.
//

#import "NGREGTheme.h"

@interface NGREGTheme (Additions)

+ (NSArray *)themeWith:(NSString*)brandId InManagedObjectContext:(NSManagedObjectContext *)context;

@end
