//
//  NGREGError.m
//  NextGenRegistrationOnline
//
//  Created by Kevin Octavian on 10/7/14.
//  Copyright (c) 2014 Kevin R. Octavian. All rights reserved.
//

#import "NGREGError.h"

@implementation NGREGError

@dynamic brand;
@dynamic detail;
@dynamic reason;
@dynamic timeError;
@dynamic userid;

@end
