//
//  NGREGSync.h
//  NextGenRegistrationOnline
//
//  Created by Kevin Octavian on 2/10/14.
//  Copyright (c) 2014 Kevin R. Octavian. All rights reserved.
//

#import <CoreData/CoreData.h>

@interface NGREGSync : NSManagedObject

@property (nonatomic, retain) id data;
@property (nonatomic, retain) NSString * email;
@property (nonatomic, retain) NSString * event;
@property (nonatomic, retain) id images;
@property (nonatomic, retain) NSString * type;
@property (nonatomic, retain) NSString * url;
@property (nonatomic, retain) NSString * brandId;

@end
