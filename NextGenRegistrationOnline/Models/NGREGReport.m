//
//  NGREGReport.m
//  NextGenRegistrationOnline
//
//  Created by Kevin Octavian on 2/9/14.
//  Copyright (c) 2014 Kevin R. Octavian. All rights reserved.
//

#import "NGREGReport.h"

@implementation NGREGReport

@dynamic email;
@dynamic existingRegistrant;
@dynamic menu;
@dynamic totalRegistrant;
@dynamic approvedRegistrant;
@dynamic rejectedRegistrant;
@dynamic menuId;
@dynamic submissionLastDate;
@dynamic submissionTotal;
@dynamic submissionVersion;

@end
