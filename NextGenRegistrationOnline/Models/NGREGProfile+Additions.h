//
//  NGREGProfile+Additions.h
//  NextGenRegistrationOnline
//
//  Created by Kevin Octavian on 1/21/14.
//  Copyright (c) 2014 Kevin R. Octavian. All rights reserved.
//

#import "NGREGProfile.h"

@interface NGREGProfile (Additions)

+ (NSArray *)profileWithEmail:(NSString*)email InManagedObjectContext:(NSManagedObjectContext *)context;

@end
