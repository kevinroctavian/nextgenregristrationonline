//
//  NGREGRegistrant+Additions.m
//  NextGenRegistrationOnline
//
//  Created by Kevin Octavian on 10/10/14.
//  Copyright (c) 2014 Kevin R. Octavian. All rights reserved.
//

#import "NGREGRegistrant+Additions.h"

@implementation NGREGRegistrant (Additions)

+ (NGREGRegistrant *)registrantWithRegistrant:(NGREGCheckRegistrantResponse*)checkRegistrant inManagedObjectContext:(NSManagedObjectContext *)context {
    
    NGREGRegistrant *registrant = [NSEntityDescription insertNewObjectForEntityForName:@"NGREGRegistrant" inManagedObjectContext:context];
    registrant.dateOfBirth = checkRegistrant.dateOfBirth;
    registrant.fullName = checkRegistrant.fullName;
    registrant.registrationID = checkRegistrant.registrationID;
    registrant.firstName = checkRegistrant.firstName;
    registrant.lastName = checkRegistrant.lastName;
    registrant.email = checkRegistrant.email;
    registrant.gIIDType = checkRegistrant.gIIDType;
    registrant.gIIDNumber = checkRegistrant.gIIDNumber;
    registrant.mobile = checkRegistrant.mobile;
    registrant.spareText5 = checkRegistrant.spareText5;
    registrant.socialAccount = checkRegistrant.socialAccount;
    registrant.sex = checkRegistrant.sex;
    registrant.city = checkRegistrant.city;
    registrant.brand1Id = checkRegistrant.brand1Id;
    registrant.subBrand1Id = checkRegistrant.subBrand1Id;
    registrant.eventId = checkRegistrant.eventId;
    registrant.userId = checkRegistrant.userId;
    
    NSDate* sourceDate = [NSDate date];
    
    NSTimeZone* sourceTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"GMT"];
    NSTimeZone* destinationTimeZone = [NSTimeZone systemTimeZone];
    
    NSInteger sourceGMTOffset = [sourceTimeZone secondsFromGMTForDate:sourceDate];
    NSInteger destinationGMTOffset = [destinationTimeZone secondsFromGMTForDate:sourceDate];
    NSTimeInterval interval = destinationGMTOffset - sourceGMTOffset;
    
    NSDate* now = [[NSDate alloc] initWithTimeInterval:interval sinceDate:sourceDate];
    
    registrant.registerDate = now;
    
    
//    NSCalendar *cal = [NSCalendar currentCalendar];
//    NSDateComponents *components = [cal components:( NSDayCalendarUnit | NSYearCalendarUnit | NSMonthCalendarUnit | NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit ) fromDate:[[NSDate alloc] init]];
//    
//    [components setDay:([components day]-30)];
//    
//    NSDate* date = [cal dateFromComponents:components];
//    
//    registrant.registerDate = date;
    
    
    registrant.signDSTImage = [NSData dataWithData:UIImagePNGRepresentation(checkRegistrant.signDSTImage)] ;
    registrant.signImage = [NSData dataWithData:UIImagePNGRepresentation(checkRegistrant.signDSTImage)] ;
    
    NSError *saveError = nil;
    [registrant.managedObjectContext save:&saveError];
    
//    [registrant.managedObjectContext setValue:[NSData dataWithData:UIImagePNGRepresentation(checkRegistrant.signDSTImage)] forKeyPath:@"signDSTImage"] ;
//    [registrant.managedObjectContext setValue:[NSData dataWithData:UIImagePNGRepresentation(checkRegistrant.signImage)] forKeyPath:@"signImage"] ;
    
    
    
    if (saveError != nil) {
        NSLog(@"Save sync error: %@", [saveError localizedDescription]);
    }
    
    return registrant;
}

+ (NSArray *)registrantBefore2WeeksinManagedObjectContext:(NSManagedObjectContext *)context {
    
    NSCalendar *cal = [NSCalendar currentCalendar];
    NSDateComponents *components = [cal components:( NSDayCalendarUnit | NSYearCalendarUnit | NSMonthCalendarUnit | NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit ) fromDate:[[NSDate alloc] init]];
    
    [components setDay:([components day]-14)];

    NSDate* date = [cal dateFromComponents:components];
    
    
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"NGREGRegistrant"];
    request.predicate = [NSPredicate predicateWithFormat:@"registerDate >= %@ ", date];
    request.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"registerDate" ascending:NO]];
    
    NSError *error = nil;
    NSArray *badges = [context executeFetchRequest:request error:&error];
    
    return badges;
}



@end
