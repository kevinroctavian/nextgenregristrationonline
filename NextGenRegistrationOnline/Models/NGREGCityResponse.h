//
//  NGREGCityResponse.h
//  NextGenRegistrationOnline
//
//  Created by Kevin Octavian on 2/6/14.
//  Copyright (c) 2014 Kevin R. Octavian. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NGREGCityResponse : NSObject

@property (nonatomic, strong) NSSet * data;
@property (nonatomic, strong) NSNumber * result;

@end
