//
//  NGREGTheme.h
//  NextGenRegistrationOnline
//
//  Created by Kevin Octavian on 1/17/14.
//  Copyright (c) 2014 Kevin R. Octavian. All rights reserved.
//

#import <CoreData/CoreData.h>

@interface NGREGTheme : NSManagedObject

@property (nonatomic,strong) NSString * globalImageUrl;
@property (nonatomic,strong) NSString * homeImageUrl;
@property (nonatomic,strong) NSString * textColor;
@property (nonatomic,strong) NSString * brandId;
@property (nonatomic,strong) NSString * formCellBg;
@property (nonatomic,strong) NSString * formCellBgDisable;
@property (nonatomic,strong) NSString * formCellTextSize;
@property (nonatomic,strong) NSString * loginBrandName;
@property (nonatomic,strong) NSString * textboxTitleSize;
@property (nonatomic,strong) NSString * labelTextSize;
@property (nonatomic,strong) NSNumber * textSize;

@end
