//
//  NGREGRegistrant+Additions.h
//  NextGenRegistrationOnline
//
//  Created by Kevin Octavian on 10/10/14.
//  Copyright (c) 2014 Kevin R. Octavian. All rights reserved.
//

#import "NGREGRegistrant.h"
#import "NGREGCheckRegistrantResponse.h"

@interface NGREGRegistrant (Additions)

+ (NGREGRegistrant *)registrantWithRegistrant:(NGREGCheckRegistrantResponse*)checkRegistrant inManagedObjectContext:(NSManagedObjectContext *)context ;
+ (NSArray *)registrantBefore2WeeksinManagedObjectContext:(NSManagedObjectContext *)context ;

@end
