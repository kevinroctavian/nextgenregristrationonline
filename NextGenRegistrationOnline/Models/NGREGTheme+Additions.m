//
//  NGREGTheme+Additions.m
//  NextGenRegistrationOnline
//
//  Created by Kevin Octavian on 1/17/14.
//  Copyright (c) 2014 Kevin R. Octavian. All rights reserved.
//

#import "NGREGTheme+Additions.h"

@implementation NGREGTheme (Additions)

+ (NSArray *)themeWith:(NSString*)brandId InManagedObjectContext:(NSManagedObjectContext *)context {
    
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"NGREGTheme"];
    request.predicate = [NSPredicate predicateWithFormat:@"brandId = %@ ", brandId];
    request.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"brandId" ascending:YES]];
    
    NSError *error = nil;
    NSArray *badges = [context executeFetchRequest:request error:&error];
    
    return badges;
}

@end
