//
//  NGREGSubBrand.h
//  NextGenRegistrationOnline
//
//  Created by Kevin Octavian on 2/5/14.
//  Copyright (c) 2014 Kevin R. Octavian. All rights reserved.
//

#import <CoreData/CoreData.h>

@interface NGREGSubBrand : NSManagedObject

@property (nonatomic,strong) NSString * brandName;
@property (nonatomic,strong) NSString * code;
@property (nonatomic,strong) NSString * nStatus;
@property (nonatomic,strong) NSString * preference;
@property (nonatomic,strong) NSString * preferenceId;
@property (nonatomic,strong) NSString * subBrandName;
@property (nonatomic,strong) NSString * theId;

@end
