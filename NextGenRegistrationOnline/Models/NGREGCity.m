//
//  NGREGCity.m
//  NextGenRegistrationOnline
//
//  Created by Kevin Octavian on 2/5/14.
//  Copyright (c) 2014 Kevin R. Octavian. All rights reserved.
//

#import "NGREGCity.h"

@implementation NGREGCity


@dynamic city;
@dynamic cityId;
@dynamic cityIdMop;
@dynamic coor;
@dynamic nStatus;
@dynamic provinceId;
@dynamic provinceName;

@end
