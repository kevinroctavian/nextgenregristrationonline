//
//  NGREGSubBrand+Additions.h
//  NextGenRegistrationOnline
//
//  Created by Kevin Octavian on 2/5/14.
//  Copyright (c) 2014 Kevin R. Octavian. All rights reserved.
//

#import "NGREGSubBrand.h"

@interface NGREGSubBrand (Additions)

+ (NSArray *)subBrandsInManagedObjectContext:(NSManagedObjectContext *)context;

@end
