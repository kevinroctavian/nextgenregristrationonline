//
//  NGREGBrand+Additions.h
//  NextGenRegistrationOnline
//
//  Created by Kevin Octavian on 1/16/14.
//  Copyright (c) 2014 Kevin R. Octavian. All rights reserved.
//

#import "NGREGBrand.h"

@interface NGREGBrand (Additions)


+ (NSArray *)brandInManagedObjectContext:(NSManagedObjectContext *)context;
+ (NGREGBrand *)brandWithBrandId:(NSString*) brandId inManagedObjectContext:(NSManagedObjectContext *)context;

@end
