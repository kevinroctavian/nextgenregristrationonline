//
//  NGREGBrand+Additions.m
//  NextGenRegistrationOnline
//
//  Created by Kevin Octavian on 1/16/14.
//  Copyright (c) 2014 Kevin R. Octavian. All rights reserved.
//

#import "NGREGBrand+Additions.h"

@implementation NGREGBrand (Additions)

+ (NSArray *)brandInManagedObjectContext:(NSManagedObjectContext *)context {
    
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"NGREGBrand"];
    request.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"brandId" ascending:YES]];
    
    NSError *error = nil;
    NSArray *badges = [context executeFetchRequest:request error:&error];
    
    return badges;
}

+ (NGREGBrand *)brandWithBrandId:(NSString*) brandId inManagedObjectContext:(NSManagedObjectContext *)context {
    
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"NGREGBrand"];
    request.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"brandId" ascending:YES]];
    request.predicate = [NSPredicate predicateWithFormat:@"brandId == %@", brandId];
    
    NSError *error = nil;
    NSArray *badges = [context executeFetchRequest:request error:&error];

    if([badges count] > 0)
        return badges[0];
    else
        return Nil;
}

@end
