//
//  NGREGReport+Additions.m
//  NextGenRegistrationOnline
//
//  Created by Kevin Octavian on 2/9/14.
//  Copyright (c) 2014 Kevin R. Octavian. All rights reserved.
//

#import "NGREGReport+Additions.h"

@implementation NGREGReport (Additions)

+ (NSArray *)reportWithEmail:(NSString*)email andMenuId:(NSString*)menu inManagedObjectContext:(NSManagedObjectContext *)context {
    
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"NGREGReport"];
    request.predicate = [NSPredicate predicateWithFormat:@"email == %@ AND menuId == %@", email, menu];
    
    NSError *error = nil;
    NSArray *badges = [context executeFetchRequest:request error:&error];
    
    return badges;
}

+ (BOOL)reportExistingRegistrantWithEmail:(NSString*)email andMenu:(NSString*)menu inManagedObjectContext:(NSManagedObjectContext *)context {
    
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"NGREGReport"];
    request.predicate = [NSPredicate predicateWithFormat:@"email == %@ AND menu == %@", email, menu];
    
    NSError *error = nil;
    NSArray *registrants = [context executeFetchRequest:request error:&error];
    
    NGREGReport *registrant = nil;
    
    
    if ([registrants count] == 0) {
        
        registrant = [NSEntityDescription insertNewObjectForEntityForName:@"NGREGReport" inManagedObjectContext:context];
        registrant.email = email;
        registrant.menu = menu;
        
    } else {
        registrant = registrants[0];
    }
    
    registrant.existingRegistrant = [NSNumber numberWithInt:([registrant.existingRegistrant intValue] + 1)];
    
    NSError *saveError = nil;
    [registrant.managedObjectContext save:&saveError];
    
    return  (saveError != nil);
}

+ (BOOL)reportNewRegistrantWithEmail:(NSString*)email andMenu:(NSString*)menu inManagedObjectContext:(NSManagedObjectContext *)context {
    
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"NGREGReport"];
    request.predicate = [NSPredicate predicateWithFormat:@"email == %@ AND menu == %@", email, menu];
    
    NSError *error = nil;
    NSArray *registrants = [context executeFetchRequest:request error:&error];
    
    NGREGReport *registrant = nil;
    
    
    if ([registrants count] == 0) {
        
        registrant = [NSEntityDescription insertNewObjectForEntityForName:@"NGREGReport" inManagedObjectContext:context];
        registrant.email = email;
        registrant.menu = menu;
        
    } else {
        registrant = registrants[0];
    }
    
    registrant.totalRegistrant = [NSNumber numberWithInt:([registrant.totalRegistrant intValue] + 1)];
    
    NSError *saveError = nil;
    [registrant.managedObjectContext save:&saveError];
    
    return  (saveError != nil);
}

@end
