//
//  NGREGProfile.m
//  NextGenRegistrationOnline
//
//  Created by Kevin Octavian on 1/21/14.
//  Copyright (c) 2014 Kevin R. Octavian. All rights reserved.
//

#import "NGREGProfile.h"

@implementation NGREGProfile

@dynamic brandId;
@dynamic customizeTemplates;
@dynamic lastName;
@dynamic email;
@dynamic name;
@dynamic profileId;
@dynamic userName;
@dynamic menu;
@dynamic messageError;

@end
