//
//  NGREGCity+Additions.h
//  NextGenRegistrationOnline
//
//  Created by Kevin Octavian on 2/5/14.
//  Copyright (c) 2014 Kevin R. Octavian. All rights reserved.
//

#import "NGREGCity.h"

@interface NGREGCity (Additions)

+ (NSArray *)cityInManagedObjectContext:(NSManagedObjectContext *)context;

@end
