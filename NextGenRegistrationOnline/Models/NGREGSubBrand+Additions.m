//
//  NGREGSubBrand+Additions.m
//  NextGenRegistrationOnline
//
//  Created by Kevin Octavian on 2/5/14.
//  Copyright (c) 2014 Kevin R. Octavian. All rights reserved.
//

#import "NGREGSubBrand+Additions.h"

@implementation NGREGSubBrand (Additions)

+ (NSArray *)subBrandsInManagedObjectContext:(NSManagedObjectContext *)context {
    
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"NGREGSubBrand"];
    
    NSError *error = nil;
    NSArray *badges = [context executeFetchRequest:request error:&error];
    
    return badges;
}

@end
