//
//  NGREGSubmission.m
//  NextGenRegistrationOnline
//
//  Created by Kevin Octavian on 3/20/14.
//  Copyright (c) 2014 Kevin R. Octavian. All rights reserved.
//

#import "NGREGSubmission.h"

@implementation NGREGSubmission

@dynamic email;
@dynamic brandId;
@dynamic submissionVersion;
@dynamic submissionTotal;
@dynamic eventId;

@end
