//
//  NGREGCity.h
//  NextGenRegistrationOnline
//
//  Created by Kevin Octavian on 2/5/14.
//  Copyright (c) 2014 Kevin R. Octavian. All rights reserved.
//

#import <CoreData/CoreData.h>

@interface NGREGCity : NSManagedObject

@property (nonatomic,strong) NSString * city;
@property (nonatomic,strong) NSString * cityId;
@property (nonatomic,strong) NSString * cityIdMop;
@property (nonatomic,strong) NSString * coor;
@property (nonatomic,strong) NSString * nStatus;
@property (nonatomic,strong) NSString * provinceId;
@property (nonatomic,strong) NSString * provinceName;

@end
