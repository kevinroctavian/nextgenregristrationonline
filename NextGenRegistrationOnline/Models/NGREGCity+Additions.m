//
//  NGREGCity+Additions.m
//  NextGenRegistrationOnline
//
//  Created by Kevin Octavian on 2/5/14.
//  Copyright (c) 2014 Kevin R. Octavian. All rights reserved.
//

#import "NGREGCity+Additions.h"

@implementation NGREGCity (Additions)

+ (NSArray *)cityInManagedObjectContext:(NSManagedObjectContext *)context {
    
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"NGREGCity"];
    
    NSError *error = nil;
    NSArray *badges = [context executeFetchRequest:request error:&error];
    
    return badges;
}

@end
