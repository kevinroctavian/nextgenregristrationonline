//
//  NGREGSubmission+Additions.h
//  NextGenRegistrationOnline
//
//  Created by Kevin Octavian on 3/20/14.
//  Copyright (c) 2014 Kevin R. Octavian. All rights reserved.
//

#import "NGREGSubmission.h"

@interface NGREGSubmission (Additions)

+ (NSArray *)submissionWithEmail:(NSString *)email brandId:(NSString*)brandId eventId:(NSString*)eventId inManagedObjectContext:(NSManagedObjectContext *)context;


@end
