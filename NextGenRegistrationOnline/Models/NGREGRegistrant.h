//
//  NGREGRegistrant.h
//  NextGenRegistrationOnline
//
//  Created by Kevin Octavian on 10/10/14.
//  Copyright (c) 2014 Kevin R. Octavian. All rights reserved.
//

#import <CoreData/CoreData.h>

@interface NGREGRegistrant : NSManagedObject


@property (nonatomic, strong) NSString * registrationID;
@property (nonatomic, strong) NSString * firstName;
@property (nonatomic, strong) NSString * lastName;
@property (nonatomic, strong) NSString * dateOfBirth;
@property (nonatomic, strong) NSString * email;
@property (nonatomic, strong) NSString * gIIDType;
@property (nonatomic, strong) NSString * gIIDNumber;
@property (nonatomic, strong) NSString * mobile;
@property (nonatomic, strong) NSString * spareText5;
@property (nonatomic, strong) NSString * socialAccount;
@property (nonatomic, strong) NSString * sex;
@property (nonatomic, strong) NSString * city;
@property (nonatomic, strong) NSString * fullName;
@property (nonatomic, strong) NSString * brand1Id;
@property (nonatomic, strong) NSString * subBrand1Id;
@property (nonatomic, strong) NSDate * registerDate;

@property (nonatomic, strong) NSString * userId;
@property (nonatomic, strong) NSString * eventId;

@property (nonatomic, strong) NSData * signImage;
@property (nonatomic, strong) NSData * signDSTImage;

@end
