//
//  NGREGSync.m
//  NextGenRegistrationOnline
//
//  Created by Kevin Octavian on 2/10/14.
//  Copyright (c) 2014 Kevin R. Octavian. All rights reserved.
//

#import "NGREGSync.h"

@implementation NGREGSync

@dynamic data;
@dynamic email;
@dynamic event;
@dynamic images;
@dynamic type;
@dynamic url;
@dynamic brandId;

@end
