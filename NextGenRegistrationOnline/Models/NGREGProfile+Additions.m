//
//  NGREGProfile+Additions.m
//  NextGenRegistrationOnline
//
//  Created by Kevin Octavian on 1/21/14.
//  Copyright (c) 2014 Kevin R. Octavian. All rights reserved.
//

#import "NGREGProfile+Additions.h"

@implementation NGREGProfile (Additions)

+ (NSArray *)profileWithEmail:(NSString*)email InManagedObjectContext:(NSManagedObjectContext *)context {
    
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"NGREGProfile"];
    request.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"profileId" ascending:YES]];
    request.predicate = [NSPredicate predicateWithFormat:@"userName CONTAINS[cd] %@", email];
    
    
    NSError *error = nil;
    NSArray *badges = [context executeFetchRequest:request error:&error];
    
    return badges;
}

@end
