//
//  NGREGResponse.h
//  NextGenRegistrationOnline
//
//  Created by Kevin Octavian on 2/5/14.
//  Copyright (c) 2014 Kevin R. Octavian. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NGREGResponse : NSObject

@property (nonatomic, strong) NSNumber * status;
@property (nonatomic, strong) NSNumber * code;
@property (nonatomic, strong) NSString * message;

@end
