//
//  NGREGSubBrand.m
//  NextGenRegistrationOnline
//
//  Created by Kevin Octavian on 2/5/14.
//  Copyright (c) 2014 Kevin R. Octavian. All rights reserved.
//

#import "NGREGSubBrand.h"

@implementation NGREGSubBrand

@dynamic brandName;
@dynamic code;
@dynamic nStatus;
@dynamic preference;
@dynamic preferenceId;
@dynamic subBrandName;
@dynamic theId;

@end
