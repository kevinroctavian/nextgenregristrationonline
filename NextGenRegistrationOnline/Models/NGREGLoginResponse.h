//
//  NGREGLoginResponse.h
//  NextGenRegistrationOnline
//
//  Created by Kevin Octavian on 1/16/14.
//  Copyright (c) 2014 Kevin R. Octavian. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NGREGLoginResponse : NSObject

@property (nonatomic, strong) NSNumber * status;
@property (nonatomic, strong) NSNumber * code;
@property (nonatomic, strong) NSString * msg;
@property (nonatomic, strong) NSString * adminname;
@property (nonatomic, strong) NSString * adminuser;
@property (nonatomic, strong) NSString * version;
@property (nonatomic, strong) NSString * messageVersion;

@end
