//
//  NGREGError+Additions.m
//  NextGenRegistrationOnline
//
//  Created by Kevin Octavian on 10/7/14.
//  Copyright (c) 2014 Kevin R. Octavian. All rights reserved.
//

#import "NGREGError+Additions.h"

@implementation NGREGError (Additions)

+ (NGREGError *)errorWithBrand:(NSString*)brand userId:(NSString*)userId timeError:(NSDate *)timeError  reason:(NSString *)reason andDetail:(NSString*)detail inManagedObjectContext:(NSManagedObjectContext *)context {
    
    NGREGError *error = [NSEntityDescription insertNewObjectForEntityForName:@"NGREGError" inManagedObjectContext:context];
    error.brand = brand;
    error.userid = userId;
    error.timeError = timeError;
    error.reason = reason;
    error.detail = detail;
    
    NSError *saveError = nil;
    [error.managedObjectContext save:&saveError];
    
    if (saveError != nil) {
        NSLog(@"Save sync error: %@", [saveError localizedDescription]);
    }
    
    return error;
}


+ (NSArray *)errorWithDate:(NSDate*)time inManagedObjectContext:(NSManagedObjectContext *)context {
    
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"NGREGError"];
    request.predicate = [NSPredicate predicateWithFormat:@"timeError == %@ ", time];
    
    NSError *error = nil;
    NSArray *badges = [context executeFetchRequest:request error:&error];
    
    return badges;
}

@end
