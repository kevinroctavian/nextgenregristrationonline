//
//  NGREGReport+Additions.h
//  NextGenRegistrationOnline
//
//  Created by Kevin Octavian on 2/9/14.
//  Copyright (c) 2014 Kevin R. Octavian. All rights reserved.
//

#import "NGREGReport.h"

@interface NGREGReport (Additions)

+ (NSArray *)reportWithEmail:(NSString*)email andMenuId:(NSString*)menu inManagedObjectContext:(NSManagedObjectContext *)context;
//+ (BOOL)reportExistingRegistrantWithEmail:(NSString*)email andMenu:(NSString*)menu inManagedObjectContext:(NSManagedObjectContext *)context;
//+ (BOOL)reportNewRegistrantWithEmail:(NSString*)email andMenu:(NSString*)menu inManagedObjectContext:(NSManagedObjectContext *)context;

@end
