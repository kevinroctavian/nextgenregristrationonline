//
//  NGREGError.h
//  NextGenRegistrationOnline
//
//  Created by Kevin Octavian on 10/7/14.
//  Copyright (c) 2014 Kevin R. Octavian. All rights reserved.
//

#import <CoreData/CoreData.h>

@interface NGREGError : NSManagedObject

@property (nonatomic, retain) NSString * brand;
@property (nonatomic, retain) NSString * detail;
@property (nonatomic, retain) NSString * reason;
@property (nonatomic, retain) NSDate * timeError;
@property (nonatomic, retain) NSString * userid;

@end
