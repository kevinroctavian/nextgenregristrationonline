//
//  NGREGBrand.h
//  NextGenRegistrationOnline
//
//  Created by Kevin Octavian on 1/16/14.
//  Copyright (c) 2014 Kevin R. Octavian. All rights reserved.
//

#import <CoreData/CoreData.h>

@interface NGREGBrand : NSManagedObject


@property (nonatomic,strong) NSString * brandName;
@property (nonatomic,strong) NSString * brandId;

@end
