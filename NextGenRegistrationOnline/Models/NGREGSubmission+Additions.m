//
//  NGREGSubmission+Additions.m
//  NextGenRegistrationOnline
//
//  Created by Kevin Octavian on 3/20/14.
//  Copyright (c) 2014 Kevin R. Octavian. All rights reserved.
//

#import "NGREGSubmission+Additions.h"

@implementation NGREGSubmission (Additions)

+ (NGREGSubmission *)cekVersionWithEmail:(NSString*)email brandId:(NSString*)brandId version:(NSNumber *)version andTotal:(NSNumber*)total inManagedObjectContext:(NSManagedObjectContext *)context {
    
    NGREGSubmission *submission = [NSEntityDescription insertNewObjectForEntityForName:@"NGREGSubmission" inManagedObjectContext:context];
    submission.submissionVersion = version;
    submission.submissionTotal = total;
    submission.email = email;
    submission.brandId = brandId;
    
    NSError *saveError = nil;
    [submission.managedObjectContext save:&saveError];
    
    if (saveError != nil) {
        NSLog(@"Save sync error: %@", [saveError localizedDescription]);
    }
    
    return submission;
}

+ (NSArray *)submissionWithEmail:(NSString *)email brandId:(NSString*)brandId eventId:(NSString*)eventId inManagedObjectContext:(NSManagedObjectContext *)context{
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"NGREGSubmission"];
    request.predicate = [NSPredicate predicateWithFormat:@"email == %@ AND brandId = %@ AND eventId = %@", email, brandId, eventId];
    
    NSError *error = nil;
    NSArray *syncs = [context executeFetchRequest:request error:&error];
    
    return syncs;
}

@end
