//
//  NGREGProfile.h
//  NextGenRegistrationOnline
//
//  Created by Kevin Octavian on 1/21/14.
//  Copyright (c) 2014 Kevin R. Octavian. All rights reserved.
//

#import <CoreData/CoreData.h>

@interface NGREGProfile : NSManagedObject

@property (nonatomic,strong) NSString * brandId;
@property (nonatomic,strong) id customizeTemplates;
@property (nonatomic,strong) id messageError;
@property (nonatomic,strong) NSString * lastName;
@property (nonatomic,strong) NSString * email;
@property (nonatomic,strong) NSString * name;
@property (nonatomic,strong) NSString * profileId;
@property (nonatomic,strong) NSString * userName;
@property (nonatomic,strong) id menu;

@end
