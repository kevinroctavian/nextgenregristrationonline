//
//  NGREGTheme.m
//  NextGenRegistrationOnline
//
//  Created by Kevin Octavian on 1/17/14.
//  Copyright (c) 2014 Kevin R. Octavian. All rights reserved.
//

#import "NGREGTheme.h"

@implementation NGREGTheme

@dynamic brandId;
@dynamic homeImageUrl;
@dynamic textSize;
@dynamic globalImageUrl;
@dynamic textColor;
@dynamic formCellBg;
@dynamic formCellBgDisable;
@dynamic formCellTextSize;
@dynamic loginBrandName;
@dynamic labelTextSize;
@dynamic textboxTitleSize;

@end
