//
//  NGREGBottomPanelViewController.m
//  NextGenRegistrationOnline
//
//  Created by Kevin Octavian on 1/22/14.
//  Copyright (c) 2014 Kevin R. Octavian. All rights reserved.
//

#import "NGREGBottomPanelViewController.h"
#import "NGREGUtil.h"
#import "NGREGSync+Additions.h"
#import "Constant.h"

@interface NGREGBottomPanelViewController ()<UICollectionViewDataSource, UICollectionViewDelegate> {
    BOOL _isSubpanelViewOpen;
    NSUInteger _selectedIndex;
    CGFloat kMinSubpanelViewY, kMaxSubpanelViewY;
    NSInteger textSize, fontId;
    NSString *textColor, *backgroundColor, *bgUrl, *md5;
    int syncCount;
}
@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

@end

@implementation NGREGBottomPanelViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    [self.collectionView setDelegate:self];
    [self.collectionView setDataSource:self];
    kMaxSubpanelViewY   = 768 - kPanelViewVisibleHeight;
    kMinSubpanelViewY = kMaxSubpanelViewY -100;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Setter

- (void)setDataArray:(NSArray *)dataArray{
    
    kMinSubpanelViewY = kMaxSubpanelViewY - kPanelItemViewHeight * (([dataArray count] + 3)/4);
    _dataArray = dataArray;
    syncCount = [NGREGSync syncsCounterCountWithBrandId:[[NSUserDefaults standardUserDefaults] valueForKey:kNGREGBrandIDNextGen]];
    [self.collectionView reloadData];
}

- (void)setTextSize:(NSInteger)textSizeL andTextColor:(NSString*)textColorL andBackgroundColor:(NSString*)bgColorL andFontId:(NSInteger)fontIdL andBgUrl:(NSString*) bgUrlL andMD5:(NSString*)md5L{
    textSize = textSizeL;
    textColor = textColorL;
    backgroundColor = bgColorL;
    fontId = fontIdL;
    bgUrl = bgUrlL;
    md5 = md5L;
    [self.collectionView reloadData];
}

#pragma mark - Helpher methods

- (void)refresh{
    syncCount = [NGREGSync syncsCounterCountWithBrandId:[[NSUserDefaults standardUserDefaults] valueForKey:kNGREGBrandIDNextGen]];
    [self.collectionView reloadData];
}

#pragma mark - Actions

- (IBAction)tap:(UITapGestureRecognizer *)sender {
    
    if (sender.state == UIGestureRecognizerStateChanged ||
        sender.state == UIGestureRecognizerStateEnded) {
        
        CGRect frame = self.view.frame;
        CGFloat y = (_isSubpanelViewOpen? kMaxSubpanelViewY: kMinSubpanelViewY);
        
        _isSubpanelViewOpen = !_isSubpanelViewOpen;
        
        [UIView animateWithDuration:0.2f animations:^{
            self.view.frame = CGRectMake(frame.origin.x, y, frame.size.width, frame.size.height);
            
        } completion:^(BOOL finished) {
//            [UIView animateWithDuration:0.2f animations:^{
//                self.subpanelArrowImageView.transform = CGAffineTransformMakeRotation((M_PI *(y /kMaxSubpanelViewY)) -M_PI);
//            }];
        }];
    }
}

- (IBAction)pan:(UIPanGestureRecognizer *)sender {
    
    if (sender.state == UIGestureRecognizerStateChanged) {
        
        CGPoint translation = [sender translationInView:sender.view];
        CGRect frame = self.view.frame;
        CGFloat y = MAX(kMinSubpanelViewY, MIN(kMaxSubpanelViewY, frame.origin.y +translation.y));
        self.view.frame = CGRectMake(frame.origin.x, y, frame.size.width, frame.size.height);
        
        [sender setTranslation:CGPointZero inView:sender.view];
        
    } else if (sender.state == UIGestureRecognizerStateEnded) {
        
        CGRect frame = self.view.frame;
        CGFloat y = (_isSubpanelViewOpen? kMaxSubpanelViewY: kMinSubpanelViewY);
        
        if (!_isSubpanelViewOpen) {
            
            if (kMaxSubpanelViewY - frame.origin.y > 44.0f) {
                y = kMinSubpanelViewY;
                _isSubpanelViewOpen = !_isSubpanelViewOpen;
            } else {
                y = kMaxSubpanelViewY;
            }
            
        } else {
            
            if (frame.origin.y > 44.0f) {
                y = kMaxSubpanelViewY;
                _isSubpanelViewOpen = !_isSubpanelViewOpen;
            } else {
                y = kMinSubpanelViewY;
            }
        }
        
        [UIView animateWithDuration:0.2f animations:^{
            self.view.frame = CGRectMake(frame.origin.x, y, frame.size.width, frame.size.height);
        } completion:^(BOOL finished) {
//            [UIView animateWithDuration:0.2f animations:^{
//                self.subpanelArrowImageView.transform = CGAffineTransformMakeRotation((M_PI *(y /kMaxSubpanelViewY)) -M_PI);
//            }];
        }];
        
        [sender setTranslation:CGPointZero inView:sender.view];
    }
}


#pragma mark - UICollectionViewDataSource, UICollectionViewDelegate

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    return [self.dataArray count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellId = @"MenuCellId";
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:CellId forIndexPath:indexPath];
    
    NSDictionary* menu = self.dataArray[indexPath.item];
    

    UILabel *label = (UILabel *)[cell viewWithTag:101];
    UILabel *syncLabel = (UILabel *)[cell viewWithTag:105];
    UIView *syncBg = (UIView *)[cell viewWithTag:109];
    UIImageView *bgImage = (UIImageView *)[cell viewWithTag:113];
    
    [label setText:[menu objectForKey:@"name"]];
    
    if([label.text isEqualToString:@"SYNC"]){
        if(syncCount > 0){
            [syncLabel setText:[NSString stringWithFormat:@"%d", syncCount]];
            syncBg.hidden = NO;
        }else{
            syncBg.hidden = YES;
            [syncLabel setText:@""];
        }
    }else{
        syncBg.hidden = YES;
        [syncLabel setText:@""];
    }
    
    if(textColor != nil){
        [label setTextColor:[NGREGUtil colorFromHexString:textColor]];
    }
    
    if(backgroundColor != nil){
        [label setBackgroundColor:[NGREGUtil colorFromHexString:backgroundColor]];
        [syncLabel setTextColor:[NGREGUtil colorFromHexString:backgroundColor]];
    }
    
    if(textSize != 0){
        label.font = [UIFont systemFontOfSize:textSize];
    }
    
    if(bgUrl.length > 0){
//        [bgImage setImageWithURL:[NSURL URLWithString:bgUrl] placeholderImage:nil options:SDWebImageRetryFailed];
        [self loadImage:bgImage withUrl:bgUrl andMD5:md5];
        [label setBackgroundColor:[UIColor clearColor]];
    }else{
        if(backgroundColor != nil){
            [label setBackgroundColor:[NGREGUtil colorFromHexString:backgroundColor]];
        }
  
    }
    
    [NGREGUtil setFontFromThisView:label andIdFont:fontId];
    [NGREGUtil setFontBoldFromThisView:syncLabel andIdFont:fontId];
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    if(self.menuSelected)
        self.menuSelected(indexPath.row, self.dataArray[indexPath.row]);
    if(self.delegate)
        [self.delegate bottomPanelViewController:self didSelectMenuAtIndex:indexPath.row WithData:self.dataArray[indexPath.row]];
    
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(  0.0f, [self.dataArray count] == 1 ? kPanelMarginLeftItem : 0.0f, 0.0f, 0.0f);
}


@end
