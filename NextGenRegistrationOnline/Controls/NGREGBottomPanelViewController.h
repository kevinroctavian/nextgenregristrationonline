//
//  NGREGBottomPanelViewController.h
//  NextGenRegistrationOnline
//
//  Created by Kevin Octavian on 1/22/14.
//  Copyright (c) 2014 Kevin R. Octavian. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NGREGGenericViewController.h"

#define kPanelViewVisibleHeight  100.0f
#define kPanelItemViewHeight  50.0f
#define kPanelMarginLeftItem  367.0f

@protocol NGREGBottomPanelViewControllerDelegate;

@interface NGREGBottomPanelViewController : NGREGGenericViewController{

}

@property (strong, nonatomic) NSArray *dataArray;
@property (assign, nonatomic) id <NGREGBottomPanelViewControllerDelegate>delegate;
@property (strong) void (^menuSelected)(int index, id menuSelected);

- (void)setTextSize:(NSInteger)textSizeL andTextColor:(NSString*)textColorL andBackgroundColor:(NSString*)bgColorL andFontId:(NSInteger)fontIdL andBgUrl:(NSString*) bgUrlL andMD5:(NSString*)md5L;
- (void)refresh;

@end


@protocol NGREGBottomPanelViewControllerDelegate <NSObject>
- (void)bottomPanelViewController:(NGREGBottomPanelViewController *)viewController didSelectMenuAtIndex:(NSInteger)index WithData:(id)menu;
@end
