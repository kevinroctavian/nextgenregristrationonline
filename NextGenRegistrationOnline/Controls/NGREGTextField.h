//
//  NGREGTextField.h
//  NextGenRegistrationOnline
//
//  Created by Kevin Octavian on 5/13/14.
//  Copyright (c) 2014 Kevin R. Octavian. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NGREGTextField : UITextField

- (void) setNormalBackgroundColor:(UIColor *)normalColor;
- (void) setDisableBackgroundColor:(UIColor *)disableColor;

@end
