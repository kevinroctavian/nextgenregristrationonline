//
//  NGREGTextField.m
//  NextGenRegistrationOnline
//
//  Created by Kevin Octavian on 5/13/14.
//  Copyright (c) 2014 Kevin R. Octavian. All rights reserved.
//

#import "NGREGTextField.h"

@implementation NGREGTextField{
    UIColor *normalColorBg, *disableColorBg;
}

- (void)setEnabled:(BOOL)enabled
{
    [super setEnabled:enabled];
    
    if(enabled)
        self.backgroundColor = normalColorBg;
    else
        self.backgroundColor = disableColorBg;
    
}


- (void) setNormalBackgroundColor:(UIColor *)normalColor{
    normalColorBg = normalColor;
    self.backgroundColor = normalColorBg;
}

- (void) setDisableBackgroundColor:(UIColor *)disableColor{
    disableColorBg = disableColor;
}

@end
