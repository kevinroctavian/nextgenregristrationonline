//
//  NGREGAppDelegate.m
//  NextGenRegistrationOnline
//
//  Created by Kevin Octavian on 1/16/14.
//  Copyright (c) 2014 Kevin R. Octavian. All rights reserved.
//

#import "NGREGAppDelegate.h"
#import <RestKit/RestKit.h>
#import <RestKit/CoreData.h>
#import "TestFlight.h"
#import "FBEncryptorAES.h"
#import "NGREGHomeViewController.h"
#import "NGREGEngine.h"
#import "NGREGSync.h"
#import "Constant.h"
#import <KSCrash/KSCrash.h>
// Include to use the standard reporter.
#import <KSCrash/KSCrashInstallationStandard.h>
// Include to use Quincy or Hockey.
#import <KSCrash/KSCrashInstallationQuincyHockey.h>
// Include to use the email reporter.
#import <KSCrash/KSCrashInstallationEmail.h>
// Include to use Victory.
#import <KSCrash/KSCrashInstallationVictory.h>
#import "UIAlertView+BlockExtensions.h"
#import <CrashReporter/PLCrashReporter.h>
#import <CrashReporter/PLCrashReport.h>
#import <CrashReporter/PLCrashReportTextFormatter.h>
#import "NGREGError+Additions.h"

@interface NGREGAppDelegate()

@property (strong, nonatomic, readonly) RKManagedObjectStore *managedObjectStore;
@property (nonatomic, strong) UIViewController *homeViewController;

@end

@implementation NGREGAppDelegate

@synthesize managedObjectStore=_managedObjectStore;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    
//    [TestFlight takeOff:@"6ce64585-b983-4c4a-bf7c-53aba8273027"]; // dev
    [TestFlight takeOff:@"60486871-d3da-4cbd-a834-0ff4226c3ede"]; // prod
    
    
    // crash KSCrash
//    KSCrashInstallationEmail* installation = [KSCrashInstallationEmail sharedInstance];
//    installation.recipients = @[@"kevin.r.octavian@gmail.com"];
//    
//    // Optional (Email): Send Apple-style reports instead of JSON
//    [installation setReportStyle:KSCrashEmailReportStyleApple useDefaultFilenameFormat:YES];
//    
//    // Optional: Add an alert confirmation (recommended for email installation)
//    [installation addConditionalAlertWithTitle:@"Crash Detected"
//                                       message:installation.message
//                                     yesAnswer:@"Sure!"
//                                      noAnswer:@"No thanks"];
//    
//    [installation install];
    
    PLCrashReporter *crashReporter = [PLCrashReporter sharedReporter];
    NSError *error;
    
         // Check if we previously crashed
         if ([crashReporter hasPendingCrashReport])
                [self handleCrashReport];
    
         // Enable the Crash Reporter
         if (![crashReporter enableCrashReporterAndReturnError: &error])
                 NSLog(@"Warning: Could not enable crash reporter: %@", error);
    
    return YES;
}

//
// Called to handle a pending crash report.
//
- (void) handleCrashReport {
    PLCrashReporter *crashReporter = [PLCrashReporter sharedReporter];
    NSData *crashData;
    NSError *error;
    
    // Try loading the crash report
    crashData = [crashReporter loadPendingCrashReportDataAndReturnError: &error];
    if (crashData == nil) {
        NSLog(@"Could not load crash report: %@", error);
        [crashReporter purgePendingCrashReport];
    }
    
    // We could send the report from here, but we'll just print out
    // some debugging info instead
    PLCrashReport *report = [[PLCrashReport alloc] initWithData: crashData error: &error] ;
    if (report == nil) {
        NSLog(@"Could not parse crash report");
        [crashReporter purgePendingCrashReport];
    }
    
    NSLog(@"Crashed on %@", report.systemInfo.timestamp);
    NSLog(@"Crashed with signal %@ (code %@, address=0x%" PRIx64 ")", report.signalInfo.name,
          report.signalInfo.code, report.signalInfo.address);
    
    
    NSString  *errorString = [NSString stringWithFormat:@"Path : %@, Crashed with signal %@ (code %@, address=0x%" PRIx64 ")", report.processInfo.processPath, report.signalInfo.name,
                                 report.signalInfo.code, report.signalInfo.address];
    
    if(report.hasExceptionInfo)
        errorString = [NSString stringWithFormat:@"Reaseon : %@, Name : %@, Stack : %@", report.exceptionInfo.exceptionReason, report.exceptionInfo.exceptionName, report.exceptionInfo.stackFrames];
    
    
    NSString *humanReadable = [PLCrashReportTextFormatter stringValueForCrashReport:report withTextFormat:PLCrashReportTextFormatiOS];
    NSLog(@"Report: %@", humanReadable);
    
    if(humanReadable.length > 5000)
        humanReadable = [humanReadable substringToIndex:5000];
    

    
//    NSString* imagesString = [NSString stringWithFormat:@"Images : %@", report.images];
//    
//    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:imagesString completionBlock:^(NSUInteger buttonIndex, UIAlertView *alertView) {
//        
//        if ([[alertView buttonTitleAtIndex:buttonIndex] isEqualToString:@"Yes"]) {
//            [[NGREGEngine sharedEngine] requestLogoutWithCompletionHandler:^(id result, NSError *error) {
//                
//            }];
//            [[NSUserDefaults standardUserDefaults] setValue:@"" forKey:kNGREGEmailNextGen];
//            
//        }
//    } cancelButtonTitle:@"Cancel" otherButtonTitles:@"Yes", nil];
//    [alertView show];
    
    if(humanReadable.length > 0){
    
        // kirim ke server
        [[NGREGEngine sharedEngine] requestCrashReportWithBrand:[[NSUserDefaults standardUserDefaults] valueForKey:kNGREGBrandIDNextGen] andUserId:[[NSUserDefaults standardUserDefaults] valueForKey:kNGREGEmailNextGen] andTime:report.systemInfo.timestamp andReason:report.exceptionInfo.exceptionReason andDetail:humanReadable andCompletionHandler:^(id result, NSError *error) {
        }];
    }
    
    
    // Purge the report
//finishReport:
//    [crashReporter purgePendingCrashReport];
    return;
}

-(void)application:(UIApplication *)application handleEventsForBackgroundURLSession:(NSString *)identifier completionHandler:(void (^)())completionHandler{
    
    self.backgroundTransferCompletionHandler = completionHandler;
    
}


- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    
    NSInteger nilai = [NGREGSync syncsCounterCountWithBrandId:[[NSUserDefaults standardUserDefaults] valueForKey:kNGREGBrandIDNextGen]];
    
    NSLog(@"Data : %d dengan brand %@", nilai,[[NSUserDefaults standardUserDefaults] valueForKey:kNGREGBrandIDNextGen]);
    
    [self saveContext];
    
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    
    NSInteger nilai = [NGREGSync syncsCounterCountWithBrandId:[[NSUserDefaults standardUserDefaults] valueForKey:kNGREGBrandIDNextGen]];
    NSLog(@"Data : %d dengan brand %@", nilai,[[NSUserDefaults standardUserDefaults] valueForKey:kNGREGBrandIDNextGen]);
    
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    NSInteger nilai = [NGREGSync syncsCounterCountWithBrandId:[[NSUserDefaults standardUserDefaults] valueForKey:kNGREGBrandIDNextGen]];
    
    NSLog(@"Data : %d dengan brand %@", nilai,[[NSUserDefaults standardUserDefaults] valueForKey:kNGREGBrandIDNextGen]);
    
    [self saveContext];
    
}

- (BOOL)application:(UIApplication *)application handleOpenURL:(NSURL *)url{
    
    NSString* decrypt = [FBEncryptorAES decryptBase64String:[[[url absoluteString] stringByReplacingOccurrencesOfString:[NSString stringWithFormat:@"%@://", [url scheme]] withString:@""] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding]
                                                  keyString:@"4r53n4L"];
    
    NSError* error;
    NSDictionary* dict =  [NSJSONSerialization JSONObjectWithData:[decrypt dataUsingEncoding:NSUTF8StringEncoding] options:kNilOptions error:&error
                           ];
    
    
    [self setRootViewControllerToHomeViewController];
    
    if([[dict valueForKey:@"menuType"]isEqualToString:@"brosur"]){
        [((NGREGHomeViewController*)self.self.window.rootViewController.childViewControllers[0]) navigateToRegisterRegistrant:[dict valueForKey:@"menuName"]];
    }else{
        [[NGREGEngine sharedEngine] requestTrackGameWithData:dict andCompletionHandler:^(id result, NSError *error) {
            [((NGREGHomeViewController*)self.self.window.rootViewController.childViewControllers[0]) viewDidBecomeActive];
        }];
    }
    return YES;
}

#pragma mark - Getter

- (RKManagedObjectStore *)managedObjectStore {
    
    if (!_managedObjectStore) { 
        
        _managedObjectStore = [[RKObjectManager sharedManager]  managedObjectStore];
    }
    
    return _managedObjectStore;
}

#pragma mark - Helper

- (NSManagedObjectContext *)managedObjectContext {
    
//    return self.managedObjectStore.mainQueueManagedObjectContext;
    return self.managedObjectStore.persistentStoreManagedObjectContext;
}

- (NSManagedObjectContext *)persistentStoreManagedObjectContext {
    
    return self.managedObjectStore.persistentStoreManagedObjectContext;
}

- (void)setRootViewControllerToHomeViewController {
    
    if (!self.homeViewController) {
        
        self.homeViewController = (NGREGHomeViewController*)[self.window.rootViewController.storyboard instantiateViewControllerWithIdentifier:@"RootHome"];
    }
    
    [UIView transitionWithView:self.window
                      duration:0.0
                       options:(UIViewAnimationOptionTransitionCrossDissolve | UIViewAnimationOptionAllowAnimatedContent)
                    animations:^{
                        
                        BOOL oldState = [UIView areAnimationsEnabled];
                        [UIView setAnimationsEnabled:NO];
                        self.window.rootViewController = self.homeViewController;
                        [UIView setAnimationsEnabled:oldState];
                        
                    } completion:nil];
}

- (void)setRootViewControllerToDownloadViewController {
    
    self.homeViewController = nil;
    
    [UIView transitionWithView:self.window
                      duration:0.5
                       options:(UIViewAnimationOptionTransitionCrossDissolve | UIViewAnimationOptionAllowAnimatedContent)
                    animations:^{
                        
                        UIViewController *authenticationViewController = [self.window.rootViewController.storyboard instantiateViewControllerWithIdentifier:@"DownloadViewController"];
                        
                        BOOL oldState = [UIView areAnimationsEnabled];
                        [UIView setAnimationsEnabled:NO];
                        self.window.rootViewController = authenticationViewController;
                        [UIView setAnimationsEnabled:oldState];
                        
                    } completion:nil];
}

- (void)setRootViewControllerToAuthenticationViewController {
    
    self.homeViewController = nil;
    
    [UIView transitionWithView:self.window
                      duration:0.5
                       options:(UIViewAnimationOptionTransitionCrossDissolve | UIViewAnimationOptionAllowAnimatedContent)
                    animations:^{
                        
                        UIViewController *authenticationViewController = [self.window.rootViewController.storyboard instantiateViewControllerWithIdentifier:@"Login"];
                        
                        BOOL oldState = [UIView areAnimationsEnabled];
                        [UIView setAnimationsEnabled:NO];
                        self.window.rootViewController = authenticationViewController;
                        [UIView setAnimationsEnabled:oldState];
                        
                    } completion:nil];
}

- (void)saveContext
{
    NSError *error = nil;
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil)
    {
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error])
        {
            /*
             Replace this implementation with code to handle the error appropriately.
             
             abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
             */
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
    }
}

@end
