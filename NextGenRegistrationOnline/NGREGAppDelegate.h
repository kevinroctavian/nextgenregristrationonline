//
//  NGREGAppDelegate.h
//  NextGenRegistrationOnline
//
//  Created by Kevin Octavian on 1/16/14.
//  Copyright (c) 2014 Kevin R. Octavian. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NGREGAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

- (NSManagedObjectContext *)managedObjectContext;
- (NSManagedObjectContext *)persistentStoreManagedObjectContext;

- (void)setRootViewControllerToHomeViewController;
- (void)setRootViewControllerToDownloadViewController;
- (void)setRootViewControllerToAuthenticationViewController;

@property (nonatomic, copy) void(^backgroundTransferCompletionHandler)();

@end
