//
//  Constant.h
//  NextGenRegistrationOnline
//
//  Created by Kevin Octavian on 4/7/14.
//  Copyright (c) 2014 Kevin R. Octavian. All rights reserved.
//
//
//#define kBEATRegisterDeviceFormatString @"<?xml version=\"1.0\" encoding=\"utf-8\"?><soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\"><soap:Header><SecurityToken xmlns=\"http://tempuri.org/\" ><token>%@</token></SecurityToken></soap:Header><soap:Body><AdminRegisterDevice xmlns=\"http://tempuri.org/\"><userName>%@</userName><password>%@</password><deviceId>%@</deviceId><deviceDescription>%@</deviceDescription><serialNumber>%@</serialNumber></AdminRegisterDevice></soap:Body></soap:Envelope>"


#define kBEATRegisterDeviceFormatString @"<?xml version=\"1.0\" encoding=\"utf-8\"?><soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\"><soap:Header><SecurityToken xmlns=\"http://tempuri.org/\" ><token>%@</token></SecurityToken></soap:Header><soap:Body><UserRegisterDevice xmlns=\"http://tempuri.org/\"><userName>%@</userName><password>%@</password><deviceId>%@</deviceId><deviceDescription>%@</deviceDescription><serialNumber>%@</serialNumber></UserRegisterDevice></soap:Body></soap:Envelope>"

#define kBEATSearchEntourageByEmailFormatString @"<ProfileCollection><Profile ID=\"\" RegistrationID=\"\"><FieldCollection><Field Name=\"Email\"><Value>%@</Value></Field></FieldCollection></Profile></ProfileCollection>"

#define kBEATSearchEntourageByGIIDNumberFormatString @"<ProfileCollection><Profile GIIDNumber=\"%@\" ResponseCode=\"\" ></Profile></ProfileCollection>"

#define kBEATSecurityToken              @"kBEATSecurityToken"


#define kNGREGEmailNextGen @"emailNextGen"
#define kNGREGPasswordNextGen @"passwordNextGen"
#define kNGREGBrandIDNextGen @"brandIdNextGen"
#define kNGREGRegisterDevice @"registerDevice"
#define kNGREGUsernameMOP @"usernameMop"
#define kNGREGPasswordMOP @"passwordMop"

