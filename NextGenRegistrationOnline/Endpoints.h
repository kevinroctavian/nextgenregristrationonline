//
//  Endpoints.h
//  NextGenRegistrationOnline
//
//  Created by Kevin Octavian on 1/16/14.
//  Copyright (c) 2014 Kevin R. Octavian. All rights reserved.
//

#ifndef NextGenRegistrationOnline_Endpoints_h
#define NextGenRegistrationOnline_Endpoints_h

// dev
//#define NGREG_API_BASE_URL               @"https://nextgen-dev.ba-space.com"
//#define BEAT_AWS_MOP_ADMIN              @"https://staging-aws-mop-id.es-dm.com/dm.mop.admin.webservice/centralAdminwebservice.asmx"
//#define BEAT_API_BASE_URL               @"https://beat-dev.ba-space.com"

//prod
#define NGREG_API_BASE_URL               @"https://nextgen.ba-space.com"
#define BEAT_AWS_MOP_ADMIN              @"https://login.ba-space.com/dm.mop.admin.webservice/centraladminwebservice.asmx"
//#define BEAT_API_BASE_URL               @"https://api.ba-space.com"

#define BEAT_API_MOP_SECRET_TOKEN       @"/service/mop/secrettoken"
#define BEAT_API_MOP_LOGIN              @"/service/mop/login"

#define BEAT_AWS_MOP_SEARCH_ENTOURAGE_BY_EMAIL          @"/AdminSearchProfile"
#define BEAT_AWS_MOP_SEARCH_ENTOURAGE_BY_GIID           @"/AdminGetProfileOnGIID"


//#define NGREG_API_LOGIN_ACCOUNT          @"/service/login/beat/"
#define NGREG_API_LOGIN_ACCOUNT          @"/service/login/mop/"
#define NGREG_API_BRAND_LIST             @"/service/login/brandlist"
#define NGREG_API_BRAND_THEME             @"/service/login/brandtheme"
#define NGREG_API_MY_PROFILE             @"/service/my/profile"
#define NGREG_API_REGISTRANT_CHECK_EMAIL             @"/service/registrant/checkemail"
#define NGREG_API_REGISTRANT_CHECK_GIIDNUMBER             @"/service/registrant/checkgiid"
#define NGREG_API_REGISTRANT_REGISTER             @"/service/registrant/register"
#define NGREG_API_REGISTRANT_REPORT             @"/service/registrant/chart"

#define NGREG_API_REGISTRANT_GET_CITY             @"/service/registrant/getCity"
#define NGREG_API_REGISTRANT_GET_SUB_BRANDS             @"/service/registrant/getBrandPref"


// tracking
#define NGREG_API_TRACK_ACTIVITY             @"/service/track/activity"
#define NGREG_API_TRACK_GAMES             @"/service/track/games"

// crash report
#define NGREG_API_CRASH_REPORT             @"/service/report/addloggingerror/"

#define NGREG_API_LOGOUT            @"/service/logout/forces"

#endif

