//
//  NGREGRegisterDeviceParser.m
//  NextGenRegistrationOnline
//
//  Created by Kevin Octavian on 4/10/14.
//  Copyright (c) 2014 Kevin R. Octavian. All rights reserved.
//

#import "NGREGRegisterDeviceParser.h"

@implementation NGREGRegisterDeviceParser{
    NSString* currentElementValue;
    NSString* currentElement;
    bool found;
}

- (BOOL)parseXMLToResultFromRegisterDeviceData:(NSData*)data withCompleteion:(void (^)(bool))giidBlock{
    self.successBlock = giidBlock;
    NSXMLParser* parser = [[NSXMLParser alloc]initWithData:data];
    parser.delegate = self;
    return [parser parse];
}

#pragma mark - NSXMLParserDelegate

- (void) parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict{
    
    if([elementName isEqualToString:@"Field"])
        currentElement = [attributeDict objectForKey:@"Name"];
}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string {
    
    currentElementValue = string;
    
}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName{
    
    NSLog(@"currentElement = %@ & currentElementValue = %@ & elementName=%@",currentElement, currentElementValue, elementName);
    if([elementName isEqualToString:@"Result"] && !found){
        //1,9,10 dan 11 - UserRegisterDevice
//        if([currentElementValue isEqualToString:@"1"] || [currentElementValue isEqualToString:@"9"] || [currentElementValue isEqualToString:@"10"] || [currentElementValue isEqualToString:@"11"])
            // 1,4,9 & 10  - AdminRegisterDevice
        if([currentElementValue isEqualToString:@"4"] || [currentElementValue isEqualToString:@"1"] || [currentElementValue isEqualToString:@"9"] || [currentElementValue isEqualToString:@"10"])
            found = YES;
//            self.successBlock(found);
            return;
    }
    
    if([elementName isEqualToString:@"Value"]){
        currentElementValue = [currentElementValue stringByTrimmingCharactersInSet:
                               [NSCharacterSet whitespaceCharacterSet]];
        
        
    }
    
}

- (void)parserDidEndDocument:(NSXMLParser *)parser{
    self.successBlock(found);
}

@end
