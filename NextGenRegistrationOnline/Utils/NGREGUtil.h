//
//  NGREGUtil.h
//  NextGenRegistrationOnline
//
//  Created by Kevin Octavian on 1/27/14.
//  Copyright (c) 2014 Kevin R. Octavian. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NGREGUtil : NSObject

+ (UIColor *)colorFromHexString:(NSString *)hexString;
+ (BOOL)validateEmailWithString:(NSString*)email;
+ (UIImage*)getUIImageFromThisUIView:(UIView*)aUIView;

//font
+ (void)setFontFromThisView:(UIView*)label andIdFont:(NSInteger) fontId;
+ (void)setFontBoldFromThisView:(UIView*)label andIdFont:(NSInteger) fontId;

@end
