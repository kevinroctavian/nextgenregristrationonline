//
//  NGREGUtil.m
//  NextGenRegistrationOnline
//
//  Created by Kevin Octavian on 1/27/14.
//  Copyright (c) 2014 Kevin R. Octavian. All rights reserved.
//

#import "NGREGUtil.h"


@implementation NGREGUtil

+ (UIColor *)colorFromHexString:(NSString *)hexString {
    unsigned rgbValue = 0;
    NSScanner *scanner = [NSScanner scannerWithString:hexString];
    [scanner setScanLocation:1]; // bypass '#' character
    [scanner scanHexInt:&rgbValue];
    return [UIColor colorWithRed:((rgbValue & 0xFF0000) >> 16)/255.0 green:((rgbValue & 0xFF00) >> 8)/255.0 blue:(rgbValue & 0xFF)/255.0 alpha:1.0];
}

+ (BOOL)validateEmailWithString:(NSString*)email
{
    if([email rangeOfString:@"@."].location != NSNotFound || [email rangeOfString:@".."].location != NSNotFound || [email rangeOfString:@".@"].location != NSNotFound)
        return false;
    
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:email];
}

+ (UIImage*)getUIImageFromThisUIView:(UIView*)aUIView
{
    UIGraphicsBeginImageContext(aUIView.bounds.size);
    [aUIView.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *viewImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return viewImage;
}

+ (void)setFontFromThisView:(UIView*)label andIdFont:(NSInteger) fontId
{
    
    NSString* fontName = @"BebasNeue";
    switch (fontId) {
        case 99 :
            fontName = @"Muli-Light" ;
            break;
        case 1:
            fontName = @"TitilliumText22L-Regular" ;
            break;
        case 2:
            fontName = @"TimesNewRomanPSMT";
            break;
        case 3:
            fontName = @"Calibri" ;
            break;
        case 4:
            fontName = @"ArialMT" ;
            break;
        case 5:
            fontName = @"BebasNeue";
            break;
        case 6:
            fontName = @"Bitsumishi";
            break;
        default:
            fontName = @"BebasNeue";
            break;
            
    }
    if([label isKindOfClass:[UILabel class]]){
        [((UILabel*)label) setFont:[UIFont fontWithName:fontName size:((UILabel*)label).font.pointSize]];
    }
    if([label isKindOfClass:[UIButton class]]){
        [ ((UIButton*)label).titleLabel setFont:[UIFont fontWithName:fontName size:((UIButton*)label).titleLabel.font.pointSize]];
    }
    if([label isKindOfClass:[UITextField class]]){
        [ ((UITextField*)label) setFont:[UIFont fontWithName:fontName size:((UITextField*)label).font.pointSize]];
    }
    if([label isKindOfClass:[UITextView class]]){
        [ ((UITextView*)label) setFont:[UIFont fontWithName:fontName size:((UITextView*)label).font.pointSize]];
    }
}

+ (void)setFontBoldFromThisView:(UIView*)label andIdFont:(NSInteger) fontId
{
    
    NSString* fontName = @"BebasNeue";
    switch (fontId) {
        case 99 :
            fontName = @"Muli" ;
            break;
        case 1:
            fontName = @"TitilliumText22L-Bold" ;
            break;
        case 2:
            fontName = @"TimesNewRomanPS-BoldMT";
            break;
        case 3:
            fontName = @"Calibri-Bold" ;
            break;
        case 4:
            fontName = @"Arial-BoldMT" ;
            break;
        case 5:
            fontName = @"BebasNeue";
            break;
        case 6:
            fontName = @"Bitsumishi";
            break;
        default:
            fontName = @"BebasNeue";
            break;
            
    }
    if([label isKindOfClass:[UILabel class]]){
        [((UILabel*)label) setFont:[UIFont fontWithName:fontName size:((UILabel*)label).font.pointSize]];
    }
    if([label isKindOfClass:[UIButton class]]){
       [ ((UIButton*)label).titleLabel setFont:[UIFont fontWithName:fontName size:((UIButton*)label).titleLabel.font.pointSize]];
    }
    if([label isKindOfClass:[UITextField class]]){
        [ ((UITextField*)label) setFont:[UIFont fontWithName:fontName size:((UITextField*)label).font.pointSize]];
    }
    if([label isKindOfClass:[UITextView class]]){
        [ ((UITextView*)label) setFont:[UIFont fontWithName:fontName size:((UITextView*)label).font.pointSize]];
    }
    
}


@end
