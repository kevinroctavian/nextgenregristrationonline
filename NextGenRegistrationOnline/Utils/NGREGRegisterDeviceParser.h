//
//  NGREGRegisterDeviceParser.h
//  NextGenRegistrationOnline
//
//  Created by Kevin Octavian on 4/10/14.
//  Copyright (c) 2014 Kevin R. Octavian. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NGREGRegisterDeviceParser : NSObject <NSXMLParserDelegate>


@property (strong) void (^successBlock)(bool);

- (BOOL)parseXMLToResultFromRegisterDeviceData:(NSData*)data withCompleteion:(void (^)(bool))giidBlock;

@end
