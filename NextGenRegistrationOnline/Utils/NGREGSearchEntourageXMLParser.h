//
//  NGREGSearchEntourageXMLParser.h
//  NextGenRegistrationOnline
//
//  Created by Kevin Octavian on 4/10/14.
//  Copyright (c) 2014 Kevin R. Octavian. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NGREGCheckRegistrantResponse.h"

@interface NGREGSearchEntourageXMLParser : NSObject <NSXMLParserDelegate>

@property (strong) void (^giidBlock)(NSString*);
@property (strong) void (^entourageBlock)(NGREGCheckRegistrantResponse*);

- (BOOL)parseXMLToGetGIIDNumberFromData:(NSData*)data withCompleteion:(void (^)(NSString*))giidBlock;
- (BOOL)parseXMLToGetEntourageFromData:(NSData*)data withCompleteion:(void (^)(NGREGCheckRegistrantResponse*))entourageBlock;

@end