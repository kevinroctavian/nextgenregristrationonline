//
//  NGREGSearchEntourageXMLParser.m
//  NextGenRegistrationOnline
//
//  Created by Kevin Octavian on 4/10/14.
//  Copyright (c) 2014 Kevin R. Octavian. All rights reserved.
//

#import "NGREGSearchEntourageXMLParser.h"

@implementation NGREGSearchEntourageXMLParser{
    
    NSString* currentElementValue;
    NSString* currentElement;
    
    BOOL parsing;
    BOOL searchingGIID;
    
    NGREGCheckRegistrantResponse* entourage;
}



@synthesize giidBlock = _giidBlock;
@synthesize entourageBlock = _entourageBlock;

- (BOOL)parseXMLToGetGIIDNumberFromData:(NSData*)data withCompleteion:(void (^)(NSString*))giidBlock {
    self.giidBlock = giidBlock;
    searchingGIID = true;
    
    NSXMLParser* parser = [[NSXMLParser alloc]initWithData:data];
    parser.delegate = self;
    return [parser parse];
}

- (BOOL)parseXMLToGetEntourageFromData:(NSData*)data withCompleteion:(void (^)(NGREGCheckRegistrantResponse*))entourageBlock {
    self.entourageBlock = entourageBlock;
    searchingGIID = false;
    entourage = [[NGREGCheckRegistrantResponse alloc] init];
    
    NSXMLParser* parser = [[NSXMLParser alloc]initWithData:data];
    parser.delegate = self;
    return [parser parse];
}

#pragma mark - NSXMLParserDelegate

- (void) parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict{
    
    if([elementName isEqualToString:@"Field"])
        currentElement = [attributeDict objectForKey:@"Name"];
}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string {
    
    currentElementValue = string;
    
}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName{
    
    //    NSLog(@"currentElement = %@ & currentElementValue = %@ & elementName=%@",currentElement, currentElementValue, elementName);
    if([elementName isEqualToString:@"Result"] && (![currentElementValue isEqualToString:@"1"] && !parsing)){
        if(self.giidBlock!=nil){
            self.giidBlock(@"");
            parsing = true;
            return;
        }
    }
    
    if([elementName isEqualToString:@"Value"]){
        currentElementValue = [currentElementValue stringByTrimmingCharactersInSet:
                               [NSCharacterSet whitespaceCharacterSet]];
        
        if(searchingGIID && !parsing && [currentElement isEqualToString:@"GIIDNumber"]){
            self.giidBlock(currentElementValue);
            parsing = true;
        }else{
            if([currentElement isEqualToString:@"GIIDNumber"]){
                entourage.gIIDNumber = currentElementValue;
            }
            if([currentElement isEqualToString:@"RegistrationID1"]){
                entourage.registrationID = currentElementValue;
            }
            if([currentElement isEqualToString:@"Email"]){
                entourage.email = currentElementValue;
            }
            if([currentElement isEqualToString:@"ConsumerID"]){
                entourage.email = currentElementValue;
            }
            if([currentElement isEqualToString:@"Gender"]){
                entourage.sex = currentElementValue;
            }
            if([currentElement isEqualToString:@"OtherName"]){
                //                entourage.nickname = currentElementValue;
            }
            if([currentElement isEqualToString:@"FirstName"]){
                entourage.fullName = currentElementValue;
            }
            if([currentElement isEqualToString:@"LastName"]){
                entourage.lastName = currentElementValue;
            }
            if([currentElement isEqualToString:@"DateOfBirth"]){
                entourage.dateOfBirth = currentElementValue;
            }
            if([currentElement isEqualToString:@"GIIDType"]){
                entourage.gIIDType = currentElementValue;
            }
            if([currentElement isEqualToString:@"SpareText2"]){
                entourage.socialAccount = [[currentElementValue stringByTrimmingCharactersInSet:
                                            [NSCharacterSet whitespaceCharacterSet]] stringByReplacingOccurrencesOfString:@"\n" withString:@""];
            }
            if([currentElement isEqualToString:@"SpareText8"]){
                //                entourage.community = currentElementValue;
            }
            if([currentElement isEqualToString:@"MobilePhoneNumber"]){
                entourage.mobile = currentElementValue;
            }
            if([currentElement isEqualToString:@"SubBrand1_ID"]){
                entourage.brand1Id = [currentElementValue stringByTrimmingCharactersInSet:
                                      [NSCharacterSet whitespaceCharacterSet]];
            }
            if([currentElement isEqualToString:@"SubBrand2_ID"]){
                //                entourage.brand1UId = currentElementValue;
            }
            if([currentElement isEqualToString:@"CityID"]){
                entourage.city = currentElementValue;
            }
        }
    }
    
}

- (void)parserDidEndDocument:(NSXMLParser *)parser{
    if(!searchingGIID)
        self.entourageBlock(entourage);
    if(!parsing && self.giidBlock != nil)
        self.giidBlock(@"");
    
}


@end
