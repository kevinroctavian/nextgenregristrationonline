//
//  UIDevice+Additions.h
//  BEAT-Universal
//
//  Created by Bayu Wy on 10/6/13.
//  Copyright (c) 2013 bayuwy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <UIKit/UIDevice.h>

@interface UIDevice (Additions)

/*
 * @method uniqueDeviceIdentifier
 * @description use this method when you need a unique identifier in one app.
 * It generates a hash from the MAC-address in combination with the bundle identifier
 * of your app.
 */

- (NSString *)uniqueDeviceIdentifier;

/*
 * @method uniqueGlobalDeviceIdentifier
 * @description use this method when you need a unique global identifier to track a device
 * with multiple apps. as example a advertising network will use this method to track the device
 * from different apps.
 * It generates a hash from the MAC-address only.
 */

- (NSString *)uniqueGlobalDeviceIdentifier;

@property (nonatomic, readonly) NSString *serialNumber;

@end
