//
//  NSData+MD5.h
//  NextGenRegistrationOnline
//
//  Created by Kevin Octavian on 10/15/14.
//  Copyright (c) 2014 Kevin R. Octavian. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSData (MD5)


- (NSString *)MD5;

@end
