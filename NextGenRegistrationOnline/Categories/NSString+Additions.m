//
//  NSString+Additions.m
//  BEAT-Universal
//
//  Created by Bayu Wy on 10/12/13.
//  Copyright (c) 2013 bayuwy. All rights reserved.
//

#import "NSString+Additions.h"
#import <CommonCrypto/CommonCrypto.h>

@implementation NSString (Additions)

- (NSString *)md5String {
    
    const char *cStr = [self UTF8String];
    unsigned char digest[CC_MD5_DIGEST_LENGTH];
    CC_MD5(cStr, strlen(cStr), digest);
    
    NSMutableString *output = [NSMutableString stringWithCapacity:CC_MD5_DIGEST_LENGTH * 2];
    for(int i = 0; i < CC_MD5_DIGEST_LENGTH; i++) {
        [output appendFormat:@"%02x", digest[i]];
    }
    
    return  output;
}

- (BOOL)emailStringValid {
    
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    
    return [emailTest evaluateWithObject:self];
}

- (BOOL)passwordStringValid:(NSString **)message {
    
    if ([self length] < 8) {
        *message = @"Password must 8 character or more";
        return NO;
    }
    if ([self rangeOfCharacterFromSet:[NSCharacterSet uppercaseLetterCharacterSet]].location == NSNotFound) {
        *message = @"Password must containts uppercase letter character";
        return NO;
    }
    if ([self rangeOfCharacterFromSet:[NSCharacterSet decimalDigitCharacterSet]].location == NSNotFound) {
        *message = @"Password must containts decimal digit character";
    }
    
    return YES;
}

@end
