//
//  UIView+Additions.h
//  BEAT-Universal
//
//  Created by Bayu Wy on 10/12/13.
//  Copyright (c) 2013 bayuwy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (Additions)

- (void)setX:(CGFloat)x;
- (CGFloat)x;
- (void)setY:(CGFloat)y;
- (CGFloat)y;
- (void)setWidth:(CGFloat)width;
- (CGFloat)width;
- (void)setHeight:(CGFloat)height;
- (CGFloat)height;

@end
