//
//  UIView+FindUIViewController.h
//  BEAT-Universal
//
//  Created by Bayu Wy on 1/9/14.
//  Copyright (c) 2014 bayuwy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (FindUIViewController)

- (UIViewController *)firstAvailableUIViewController;
- (id)traverseResponderChainForUIViewController;

@end
