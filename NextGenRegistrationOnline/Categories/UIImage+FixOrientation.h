//
//  UIImage+FixOrientation.h
//  BEAT-Universal
//
//  Created by Esa Firman on 7/18/13.
//  Copyright (c) 2013 bayuwy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (FixOrientation)
- (UIImage *)fixOrientation;
@end
