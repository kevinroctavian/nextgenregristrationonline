//
//  NSString+Additions.h
//  BEAT-Universal
//
//  Created by Bayu Wy on 10/12/13.
//  Copyright (c) 2013 bayuwy. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Additions)
- (NSString *)md5String;
- (BOOL)emailStringValid;
- (BOOL)passwordStringValid:(NSString **)message;

@end
