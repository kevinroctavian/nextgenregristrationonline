//
//  UIViewController+Container.m
//  BEAT-Universal
//
//  Created by Bayu Wy on 10/9/13.
//  Copyright (c) 2013 bayuwy. All rights reserved.
//

#import "UIViewController+Container.h"

@implementation UIViewController (Container)

- (void)addChildViewController:(UIViewController *)childViewController withContainerView:(UIView *)view {
    
    CGRect frame = view.frame;
    frame.origin.x = 0.0f;
    frame.origin.y = 0.0f;
    childViewController.view.frame = frame;
    
    UIViewController *fromViewController = [self.childViewControllers lastObject];
    if (fromViewController) {
        
        [self addChildViewController:childViewController];
        [fromViewController willMoveToParentViewController:nil];
        
        [self transitionFromViewController:fromViewController
                          toViewController:childViewController
                                  duration:0.25f
                                   options:UIViewAnimationOptionTransitionCrossDissolve
                                animations:nil
                                completion:^(BOOL finished) {
                                    [fromViewController.view removeFromSuperview];
                                    [fromViewController removeFromParentViewController];
                                    [childViewController didMoveToParentViewController:self];
                                }];
    } else {
        
        [self addChildViewController:childViewController];
        [view addSubview:childViewController.view];
        [childViewController didMoveToParentViewController:self];
    }
}

@end
