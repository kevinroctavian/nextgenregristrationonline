//
//  UIViewController+Container.h
//  BEAT-Universal
//
//  Created by Bayu Wy on 10/9/13.
//  Copyright (c) 2013 bayuwy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (Container)

- (void)addChildViewController:(UIViewController *)childViewController withContainerView:(UIView *)view;

@end
