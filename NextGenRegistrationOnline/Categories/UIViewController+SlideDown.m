//
//  UIViewController+SlideDown.m
//  BEAT-Universal
//
//  Created by Bayu Wy on 1/9/14.
//  Copyright (c) 2014 bayuwy. All rights reserved.
//

#import "UIViewController+SlideDown.h"
#import <QuartzCore/QuartzCore.h>
#import "UIView+FindUIViewController.h"

#define kOverlayViewTag         7147
#define kSlideDownViewTag       (7147 +2)
#define kSlideUpViewTag         (7147 +3)
#define kSlideLeftViewTag       (7147 +5)
#define kSlideRightViewTag      (7147 +7)
#define kSlideFadeViewTag       (7147 +11)

@implementation UIViewController (SlideDown)

- (void)slideViewController:(UIViewController *)controller inView:(UIView *)view targetRect:(CGRect)targetRect fromPosition:(SlideViewControllerPosition)position completion:(SlideDownCompletionHandler)block {
    
    // Add shadow
    controller.view.layer.masksToBounds = YES;
    controller.view.layer.cornerRadius = 5.0f;
//    controller.view.layer.shadowOffset = CGSizeMake(5.0f, 5.0f);
//    controller.view.layer.shadowRadius = 8.0f;
//    controller.view.layer.shadowOpacity = 0.8f;
    
    // Add semi overlay
    UIView * overlay = [[UIView alloc] initWithFrame:view.bounds];
    overlay.backgroundColor = [UIColor blackColor];
    overlay.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    overlay.alpha = 0.0f;
    overlay.tag = kOverlayViewTag;
    
    // Dismiss button
    // Don't use UITapGestureRecognizer to avoid complex handling
    UIButton * dismissButton = [UIButton buttonWithType:UIButtonTypeCustom];
    dismissButton.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    [dismissButton addTarget:self action:@selector(dismissButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    //dismissButton.layer.borderColor = [UIColor redColor].CGColor;
    //dismissButton.layer.borderWidth = 1.0f;
    dismissButton.backgroundColor = [UIColor clearColor];
    dismissButton.frame = view.bounds;
    [overlay addSubview:dismissButton];
    [view addSubview:overlay];
    
    CGRect fromRect = CGRectZero;
    if (position == SlideViewControllerPositionTop) {
        fromRect = CGRectMake(targetRect.origin.x, -targetRect.size.height, targetRect.size.width, targetRect.size.height);
        controller.view.tag = kSlideDownViewTag;
    }
    else if (position == SlideViewControllerPositionRight) {
        fromRect = CGRectMake(view.frame.size.width, targetRect.origin.y, targetRect.size.width, targetRect.size.height);
        controller.view.tag = kSlideLeftViewTag;
    }
    else if (position == SlideViewControllerPositionLeft) {
        fromRect = CGRectMake(-targetRect.size.height, targetRect.origin.y, targetRect.size.width, targetRect.size.height);
        controller.view.tag = kSlideRightViewTag;
    }
    else if (position == SlideViewControllerPositionBottom) {
        fromRect = CGRectMake(targetRect.origin.x, view.bounds.size.height +targetRect.size.height, targetRect.size.width, targetRect.size.height);
        controller.view.tag = kSlideUpViewTag;
    }
    else {
        fromRect = targetRect;
        controller.view.tag = kSlideFadeViewTag;
    }
    
    controller.view.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    controller.view.frame = fromRect;
    controller.view.alpha = 0.0f;
    
    [self addChildViewController:controller];
    [view addSubview:controller.view];
    [controller didMoveToParentViewController:self];
    
    [UIView animateWithDuration:0.1f animations:^{
        
        if (!(position == SlideViewControllerPositionNone)) {
            controller.view.alpha = 1.0f;
        }
        
    } completion:^(BOOL finished) {
       
        [UIView animateWithDuration:0.3f animations:^{
            
            if (position == SlideViewControllerPositionNone) {
                controller.view.alpha = 1.0f;
            }
            
            controller.view.frame = targetRect;
            overlay.alpha = 0.5f;
            
        } completion:^(BOOL finished) {
            
            if (block) {
                block();
            }
        }];
    }];
}

- (void)dismissButtonClicked:(UIButton *)sender {
    
    SlideViewControllerPosition position;
    
    UIView *view = [self.view viewWithTag:kSlideDownViewTag];
    position = SlideViewControllerPositionTop;
    if (!view) {
        view = [self.view viewWithTag:kSlideUpViewTag];
        position = SlideViewControllerPositionBottom;
        if (!view) {
            view = [self.view viewWithTag:kSlideLeftViewTag];
            position = SlideViewControllerPositionRight;
            if (!view) {
                view = [self.view viewWithTag:kSlideRightViewTag];
                position = SlideViewControllerPositionLeft;
                if (!view) {
                    view = [self.view viewWithTag:kSlideFadeViewTag];
                    position = SlideViewControllerPositionNone;
                }
            }
        }
    }
    
    [self dismissSlideViewControllerToPosition:position completion:nil];
}

- (void)dismissSlideViewControllerToPosition:(SlideViewControllerPosition)position completion:(SlideDownCompletionHandler)block {
    
    UIView *overlay = [self.view viewWithTag:kOverlayViewTag];
    UIView *view = [self.view viewWithTag:kSlideDownViewTag];
    if (!view) {
        view = [self.view viewWithTag:kSlideUpViewTag];
        if (!view) {
            view = [self.view viewWithTag:kSlideLeftViewTag];
            if (!view) {
                view = [self.view viewWithTag:kSlideRightViewTag];
                if (!view) {
                    view = [self.view viewWithTag:kSlideFadeViewTag];
                }
            }
        }
    }
    
    CGRect targetRect = view.frame;
    if (position == SlideViewControllerPositionTop) {
        targetRect = CGRectMake(targetRect.origin.x, -targetRect.size.height, targetRect.size.width, targetRect.size.height);
    }
    else if (position == SlideViewControllerPositionRight) {
        targetRect = CGRectMake(view.frame.size.width, targetRect.origin.y, targetRect.size.width, targetRect.size.height);
    }
    else if (position == SlideViewControllerPositionLeft) {
        targetRect = CGRectMake(-targetRect.size.height, targetRect.origin.y, targetRect.size.width, targetRect.size.height);
    }
    else if (position == SlideViewControllerPositionBottom) {
        targetRect = CGRectMake(targetRect.origin.x, view.bounds.size.height +targetRect.size.height, targetRect.size.width, targetRect.size.height);
    }
    
    [UIView animateWithDuration:0.3f animations:^{
        
        if (position == SlideViewControllerPositionNone) {
            view.alpha = 0.1;
        }
        
        view.frame = targetRect;
        overlay.alpha = 0.0f;
        
    } completion:^(BOOL finished) {
      
       [UIView animateWithDuration:0.1f animations:^{
           
           if (!(position == SlideViewControllerPositionNone)) {
               view.alpha = 0.1;
           }
           
       } completion:^(BOOL finished) {
          
           UIViewController *controller = [view firstAvailableUIViewController];
           [controller willMoveToParentViewController:nil];
           [controller.view removeFromSuperview];
           [controller removeFromParentViewController];
           
           [overlay removeFromSuperview];
           
           if (block) {
               block();
           }
       }];
    }];
}

@end
