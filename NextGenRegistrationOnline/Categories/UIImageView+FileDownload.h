//
//  UIImageView+FileDownload.h
//  NextGenRegistrationOnline
//
//  Created by Kevin Octavian on 10/15/14.
//  Copyright (c) 2014 Kevin R. Octavian. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImageView (FileDownload)

- (void) loadImage:(NSString*)url andMD5:(NSString*) md5;

@end
