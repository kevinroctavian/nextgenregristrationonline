//
//  UIImageView+FileDownload.m
//  NextGenRegistrationOnline
//
//  Created by Kevin Octavian on 10/15/14.
//  Copyright (c) 2014 Kevin R. Octavian. All rights reserved.
//

#import "UIImageView+FileDownload.h"
#import "NSData+MD5.h"
#import "FileDownloadInfo.h"
#import "NGREGAppDelegate.h"

@interface UIImageView ()<NSURLSessionDelegate>

@property (nonatomic, strong) NSURL *docDirectoryURL;
@property (nonatomic, strong) NSURLSession *session;
@property (nonatomic, strong) FileDownloadInfo *fdi;

@end

@implementation UIImageView (FileDownload)

- (void) loadImage:(NSString*)url andMD5:(NSString*) md5{
    
    NSURLSessionConfiguration *sessionConfiguration = [NSURLSessionConfiguration backgroundSessionConfiguration:@"com.kana.ios.Touchbase.download"];
    sessionConfiguration.HTTPMaximumConnectionsPerHost = 5;
    
    self.session = [NSURLSession sessionWithConfiguration:sessionConfiguration
                                                 delegate:self
                                            delegateQueue:nil];
    
    NSArray *URLs = [[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask];
    self.docDirectoryURL = [URLs objectAtIndex:0];
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    NSArray *parts = [url componentsSeparatedByString:@"/"];
    NSString *filename = [parts objectAtIndex:[parts count]-1];
    NSURL *destinationURL = [self.docDirectoryURL URLByAppendingPathComponent:filename];
    
    if ([fileManager fileExistsAtPath:[destinationURL path]]) {
        // load image dari file
        self.image = [[UIImage alloc] initWithContentsOfFile:[destinationURL path]];
        
        NSData *nsData = [NSData dataWithContentsOfFile:[destinationURL path]];
        if (nsData){
            NSString* md5File = [nsData MD5];
            NSLog(@"MD5-nya adalah %@", md5File);
            if(![md5File isEqualToString:md5]){
                // download yang terbaru
                [self download:url];
            }
        }
    }else{
        // download file
        [self download:url];
    }
    
}

- (void)download:(NSString*)url{
    self.fdi = [[FileDownloadInfo alloc] initWithFileTitle:@"image" andDownloadSource:url];
    if (self.fdi.taskIdentifier == -1) {
        // If the taskIdentifier property of the fdi object has value -1, then create a new task
        // providing the appropriate URL as the download source.
        self.fdi.downloadTask = [self.session downloadTaskWithURL:[NSURL URLWithString:self.fdi.downloadSource]];
        
        // Keep the new task identifier.
        self.fdi.taskIdentifier = self.fdi.downloadTask.taskIdentifier;
        
        // Start the task.
        [self.fdi.downloadTask resume];
    }
    else{
        // Create a new download task, which will use the stored resume data.
        self.fdi.downloadTask = [self.session downloadTaskWithResumeData:self.fdi.taskResumeData];
        [self.fdi.downloadTask resume];
        
        // Keep the new download task identifier.
        self.fdi.taskIdentifier = self.fdi.downloadTask.taskIdentifier;
    }
    self.fdi.isDownloading = !self.fdi.isDownloading;
}

#pragma mark - NSURLSession Delegate method implementation

-(void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask didFinishDownloadingToURL:(NSURL *)location{
    
    NSError *error;
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    NSString *destinationFilename = downloadTask.originalRequest.URL.lastPathComponent;
    NSURL *destinationURL = [self.docDirectoryURL URLByAppendingPathComponent:destinationFilename];
    
    if ([fileManager fileExistsAtPath:[destinationURL path]]) {
        [fileManager removeItemAtURL:destinationURL error:nil];
    }
    
    BOOL success = [fileManager copyItemAtURL:location
                                        toURL:destinationURL
                                        error:&error];
    
    if (success) {
        // Change the flag values of the respective FileDownloadInfo object.
        
        self.fdi.isDownloading = NO;
        self.fdi.downloadComplete = YES;
        
        // Set the initial value to the taskIdentifier property of the fdi object,
        // so when the start button gets tapped again to start over the file download.
        self.fdi.taskIdentifier = -1;
        
        // In case there is any resume data stored in the fdi object, just make it nil.
        self.fdi.taskResumeData = nil;
        
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            
            self.image = [[UIImage alloc] initWithContentsOfFile:[destinationURL path]];
        }];
        
    }
    else{
        NSLog(@"Unable to copy temp file. Error: %@", [error localizedDescription]);
    }
}


-(void)URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task didCompleteWithError:(NSError *)error{
    if (error != nil) {
        NSLog(@"Download completed with error: %@", [error localizedDescription]);
    }
    else{
        NSLog(@"Download finished successfully.");
    }
}


-(void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask didWriteData:(int64_t)bytesWritten totalBytesWritten:(int64_t)totalBytesWritten totalBytesExpectedToWrite:(int64_t)totalBytesExpectedToWrite{
    
    if (totalBytesExpectedToWrite == NSURLSessionTransferSizeUnknown) {
        NSLog(@"Unknown transfer size");
    }
    else{
        // Locate the FileDownloadInfo object among all based on the taskIdentifier property of the task.

        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            // Calculate the progress.
            self.fdi.downloadProgress = (double)totalBytesWritten / (double)totalBytesExpectedToWrite;
            
            // Get the progress view of the appropriate cell and update its progress.
            //            UITableViewCell *cell = [self.tblFiles cellForRowAtIndexPath:[NSIndexPath indexPathForRow:index inSection:0]];
            //            UIProgressView *progressView = (UIProgressView *)[cell viewWithTag:CellProgressBarTagValue];
            //            progressView.progress = fdi.downloadProgress;
//            [progressView setProgress:(currentProgressDownloadPerFile + (fdi.downloadProgress/(float)self.arrFileDownloadData.count))];
            
        }];
    }
}


-(void)URLSessionDidFinishEventsForBackgroundURLSession:(NSURLSession *)session{
    NGREGAppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
    
    // Check if all download tasks have been finished.
    [self.session getTasksWithCompletionHandler:^(NSArray *dataTasks, NSArray *uploadTasks, NSArray *downloadTasks) {
        
        if ([downloadTasks count] == 0) {
            if (appDelegate.backgroundTransferCompletionHandler != nil) {
                // Copy locally the completion handler.
                void(^completionHandler)() = appDelegate.backgroundTransferCompletionHandler;
                
                // Make nil the backgroundTransferCompletionHandler.
                appDelegate.backgroundTransferCompletionHandler = nil;
                
                [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                    // Call the completion handler to tell the system that there are no other background transfers.
                    completionHandler();
                    
                    // Show a local notification when all downloads are over.
                  
                }];
            }
        }
    }];
}

@end
