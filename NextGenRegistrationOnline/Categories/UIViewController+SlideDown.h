//
//  UIViewController+SlideDown.h
//  BEAT-Universal
//
//  Created by Bayu Wy on 1/9/14.
//  Copyright (c) 2014 bayuwy. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^SlideDownCompletionHandler)(void);

typedef enum {
    SlideViewControllerPositionTop,
    SlideViewControllerPositionBottom,
    SlideViewControllerPositionRight,
    SlideViewControllerPositionLeft,
    SlideViewControllerPositionNone
} SlideViewControllerPosition;

@interface UIViewController (SlideDown)

- (void)slideViewController:(UIViewController *)controller inView:(UIView *)view targetRect:(CGRect)targetRect fromPosition:(SlideViewControllerPosition)position completion:(SlideDownCompletionHandler)block;
- (void)dismissSlideViewControllerToPosition:(SlideViewControllerPosition)position completion:(SlideDownCompletionHandler)block;

@end
